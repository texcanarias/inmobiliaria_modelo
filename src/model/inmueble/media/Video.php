<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble\media;

class Video extends Media{
    public function factoryVideo(?int $id, string $clave , string $name){
        $p = new self($id, $clave, $name);
        return $p;
    }    
}