<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\tipologia\comun;

trait TraitPlantasConstruidas{
    /**
     * Plantas construidas
     * @var int
     */
    private ?int $plantasConstruidas = null;

    public function getPlantasConstruidas() : ?int{
        return $this->plantasConstruidas;
    }

    public function setPlantasConstruidas(?int $plantasConstruidas){
        if(null == $plantasConstruidas){
            return $this;
        }

        if(0 <= $plantasConstruidas ){
            $this->plantasConstruidas = $plantasConstruidas;
        }else{
            throw new \Exception("Error en la plantas contruidas debe ser mayor o igual que 0");
        }
        return $this;
    }
}