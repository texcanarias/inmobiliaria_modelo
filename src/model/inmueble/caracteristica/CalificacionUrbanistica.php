<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble\caracteristica;

class CalificacionUrbanistica  extends CaracteristicaInt {
    static $ITEMS = ["RUSTICO" => "Rustico", 
                    "URBANIZABLE" => "Urbanizable", 
                    "URBANO" => "Urbano", 
                    "INDUSTRIAL" => "Industrial", 
                    "EN_DESARROLLO" => "En desarrollo"];

    static $RUSTICO = 0;
    static $URBANIZABLE = 1; 
    static $URBANO = 2;
    static $INDUSTRIAL = 3;
    static $EN_DESARROLLO = 4;

    use TraitFactory;
    

    protected function isCorrecto(int $item)
    {
        return self::$RUSTICO <= $item AND self::$EN_DESARROLLO >= $item;
    }
}