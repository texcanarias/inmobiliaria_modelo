<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble;

/**
 * @todo Pendiente de ver para que sirve en concreto
 */
class Nota extends \inmotek\model\base\DuplaIdNombre {
      static $TRANSFER  = 1;
      static $PUBLIC = 2;//En el caso de las notas publicas se debe grabar en el campo inmuebles_alt
      static $DELIVERY = 3;
      static $PRIVATE = 4;//En el caso de las notas privadas se debe registrar en la tabla inmueble
      static $GROUP = 5;
      
      static $ENTIDAD_INMUEBLE = 1;
      static $ENTIDAD_INTERES_CLIENTE = 2;

    /**
     * @var int Identificador del inmueble 
     */
    private $id_inmueble;
    
    /**
     * @var char Identidicador del idioma
     */
    private $id_idioma;
    
    /**
     * @var int Tipo de nota del que se trata
     */
    private $tipo_nota;
    
    
    private function __construct($id = null, $texto, $id_inmueble, $id_idioma, $tipo_nota) {
        parent::__construct($id, $texto);
        $this->id_inmueble = $id_inmueble;
        $this->id_idioma = $id_idioma;
        $this->tipo_nota = $tipo_nota;
    }

    public static function factory($id, $texto, $id_inmueble, $id_idioma, $tipo_nota){
        $n = new self($id, $texto, $id_inmueble, $id_idioma, $tipo_nota);
        return $n;
    }
}