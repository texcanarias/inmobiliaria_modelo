<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\tipologia\comun;

trait TraitSubtipo{
    /**
     * Subtipo de inmueble
     * @var int
     */
    private ?int $subtipo = null;

    /**
     * Recuperar el subtipo
     * @return int
     */
    public function getSubtipo() : ?int{
        return $this->subtipo;
    }

    /**
     * Setear el subtipo
     * @param integer $subtipo
     */
    public function setSubtipo(?int $subtipo) : self
    {
        $this->subtipo = $subtipo;
        return $this;
    }
}