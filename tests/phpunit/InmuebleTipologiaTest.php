<?php namespace inmotek_test\phpunit;

use PHPUnit\Framework\TestCase;
use inmotek\model\inmobiliaria\Inmobiliaria;

final class inmuebleTipologiaTest extends TestCase {

    private function getInmu(){
        $inmo = new Inmobiliaria(1, 'PRO', "Pruebas y text,S.A.");
        $inmo->setLetra('PR');
        $inmo->setToken('zztop123');
        $inmo->setDemo(false);
        $inmo->setSistema(true);        

        $direccion = \inmotek\model\inmueble\Localizacion::factoryLocalizacion('clave','ES', 35, 'Las Palmas', 35012, 'LPGC', '35012', 20.00, 20.01, 'Don Pío Coronado', '172', 'Unica', '2º', 'A');
        $inmo->setDireccion($direccion);

        $usuarios = new \inmotek\model\inmobiliaria\oficina\usuario\UsuarioList();
        $usuarios[] = new \inmotek\model\inmobiliaria\oficina\usuario\Usuario(1, "Roberto Fernandez");
        $usuarios[0]->setDireccion($direccion);

        $oficinas = new \inmotek\model\inmobiliaria\oficina\OficinaList();
        $oficinas[] = new \inmotek\model\inmobiliaria\oficina\Oficina(1,"Central");
        $oficinas[0]->setDireccion($direccion);
        $oficinas[0]->setPrincipal(true);
        $oficinas[0]->setOrden(0);
        $oficinas[0]->setUsuarios($usuarios);
        $inmo->setOficinas($oficinas);


        $inmueble = \inmotek\model\inmueble\Inmueble::factoryInmueble(1, "Casa palaciega",$inmo);

        return $inmueble;
    }

    public function testTipologiaCasa(){
        $inmueble = $this->getInmu();

        $casa = new \inmotek\model\inmueble\tipologia\Casa();
        $casa->setSubtipo(\inmotek\model\inmueble\tipologia\Casa::$SUBTIPO_CASA);
        $casa->setEstado(\inmotek\model\inmueble\tipologia\Casa::$ESTADO_REFORMA_INTEGRAL);
        $casa->setCes(\inmotek\model\inmueble\ces\Ces::factory(false, \inmotek\model\inmueble\ces\CesCore::factory(20,'A') , \inmotek\model\inmueble\ces\CesCore::factory(10,'C'), "CES2050", "", new \DateTime()));
        $casa->setOrientacionEste(true);
        $casa->setOrientacionNorte(true);

        $inmueble->setTipologia($casa);

        $this->assertTrue( \inmotek\model\inmueble\tipologia\Casa::$SUBTIPO_CASA == $inmueble->getTipologia()->getSubtipo() );
        $this->assertTrue( \inmotek\model\inmueble\tipologia\Casa::$ESTADO_REFORMA_INTEGRAL ==  $inmueble->getTipologia()->getEstado() );
        $this->assertTrue( $inmueble->getTipologia()->getOrientacionEste() );
        $this->assertTrue( $inmueble->getTipologia()->getOrientacionNorte() );
        $this->assertFalse( $inmueble->getTipologia()->getOrientacionOeste() );
        $this->assertFalse( $inmueble->getTipologia()->getOrientacionSur() );

    }


    public function testTipologiaEdificio(){
        $inmueble = $this->getInmu();

        $casa = new \inmotek\model\inmueble\tipologia\Edificio();
        $casa->setSubtipo(\inmotek\model\inmueble\tipologia\Edificio::$SUBTIPO_EDIFICIO);
        $casa->setEstado(\inmotek\model\inmueble\tipologia\Edificio::$ESTADO_REFORMA_INTEGRAL);
        $casa->setCes(\inmotek\model\inmueble\ces\Ces::factory(false, \inmotek\model\inmueble\ces\CesCore::factory(20,'A') , \inmotek\model\inmueble\ces\CesCore::factory(10,'C'), "CES2050", "", new \DateTime()));
        $casa->setSuperficieConstruida(200);
        $casa->setSuperficieUtil(150);
        $casa->setSuperficieAjardinada(50);
        $casa->setAnyoConstruccion(1973);
        $casa->setPlantasConstruidas(5);
        
        $casa->setOrientacionEste(true);
        $casa->setOrientacionNorte(true);
        
        $casa->setAscensorNumero(5);

        $casa->setSeguridadSistema(true);
        $casa->setSeguridadAlarma(true);
        $casa->setSeguridadPuerta(true);
        $casa->setSeguridad24h(true);
        $casa->setSeguridadPersonal(true);
    

        $casa->setEmergenciaSalida(true);
        $casa->setEmergenciaLuces(true);        

        $casa->setEficicioPropietarios(true);
        $casa->setEdificioAlquiler(true);
        $casa->setPlazasGarage(20);
        $casa->setTipoEstructura(\inmotek\model\inmueble\caracteristica\TipoEstructura::factory( \inmotek\model\inmueble\caracteristica\TipoEstructura::$HORMIGON )) ;
        $casa->setTipoFachada(\inmotek\model\inmueble\caracteristica\TipoFachada::factory( \inmotek\model\inmueble\caracteristica\TipoFachada::$RASEADA ) );

        $inmueble->setTipologia($casa);

        $this->assertTrue( \inmotek\model\inmueble\tipologia\Edificio::$SUBTIPO_EDIFICIO == $inmueble->getTipologia()->getSubtipo() );
        $this->assertTrue( \inmotek\model\inmueble\tipologia\Edificio::$ESTADO_REFORMA_INTEGRAL ==  $inmueble->getTipologia()->getEstado() );
        $this->assertTrue( 200 ==  $inmueble->getTipologia()->getSuperficieConstruida() );
        $this->assertTrue( 150 ==  $inmueble->getTipologia()->getSuperficieUtil() );
        $this->assertTrue( 50 ==  $inmueble->getTipologia()->getSuperficieAjardinada() );
        $this->assertTrue( 1973 ==  $inmueble->getTipologia()->getAnyoConstruccion() );
        $this->assertTrue( 5 ==  $inmueble->getTipologia()->getPlantasConstruidas() );
        $this->assertTrue( $inmueble->getTipologia()->getOrientacionEste() );
        $this->assertTrue( $inmueble->getTipologia()->getOrientacionNorte() );
        $this->assertFalse( $inmueble->getTipologia()->getOrientacionOeste() );
        $this->assertFalse( $inmueble->getTipologia()->getOrientacionSur() );
        $this->assertTrue( 5 == $inmueble->getTipologia()->getAscensorNumero());
        $this->assertTrue( null == $inmueble->getTipologia()->getAscensorCotaCero() );
        $this->assertTrue( $inmueble->getTipologia()->isAscensorDisponible() );
        $this->assertTrue( $inmueble->getTipologia()->getSeguridadSistema() );
        $this->assertTrue( $inmueble->getTipologia()->getSeguridadAlarma() );
        $this->assertTrue( $inmueble->getTipologia()->getSeguridadPuerta() );
        $this->assertTrue( $inmueble->getTipologia()->getSeguridad24h() );
        $this->assertTrue( $inmueble->getTipologia()->getSeguridadPersonal() );
        $this->assertTrue( $inmueble->getTipologia()->getEmergenciaSalida());
        $this->assertTrue( $inmueble->getTipologia()->getEmergenciaLuces());
        $this->assertTrue( $inmueble->getTipologia()->getEficicioPropietarios() );
        $this->assertTrue( $inmueble->getTipologia()->getEdificioAlquiler() );
        $this->assertTrue( 20 ==  $inmueble->getTipologia()->getPlazasGarage() );
        $this->assertTrue( \inmotek\model\inmueble\caracteristica\TipoEstructura::$HORMIGON == $inmueble->getTipologia()->getTipoEstructura()()  );
        $this->assertTrue( \inmotek\model\inmueble\caracteristica\TipoFachada::$RASEADA == $inmueble->getTipologia()->getTipoFachada()() );
    }


    public function testTipologiaGaraje(){
        $inmueble = $this->getInmu();

        $casa = new \inmotek\model\inmueble\tipologia\Garaje();
        $casa->setSubtipo(\inmotek\model\inmueble\tipologia\Garaje::$SUBTIPO_COCHE);
        $casa->setEstado(\inmotek\model\inmueble\tipologia\Garaje::$ESTADO_REFORMA_INTEGRAL);
        $casa->setSuperficieConstruida(20);
        $casa->setAscensorNumero(1);
        $casa->setPuertaAutomatica(true);
        $casa->setPlazaCubierta(true);
        $casa->setSeguridadAlarma(true);
        $casa->setSeguridadPersonal(true);
        $casa->setSeguridadSistema(true);

        $inmueble->setTipologia($casa);


        $this->assertTrue( \inmotek\model\inmueble\tipologia\Garaje::$SUBTIPO_COCHE == $inmueble->getTipologia()->getSubtipo() );
        $this->assertTrue( \inmotek\model\inmueble\tipologia\Garaje::$ESTADO_REFORMA_INTEGRAL ==  $inmueble->getTipologia()->getEstado() );
        $this->assertTrue( 20 ==  $inmueble->getTipologia()->getSuperficieConstruida() );
        $this->assertTrue( $inmueble->getTipologia()->isAscensorDisponible() );
        $this->assertTrue( $inmueble->getTipologia()->getPuertaAutomatica() );
        $this->assertTrue( $inmueble->getTipologia()->getPlazaCubierta() );
        $this->assertTrue( $inmueble->getTipologia()->getSeguridadAlarma() );
        $this->assertTrue( $inmueble->getTipologia()->getSeguridadPersonal() );
        $this->assertTrue( $inmueble->getTipologia()->getSeguridadSistema() );

    }


    public function testTipologiaHabitacion(){
        $inmueble = $this->getInmu();

        $casa = new \inmotek\model\inmueble\tipologia\Habitacion();
        $casa->setSubtipo(\inmotek\model\inmueble\tipologia\Habitacion::$SUBTIPO_ESTUDIANTE);
        $casa->setSuperficieConstruida(200);
        $casa->setSuperficieUtil(150);

        $inmueble->setTipologia($casa);

        $this->assertTrue( \inmotek\model\inmueble\tipologia\Habitacion::$SUBTIPO_ESTUDIANTE == $inmueble->getTipologia()->getSubtipo() );
        $this->assertTrue( 200 ==  $inmueble->getTipologia()->getSuperficieConstruida() );
        $this->assertTrue( 150 ==  $inmueble->getTipologia()->getSuperficieUtil() );
    }


    public function testTipologiaLocal(){
        $inmueble = $this->getInmu();

        $casa = new \inmotek\model\inmueble\tipologia\Local();
        $casa->setSubtipo(\inmotek\model\inmueble\tipologia\Local::$SUBTIPO_ALMACEN);
        $casa->setEstado(\inmotek\model\inmueble\tipologia\Local::$ESTADO_REFORMA_INTEGRAL);
        $casa->setSuperficieConstruida(200);
        $casa->setSuperficieUtil(150);
        $casa->setSuperficieAjardinada(50);
        $casa->setPlantasConstruidas(5);

        $casa->setBanyoNumero(2);
        $casa->setBanyoAdaptado(true);

        $casa->setSeguridadSistema(true);
        $casa->setSeguridadAlarma(true);
        $casa->setSeguridadPuerta(true);
        $casa->setSeguridad24h(true);
        $casa->setSeguridadPersonal(true);


        $casa->setAireAcondicionado(\inmotek\model\inmueble\caracteristica\AireAcondicionado::factory( \inmotek\model\inmueble\caracteristica\AireAcondicionado::$BOMBA_DE_CALOR ) );
        $casa->setCalefaccion(\inmotek\model\inmueble\caracteristica\Calefaccion::factory( \inmotek\model\inmueble\caracteristica\Calefaccion::$INDIVIDUAL_SUELO) );
        $casa->setCocinaEquipada(false);
        $casa->setAreaFachada(45);
        $casa->setUltimaActividad("garito");
        $casa->setHuecos(5);
        $casa->setAlmacen(true);
        $casa->setExtractorHumos(true);
        $casa->setUbicacion(\inmotek\model\inmueble\tipologia\Local::$UBICACION_SOTANO);
        $casa->setNumeroVentanas(5);        

        $inmueble->setTipologia($casa);

        $this->assertTrue( \inmotek\model\inmueble\tipologia\Local::$SUBTIPO_ALMACEN == $inmueble->getTipologia()->getSubtipo() );
        $this->assertTrue( \inmotek\model\inmueble\tipologia\Local::$ESTADO_REFORMA_INTEGRAL ==  $inmueble->getTipologia()->getEstado() );
        $this->assertTrue( 200 ==  $inmueble->getTipologia()->getSuperficieConstruida() );
        $this->assertTrue( 150 ==  $inmueble->getTipologia()->getSuperficieUtil() );
        $this->assertTrue( 50 ==  $inmueble->getTipologia()->getSuperficieAjardinada() );
        $this->assertTrue( 5 ==  $inmueble->getTipologia()->getPlantasConstruidas() );
        $this->assertTrue( 2 == $inmueble->getTipologia()->getBanyoNumero(2) ); 
        $this->assertTrue( $inmueble->getTipologia()->getBanyoAdaptado() );
        $this->assertTrue( $inmueble->getTipologia()->getSeguridadSistema() );
        $this->assertTrue( $inmueble->getTipologia()->getSeguridadAlarma() );
        $this->assertTrue( $inmueble->getTipologia()->getSeguridadPuerta() );
        $this->assertTrue( $inmueble->getTipologia()->getSeguridad24h() );
        $this->assertTrue( $inmueble->getTipologia()->getSeguridadPersonal() );

        $this->assertTrue( \inmotek\model\inmueble\caracteristica\AireAcondicionado::$BOMBA_DE_CALOR == $inmueble->getTipologia()->getAireAcondicionado()() );
        $this->assertTrue( \inmotek\model\inmueble\caracteristica\Calefaccion::$INDIVIDUAL_SUELO == $inmueble->getTipologia()->getCalefaccion()() );
        $this->assertFalse( $inmueble->getTipologia()->getCocinaEquipada() );
        $this->assertTrue( 45 == $inmueble->getTipologia()->getAreaFachada() );
        $this->assertTrue( "garito" == $inmueble->getTipologia()->getUltimaActividad() );
        $this->assertTrue( 5 == $inmueble->getTipologia()->getHuecos() );
        $this->assertTrue( $inmueble->getTipologia()->getAlmacen() );
        $this->assertTrue( $inmueble->getTipologia()->getExtractorHumos() );
        $this->assertTrue( \inmotek\model\inmueble\tipologia\Local::$UBICACION_SOTANO == $inmueble->getTipologia()->getUbicacion() );
        $this->assertTrue( 5 == $inmueble->getTipologia()->getNumeroVentanas() );

    }

    public function testTipologiaNave(){
        $inmueble = $this->getInmu();

        $casa = new \inmotek\model\inmueble\tipologia\Nave();
        $casa->setSubtipo(\inmotek\model\inmueble\tipologia\Nave::$SUBTIPO_GRANERO);
        $casa->setEstado(\inmotek\model\inmueble\tipologia\Nave::$ESTADO_REFORMA_INTEGRAL);

        $casa->setVolumen(\inmotek\model\inmueble\caracteristica\Volumen::factory(20,30,40));
        
        $inmueble->setTipologia($casa);

        $this->assertTrue( \inmotek\model\inmueble\tipologia\Nave::$SUBTIPO_GRANERO == $inmueble->getTipologia()->getSubtipo() );
        $this->assertTrue( \inmotek\model\inmueble\tipologia\Nave::$ESTADO_REFORMA_INTEGRAL ==  $inmueble->getTipologia()->getEstado() );
        $this->assertTrue(20 == $inmueble->getTipologia()->getVolumen()->getProfundidad());
        $this->assertTrue(30 == $inmueble->getTipologia()->getVolumen()->getAnchura());
        $this->assertTrue(40 == $inmueble->getTipologia()->getVolumen()->getAltura());
        $this->assertTrue(600 == $inmueble->getTipologia()->getVolumen()->getSuperficie() );
        $this->assertTrue(24000 == $inmueble->getTipologia()->getVolumen()->getVolumen() );    
    }


    public function testTipologiaOficina(){
        $inmueble = $this->getInmu();

        $casa = new \inmotek\model\inmueble\tipologia\Oficina();
        $casa->setSubtipo(\inmotek\model\inmueble\tipologia\Oficina::$SUBTIPO_DESPACHO);
        $casa->setEstado(\inmotek\model\inmueble\tipologia\Oficina::$ESTADO_REFORMA_INTEGRAL);


        $inmueble->setTipologia($casa);

        $this->assertTrue( \inmotek\model\inmueble\tipologia\Oficina::$SUBTIPO_DESPACHO == $inmueble->getTipologia()->getSubtipo() );
        $this->assertTrue( \inmotek\model\inmueble\tipologia\Oficina::$ESTADO_REFORMA_INTEGRAL ==  $inmueble->getTipologia()->getEstado() );
    }


    public function testTipologiaPiso(){
        $inmueble = $this->getInmu();

        $casa = new \inmotek\model\inmueble\tipologia\Piso();
        $casa->setSubtipo(\inmotek\model\inmueble\tipologia\Piso::$SUBTIPO_ATICO);
        $casa->setEstado(\inmotek\model\inmueble\tipologia\Piso::$ESTADO_REFORMA_INTEGRAL);
        $casa->setCes(\inmotek\model\inmueble\ces\Ces::factory(false, \inmotek\model\inmueble\ces\CesCore::factory(20,'A') , \inmotek\model\inmueble\ces\CesCore::factory(10,'C'), "CES2050", "", new \DateTime()));
        $casa->setSuperficieConstruida(200);
        $casa->setSuperficieUtil(150);
        $casa->setAnyoConstruccion(1973);

        $inmueble->setTipologia($casa);

        $this->assertTrue( \inmotek\model\inmueble\tipologia\Piso::$SUBTIPO_ATICO == $inmueble->getTipologia()->getSubtipo() );
        $this->assertTrue( \inmotek\model\inmueble\tipologia\Piso::$ESTADO_REFORMA_INTEGRAL ==  $inmueble->getTipologia()->getEstado() );
        $this->assertTrue( 200 ==  $inmueble->getTipologia()->getSuperficieConstruida() );
        $this->assertTrue( 150 ==  $inmueble->getTipologia()->getSuperficieUtil() );
        $this->assertTrue( 1973 ==  $inmueble->getTipologia()->getAnyoConstruccion() );
    }


    public function testTipologiaTerreno(){
        $inmueble = $this->getInmu();

        $casa = new \inmotek\model\inmueble\tipologia\Terreno();
        $casa->setSubtipo(\inmotek\model\inmueble\tipologia\Terreno::$SUBTIPO_TERRENO);
        $casa->setSuperficie(200);
        $casa->setSuperficieEdificable(100);
        $casa->setTipoTerreno(\inmotek\model\inmueble\caracteristica\TipoTerreno::factory( \inmotek\model\inmueble\caracteristica\TipoTerreno::$LLANO) );
        $casa->setCalificacionUrbanistica( \inmotek\model\inmueble\caracteristica\CalificacionUrbanistica::factory( \inmotek\model\inmueble\caracteristica\CalificacionUrbanistica::$INDUSTRIAL) );
        $casa->setFacilityElectricidad(true);
        $casa->setFacilityGasNatural(true);
        $casa->setFacilityAccesoRodado(true);
        $casa->setFacilityAlcantarillado(true);
        $casa->setFacilityAcera(true);
        $casa->setFacilityIluminacionPublica(true);
        $casa->setFacilityAgua(true);        

        $inmueble->setTipologia($casa);

        $this->assertTrue( \inmotek\model\inmueble\tipologia\Terreno::$SUBTIPO_TERRENO == $inmueble->getTipologia()->getSubtipo() );
        $this->assertTrue( 200 ==  $inmueble->getTipologia()->getSuperficie() );
        $this->assertTrue( 100 ==  $inmueble->getTipologia()->getSuperficieEdificable() );

        $this->assertTrue( \inmotek\model\inmueble\caracteristica\TipoTerreno::$LLANO == $inmueble->getTipologia()->getTipoTerreno()() );
        $this->assertTrue( \inmotek\model\inmueble\caracteristica\CalificacionUrbanistica::$INDUSTRIAL == $inmueble->getTipologia()->getCalificacionUrbanistica()() );       

        $this->assertTrue( $inmueble->getTipologia()->getFacilityElectricidad() );
        $this->assertTrue( $inmueble->getTipologia()->getFacilityGasNatural() );
        $this->assertTrue( $inmueble->getTipologia()->getFacilityAccesoRodado() );
        $this->assertTrue( $inmueble->getTipologia()->getFacilityAlcantarillado() );
        $this->assertTrue( $inmueble->getTipologia()->getFacilityAcera() );
        $this->assertTrue( $inmueble->getTipologia()->getFacilityIluminacionPublica() );
        $this->assertTrue( $inmueble->getTipologia()->getFacilityAgua() );        

    }


    public function testTipologiaTrastero(){
        $inmueble = $this->getInmu();

        $casa = new \inmotek\model\inmueble\tipologia\Trastero();
        $casa->setSubtipo(\inmotek\model\inmueble\tipologia\Trastero::$SUBTIPO_TRASTERO);
        $casa->setEstado(\inmotek\model\inmueble\tipologia\Trastero::$ESTADO_REFORMA_INTEGRAL);

        $inmueble->setTipologia($casa);

        $this->assertTrue( \inmotek\model\inmueble\tipologia\Trastero::$SUBTIPO_TRASTERO == $inmueble->getTipologia()->getSubtipo() );
        $this->assertTrue( \inmotek\model\inmueble\tipologia\Trastero::$ESTADO_REFORMA_INTEGRAL ==  $inmueble->getTipologia()->getEstado() );
    }

}