<?php

declare(strict_types=1);

namespace inmotek\model\inmueble\tipologia;

use \inmotek\model\inmueble\caracteristica\TipoTerreno;
use \inmotek\model\inmueble\caracteristica\CalificacionUrbanistica;
use inmotek\model\inmueble\caracteristica\TipoFachada;

class Terreno implements InterfaceTipologia
{
    public static $SUBTIPO_PARCELA = 0;
    public static $SUBTIPO_SOLAR = 1;
    public static $SUBTIPO_TERRENO = 2;

    use comun\TraitSubtipo;

    /**
     * Tipo de terreno
     * @var \inmotek\model\inmueble\caracteristica\TipoTerreno
     */
    private ?TipoTerreno $tipoTerreno = null;

    /**
     * Calificacion urbanistica
     * @var \inmotek\model\inmueble\caracteristica\CalificacionUrbanistica
     */
    private ?CalificacionUrbanistica $calificacionUrbanistica = null;

    /**
     * @var int Superficie total
     */
    private ?int $superficie = null;

    /**
     * @var int Superficie edificable
     */
    private ?int $superficieEdificable = null;

    /**
     * @var bool Si hay preinstalacion electrica
     */
    private ?bool $facilityElectricidad = true;

    /**
     * Preinstalacion de gas natural
     * @var bool
     */
    private ?bool $facilityGasNatural = true;

    /**
     * Acceso por carretera
     * @var bool
     */
    private ?bool $facilityAccesoRodado = true;

    /**
     * Preintalacion de alcantarillado
     * @var bool
     */
    private ?bool $facilityAlcantarillado = true;

    /**
     * Presencia de acera
     * @var bool
     */
    private ?bool $facilityAcera = true;

    /**
     * Presencia de iluminacion publica
     * @var bool
     */
    private ?bool $facilityIluminacionPublica = true;

    /**
     * Preintalacion de agua de abasto
     * @var bool
     */
    private ?bool $facilityAgua = true;


    public function getTipologia(): self
    {
        return $this;
    }

    public function getTipo() : string{
        return "Terreno";
    }

    public function getSuperficie() : ?int
    {
        return $this->superficie;
    }

    public function setSuperficie(?int $superficie) : self
    {
        $this->superficie = $superficie;
        return $this;
    }

    public function getSuperficieEdificable() : ?int
    {
        return $this->superficieEdificable;
    }

    public function setSuperficieEdificable(?int $superficieEdificable) : self
    {
        $this->superficieEdificable = $superficieEdificable;
        return $this;
    }


    /**
     * Get si hay preinstalacion electrica
     *
     * @return  bool
     */
    public function getFacilityElectricidad(): ?bool
    {
        return $this->facilityElectricidad;
    }

    /**
     * Set si hay preinstalacion electrica
     *
     * @param  bool  $facilityElectricidad  Si hay preinstalacion electrica
     *
     * @return  self
     */
    public function setFacilityElectricidad(?bool $facilityElectricidad) : self
    {
        $this->facilityElectricidad = $facilityElectricidad;

        return $this;
    }

    /**
     * Get preinstalacion de gas natural
     *
     * @return  bool
     */
    public function getFacilityGasNatural() : ?bool
    {
        return $this->facilityGasNatural;
    }

    /**
     * Set preinstalacion de gas natural
     *
     * @param  bool  $facilityGasNatural  Preinstalacion de gas natural
     *
     * @return  self
     */
    public function setFacilityGasNatural(?bool $facilityGasNatural) : self
    {
        $this->facilityGasNatural = $facilityGasNatural;

        return $this;
    }

    /**
     * Get acceso por carretera
     *
     * @return  bool
     */
    public function getFacilityAccesoRodado() : ?bool
    {
        return $this->facilityAccesoRodado;
    }

    /**
     * Set acceso por carretera
     *
     * @param  bool  $facilityAccesoRodado  Acceso por carretera
     *
     * @return  self
     */
    public function setFacilityAccesoRodado(?bool $facilityAccesoRodado) : self
    {
        $this->facilityAccesoRodado = $facilityAccesoRodado;

        return $this;
    }

    /**
     * Get preintalacion de alcantarillado
     *
     * @return  bool
     */
    public function getFacilityAlcantarillado() : ?bool
    {
        return $this->facilityAlcantarillado;
    }

    /**
     * Set preintalacion de alcantarillado
     *
     * @param  bool  $facilityAlcantarillado  Preintalacion de alcantarillado
     *
     * @return  self
     */
    public function setFacilityAlcantarillado(?bool $facilityAlcantarillado) : self
    {
        $this->facilityAlcantarillado = $facilityAlcantarillado;

        return $this;
    }

    /**
     * Get presencia de acera
     *
     * @return  bool
     */
    public function getFacilityAcera() : ?bool
    {
        return $this->facilityAcera;
    }

    /**
     * Set presencia de acera
     *
     * @param  bool  $facilityAcera  Presencia de acera
     *
     * @return  self
     */
    public function setFacilityAcera(?bool $facilityAcera) : self
    {
        $this->facilityAcera = $facilityAcera;

        return $this;
    }

    /**
     * Get presencia de iluminacion publica
     *
     * @return  bool
     */
    public function getFacilityIluminacionPublica() : ?bool
    {
        return $this->facilityIluminacionPublica;
    }

    /**
     * Set presencia de iluminacion publica
     *
     * @param  bool  $facilityIluminacionPublica  Presencia de iluminacion publica
     *
     * @return  self
     */
    public function setFacilityIluminacionPublica(?bool $facilityIluminacionPublica) : self
    {
        $this->facilityIluminacionPublica = $facilityIluminacionPublica;

        return $this;
    }

    /**
     * Get preintalacion de agua de abasto
     *
     * @return  bool
     */
    public function getFacilityAgua() : ?bool
    {
        return $this->facilityAgua;
    }

    /**
     * Set preintalacion de agua de abasto
     *
     * @param  bool  $facilityAgua  Preintalacion de agua de abasto
     *
     * @return  self
     */
    public function setFacilityAgua(?bool $facilityAgua) : self
    {
        $this->facilityAgua = $facilityAgua;

        return $this;
    }

    /**
     * Get tipo de terreno
     *
     * @return  \inmotek\model\inmueble\caracteristica\TipoTerreno
     */
    public function getTipoTerreno() : ?TipoTerreno
    {
        return $this->tipoTerreno;
    }

    /**
     * Set tipo de terreno
     *
     * @param  \inmotek\model\inmueble\caracteristica\TipoTerreno  $tipoTerreno  Tipo de terreno
     *
     * @return  self
     */
    public function setTipoTerreno(?TipoTerreno $tipoTerreno) : self
    {
        $this->tipoTerreno = $tipoTerreno;

        return $this;
    }

    /**
     * Get calificacion urbanistica
     *
     * @return  \inmotek\model\inmueble\caracteristica\CalificacionUrbanistica
     */
    public function getCalificacionUrbanistica() : ?CalificacionUrbanistica
    {
        return $this->calificacionUrbanistica;
    }

    /**
     * Set calificacion urbanistica
     *
     * @param  \inmotek\model\inmueble\caracteristica\CalificacionUrbanistica  $calificacionUrbanistica  Calificacion urbanistica
     *
     * @return  self
     */
    public function setCalificacionUrbanistica(CalificacionUrbanistica $calificacionUrbanistica) : self
    {
        $this->calificacionUrbanistica = $calificacionUrbanistica;

        return $this;
    }
}
