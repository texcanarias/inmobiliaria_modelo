<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\tipologia;

class Casa 
    extends Edificio 
    implements InterfaceTipologia{

    public static $SUBTIPO_ADOSADO = 0;
    public static $SUBTIPO_BIFAMILIAR = 1;
    public static $SUBTIPO_BUNGALOW = 2;
    public static $SUBTIPO_CASA = 3;
    public static $SUBTIPO_CASERIO = 4;
    public static $SUBTIPO_CHALET = 5;
    public static $SUBTIPO_MASIA = 6;
    public static $SUBTIPO_MODULO_CAMPING = 7;
    public static $SUBTIPO_VILLA = 8;

    use comun\TraitBanyo;
    use comun\TraitAccesibilidad;
    use comun\TraitVivienda;
    use comun\TraitDeporteOcio;

    public function getTipo() : string{
        return "Casa";
    }
}