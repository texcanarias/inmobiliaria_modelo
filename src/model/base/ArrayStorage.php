<?php 
declare(strict_types = 1);
namespace inmotek\model\base;

class ArrayStorage 
    implements \Iterator, \ArrayAccess, \Countable
{
    /**
     * Array que almacenará los datos
     */
    private $holder = [];

    /**
     * Tipo de objeto que se va a almacenar
     */
    private $instanceName;

    /**
     * 
     * @param \stdClass $instanceName Instancia de un objeto determinado
     * @throws \Exception
     */
    public function __construct($instanceName)
    {
        if (!class_exists($instanceName)) {
            throw new \Exception('No se ha encontrado la clase '.$instanceName.' .');
        }
        $this->instanceName = $instanceName;
    }
    
    /**
     * implementacion de la interfaz iterator
     */
    public function rewind() 
    {
        reset($this->holder);
    }

    /**
     * implementacion de la interfaz iterator
     */    
    public function current() 
    {
        return current($this->holder);
    }

    /**
     * implementacion de la interfaz iterator
     */    
    public function key() 
    {
        return key($this->holder);
    }

    /**
     * implementacion de la interfaz iterator
     */    
    public function next() 
    {
        next($this->holder);
    }

    /**
     * implementacion de la interfaz iterator
     */    
    public function valid() 
    {
        return false !== $this->current();
    }

    /**
     * implementacion para la interfaz ArrayAccess
     * Comprueba si existe o no un indice
     * @param int $offset
     * @param \stdClass $value
     * @throws \Exception
     */
    public function offsetSet($offset, $value) 
    {
        if (!($value instanceof $this->instanceName)) {
            throw new \Exception('Sólo se permiten intancias de "'.$this->instanceName.'" ');
        }
        if (is_null($offset)) {
            $this->holder[] = $value;
        } else {
            $this->holder[$offset] = $value;
        }
    }

    /**
     * implementacion para la interfaz ArrayAccess
     * Comprueba si existe o no un indice
     * @param int $offset
     * @return bool
     */
    public function offsetExists($offset) 
    {
        return isset($this->holder[$offset]);
    }

    /**
     * implementacion para la interfaz ArrayAccess
     * Destruye una posicion del offset
     * @param type $offset
     */
    public function offsetUnset($offset) 
    {
        unset($this->holder[$offset]);
    }

    /**
     * implementacion para la interfaz ArrayAccess
     * Recupera una posicion del array
     * @param type $offset
     * @return type
     */
    public function offsetGet($offset) 
    {
        return isset($this->holder[$offset]) ? $this->holder[$offset] : null;
    }
    
    /**
     * Implementación para la interfaz countable
     * @return int
     */
    public function count(){
        return count($this->holder);
    }
}