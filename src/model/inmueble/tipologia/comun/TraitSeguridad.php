<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\tipologia\comun;

trait TraitSeguridad
{
    /**
     * Video portero
     * @var ?bool
     */
    private ?bool $videoPortero = null;

    /**
     * Sistema de seguridad
     * @var bool
     */
    private ?bool $seguridadSistema = null;


    /**
     * Alarma de seguridad
     * @var bool
     */
    private ?bool $seguridadAlarma = null;


    /**
     * Puerta de seguridad
     * @var bool
     */
    private ?bool $seguridadPuerta = null;

    
    /**
     * Seguridad 24h
     * @var bool
     */
    private ?bool $seguridad24h = null;


  /**
   * Portero , personal porteria o seguridad
   * @bool
   */
  private ?bool $seguridadPersonal = null;


    /**
     * Get alarma de seguridad
     *
     * @return  bool
     */ 
    public function getSeguridadAlarma() : ?bool
    {
        return $this->seguridadAlarma;
    }

    /**
     * Set alarma de seguridad
     *
     * @param  bool  $seguridadAlarma  Alarma de seguridad
     *
     * @return  self
     */ 
    public function setSeguridadAlarma(?bool $seguridadAlarma) : self
    {
        $this->seguridadAlarma = $seguridadAlarma;

        return $this;
    }

    /**
     * Get puerta de seguridad
     */ 
    public function getSeguridadPuerta() : ?bool
    {
        return $this->seguridadPuerta;
    }

    /**
     * Set puerta de seguridad
     *
     * @return  self
     */ 
    public function setSeguridadPuerta(?bool $seguridadPuerta) : self
    {
        $this->seguridadPuerta = $seguridadPuerta;

        return $this;
    }

    /**
     * Get sistema de seguiridad
     *
     * @return  bool
     */ 
    public function getSeguridadSistema() : ?bool
    {
        return $this->seguridadSistema;
    }

    /**
     * Set sistema de seguiridad
     *
     * @param  bool  $seguridadSistema  Sistema de seguiridad
     *
     * @return  self
     */ 
    public function setSeguridadSistema(?bool $seguridadSistema) : self
    {
        $this->seguridadSistema = $seguridadSistema;

        return $this;
    }

    /**
     * Get seguridad 24h
     *
     * @return  bool
     */ 
    public function getSeguridad24h() : ?bool
    {
        return $this->seguridad24h;
    }

    /**
     * Set seguridad 24h
     *
     * @param  bool  $seguridad24h  Seguridad 24h
     *
     * @return  self
     */ 
    public function setSeguridad24h(?bool $seguridad24h) : self
    {
        $this->seguridad24h = $seguridad24h;

        return $this;
    }

  

  /**
   * Get portero , personal porteria o seguridad
   */ 
  public function getSeguridadPersonal() : ?bool
  {
    return $this->seguridadPersonal;
  }

  /**
   * Set portero , personal porteria o seguridad
   *
   * @return  self
   */ 
  public function setSeguridadPersonal(?bool $seguridadPersonal) : self
  {
    $this->seguridadPersonal = $seguridadPersonal;

    return $this;
  }

    /**
     * Get video portero
     *
     * @return  ?bool
     */ 
    public function getVideoPortero() : ?bool
    {
        return $this->videoPortero;
    }

    /**
     * Set video portero
     *
     * @param  ?bool  $videoPortero  Video portero
     *
     * @return  self
     */ 
    public function setVideoPortero(?bool $videoPortero) : self
    {
        $this->videoPortero = $videoPortero;

        return $this;
    }
}
