<?php namespace inmotek_test\phpunit;

use PHPUnit\Framework\TestCase;
use inmotek\model\inmobiliaria\Inmobiliaria;

final class inmuebleTradeTest extends TestCase {

    private function getInmu(){
        $inmo = new Inmobiliaria(1, 'PRO', "Pruebas y text,S.A.");
        $inmo->setLetra('PR');
        $inmo->setToken('zztop123');
        $inmo->setDemo(false);
        $inmo->setSistema(true);        

        $direccion = \inmotek\model\inmueble\Localizacion::factoryLocalizacion('clave','ES', 35, 'Las Palmas', 35012, 'LPGC', '35012', 20.00, 20.01, 'Don Pío Coronado', '172', 'Unica', '2º', 'A');
        $inmo->setDireccion($direccion);

        $usuarios = new \inmotek\model\inmobiliaria\oficina\usuario\UsuarioList();
        $usuarios[] = new \inmotek\model\inmobiliaria\oficina\usuario\Usuario(1, "Roberto Fernandez");
        $usuarios[0]->setDireccion($direccion);

        $oficinas = new \inmotek\model\inmobiliaria\oficina\OficinaList();
        $oficinas[] = new \inmotek\model\inmobiliaria\oficina\Oficina(1,"Central");
        $oficinas[0]->setDireccion($direccion);
        $oficinas[0]->setPrincipal(true);
        $oficinas[0]->setOrden(0);
        $oficinas[0]->setUsuarios($usuarios);
        $inmo->setOficinas($oficinas);


        $inmueble = \inmotek\model\inmueble\Inmueble::factoryInmueble(1, "Casa palaciega",$inmo);
        return $inmueble;
    }

    public function testTrade(){
        $inmueble = $this->getInmu();
        $tradeList = $inmueble->getTrade();

        $alquiler = new \inmotek\model\inmueble\trade\Alquiler();
        $alquiler->setOpcionCompra(true);
        $alquiler->setPrecio(1500);
        $tradeList->setAlquiler($alquiler);

        $venta = new \inmotek\model\inmueble\trade\Venta();
        $venta->setPrecio(500000);
        $tradeList->setVenta($venta);

        $vacacional = new \inmotek\model\inmueble\trade\Vacacional();
        $vacacional->setPrecio(500);
        $tradeList->setVacacional($vacacional);

        $traspaso = new \inmotek\model\inmueble\trade\Traspaso();
        $traspaso->setPrecio(600000);
        $tradeList->setTraspaso($traspaso);

        $inmueble->setTrade($tradeList);

        $this->assertTrue( 1500 == $tradeList['alquiler']->getPrecio() );
        $this->assertTrue( 500000 == $tradeList['venta']->getPrecio() );
        $this->assertTrue( 500 == $tradeList['vacacional']->getPrecio() );
        $this->assertTrue( 600000 == $tradeList['traspaso']->getPrecio() );
    }


    public function testTradeAlt(){
        $inmueble = $this->getInmu();
        $tradeList = $inmueble->getTrade();

        $alquiler = new \inmotek\model\inmueble\trade\Alquiler();
        $alquiler->setOpcionCompra(true);
        $alquiler->setPrecio(1500);
        $tradeList[] = $alquiler;

        $venta = new \inmotek\model\inmueble\trade\Venta();
        $venta->setPrecio(500000);
        $tradeList[] = $venta;

        $vacacional = new \inmotek\model\inmueble\trade\Vacacional();
        $vacacional->setPrecio(500);
        $tradeList[] = $vacacional;

        $traspaso = new \inmotek\model\inmueble\trade\Traspaso();
        $traspaso->setPrecio(600000);
        $tradeList[] = $traspaso;

        $inmueble->setTrade($tradeList);

        $this->assertTrue( 1500 == $tradeList['alquiler']->getPrecio() );
        $this->assertTrue( 500000 == $tradeList['venta']->getPrecio() );
        $this->assertTrue( 500 == $tradeList['vacacional']->getPrecio() );
        $this->assertTrue( 600000 == $tradeList['traspaso']->getPrecio() );
    }

}
