```plantuml
@startuml

package inmotek <<Frame>> {
    package model <<Frame>> {
        package inmueble <<Frame>> {
            package factura <<Frame>> {
                class Factura
                class FacturaDetalle
                class FacturaDetalleList
            }
        }
    }
}

class Factura
{
}

Factura *-- FacturaDetalleList

class FacturaDetalle{
    - id_factura
    - id_producto
    - concepto
    - importe
    - unidades
    - total
    - iva
}

class FacturaDetalleList{
    + getTotalNeto()
    + getTotalBruto()
    + getTotalImpuestos()
}

FacturaDetalleList *-- FacturaDetalle

class Producto{

}

FacturaDetalle o-- Producto
@enduml
```