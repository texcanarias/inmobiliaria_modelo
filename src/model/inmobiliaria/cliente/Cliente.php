<?php 
declare(strict_types = 1);
namespace inmotek\model\inmobiliaria\cliente;

/**
 * Modelos para clientes
 */
class Cliente extends ClienteBase{

    /**
     * Colección de clientes relacionados 
     * Tipo de objeto ClienteRelacion
     * @var array
     *
     */        
    private $relaciones;
    
    /**
     * Coleccion de etiquetas vinculadas al cliente
     * @var char
     *
     */
    private $labels;
    
    public function __construct($id, $nombre) {
        parent::__construct($id, $nombre);
    }

}
