<?php
declare(strict_types=1);
namespace inmotek\model\inmueble\caracteristica;

use Exception;

class DisponibilidadTrastero extends CaracteristicaInt
{
    static $ITEMS = [
        "NO_TIENE" => 0,
        "TRASTERO_OPCIONAL" => 1,
        "TRASTERO_INCLUIDO" => 2
    ];

    static $NO_TIENE = 0;
    static $TRASTERO_OPCIONAL = 1;
    static $TRASTERO_INCLUIDO = 2;

    use TraitFactory;

    protected function isCorrecto(int $item)
    {
        return self::$NO_TIENE <= $item and self::$TRASTERO_INCLUIDO >= $item;
    }
}
