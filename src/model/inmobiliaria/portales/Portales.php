<?php 
declare(strict_types = 1);
namespace inmotek\model\inmobiliaria\portales;

/**
* Portales con los cual tiene relación la inmobiliaria
*/
class Portales{
    /**
     * @var bool Si tiene opción de publicar en portales
     */
    private $publicarPortales;

    /**
     * @var \inmotek\model\inmobiliaria\portales\Idealista
     */
    private $idealista;

    /**
     * @var \inmotek\model\inmobiliaria\portales\Fotocasa
     */
    private $fotocasa;


    public function __construct(){
        $this->publicarPortales = false;
    }

    public function getPublicarPortales(){
        return $this->publicarPortales;
    }

    public function setPublicarPortales($item){
        $this->publicarPortales = $item;
        return $this;
    }

    public function getIdealista(){
        return $this->idealista;
    }    

    public function setIdealista(\inmotek\model\inmobiliaria\portales\Idealista $idealista){
        $this->idealista = $idealista;
        return $this;
    }    

    public function getFotocasa(){
        return $this->fotocasa;
    }    

    public function setFotocasa(\inmotek\model\inmobiliaria\portales\Fotocasa $fotocasa){
        $this->fotocasa = $fotocasa;
        return $this;
    }    

}