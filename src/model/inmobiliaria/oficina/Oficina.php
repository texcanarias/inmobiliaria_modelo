<?php 
declare(strict_types = 1);
namespace inmotek\model\inmobiliaria\oficina;

use \inmotek\model\inmobiliaria\oficina\usuario\UsuarioList;
use \inmotek\model\inmueble\Localizacion;
use inmotek\model\base\comunicacion\ComunicacionList;

class Oficina extends \inmotek\model\base\DuplaIdNombre {

    /**
     * @var bool Si es la oficina central
     */
    private bool $principal;

    /**
     * @var bool Si debe ser publicado en internet
     */
    private bool $publicar;

    /**
     * @var int Orden en los listados
     */
    private int $orden;

    /**
     *
     * @var \inmotek\model\inmueble\Localizacion Direccion de la inmobiliaria
     */
    private Localizacion $direccion;

    /**
     * @var \inmotek\model\inmobiliaria\oficina\usuario\UsuarioList Lista de usuarios
     */
    private UsuarioList $usuarios;

    /**
     * @var ComunicacionList Datos relacionados con las comunicaciones (telefono, fax ...)
     */
    private ComunicacionList $comunicacion;    

    public function __construct(?int $id, string $clave, string $name = "") {
        parent::__construct($id,$clave, $name);
    }
    public function getDireccion(): \inmotek\model\inmueble\Localizacion {
        return $this->direccion;
    }

    public function setDireccion(\inmotek\model\inmueble\Localizacion $direccion) :self {
        $this->direccion = $direccion;
        return $this;
    }

    public function getPrincipal() : bool {
        return $this->principal;
    }

    public function setPrincipal(bool $principal) : self{
        $this->principal = $principal;
        return $this;
    }

    public function getPublicar() : bool{
        return $this->publicar;
    }

    public function setPublicar(bool $publicar) : self{
        $this->publicar = $publicar;
        return $this;
    }

    public function getOrden() : int{
        return $this->orden;
    }

    public function setOrden(int $orden){
        $this->orden = $orden;
        return $this;
    }

    public function getUsuarios(): \inmotek\model\inmobiliaria\oficina\usuario\UsuarioList{
        return $this->usuarios;
    }

    public function setUsuarios(\inmotek\model\inmobiliaria\oficina\usuario\UsuarioList $usuarios) : self{
        $this->usuarios = $usuarios;
        return $this;
    }
    /**
     * Get relacionados con las comunicaciones (telefono, fax ...)
     *
     * @return  ComunicacionList
     */ 
    public function getComunicacion() : ComunicacionList
    {
        return $this->comunicacion;
    }

    /**
     * Set relacionados con las comunicaciones (telefono, fax ...)
     *
     * @param  ComunicacionList  $comunicacion  Datos relacionados con las comunicaciones (telefono, fax ...)
     *
     * @return  self
     */ 
    public function setComunicacion(ComunicacionList $comunicacion) : self
    {
        $this->comunicacion = $comunicacion;

        return $this;
    }
}