<?php namespace inmotek_test\phpunit;

use PHPUnit\Framework\TestCase;

final class oficinaTest extends TestCase {

    public function testOficina() {

        $direccion = \inmotek\model\inmueble\Localizacion::factoryLocalizacion('clave','ES', 35, 'Las Palmas', 35012, 'LPGC', '35012', 20.00, 20.01, 'Don Pío Coronado', '172', 'Unica', '2º', 'A');

        $usuarios = new \inmotek\model\inmobiliaria\oficina\usuario\UsuarioList();
        $usuarios[] = new \inmotek\model\inmobiliaria\oficina\usuario\Usuario(1, "Roberto Fernandez");
        $usuarios[0]->setDireccion($direccion);

        $oficinas = new \inmotek\model\inmobiliaria\oficina\Oficina(1,"Central");
        $oficinas->setDireccion($direccion);
        $oficinas->setPrincipal(true);
        $oficinas->setOrden(0);
        $oficinas->setUsuarios($usuarios);



        $this->assertTrue( 1 == $oficinas->getId() );
    }

}
