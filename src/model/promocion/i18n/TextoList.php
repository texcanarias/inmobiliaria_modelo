<?php 
declare(strict_types = 1);
namespace inmotek\model\promocion\i18n;

class TextoList extends \inmotek\model\inmueble\i18n\TextoList implements \Iterator, \ArrayAccess, \Countable {
    static public $TEXTO = 28;
    static public $DESCRIPCION = 29;
    static public $SITUACION_A = 30; //situaciontextoextra
    static public $COMO_LLEGAR_TEXTO_A = 31;//comollegartexto1
    static public $COMO_LLEGAR_TEXTO_B = 32;//comollegartexto2
    static public $COMO_LLEGAR_TEXTO_C = 33;//comollegartexto3
    static public $COMO_LLEGAR_TEXTO_D = 34;//comollegartexto4
    static public $TIPOLOGIA_A = 35; //tipologia1
    static public $TIPOLOGIA_B = 36; //tipologia2
    static public $TIPOLOGIA_C = 37; //tipologia3
    static public $TIPOLOGIA_D = 38; //tipologia4
    static public $INFOGRAFIA_A = 39; //infografia1
    static public $INFOGRAFIA_B = 40; //infografia2
    static public $INFOGRAFIA_C = 41; //infografia3
    static public $INFOGRAFIA_D = 42; //infografia4
}
