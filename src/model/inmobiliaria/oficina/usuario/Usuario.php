<?php 
declare(strict_types = 1);
namespace inmotek\model\inmobiliaria\oficina\usuario;

use \inmotek\model\base\comunicacion\Comunicacion;
use inmotek\model\base\comunicacion\ComunicacionList;
use \inmotek\model\inmueble\Localizacion;

class Usuario extends \inmotek\model\base\DuplaIdNombre {

    /**
     *
     * @var \inmotek\model\inmueble\Localizacion Direccion del usaurio
     */
    private Localizacion $direccion;


    /**
     * @var ComunicacionList Datos relacionados con las comunicaciones (telefono, fax ...)
     */
    private ComunicacionList $comunicacion;


    public function __construct(?int $id, string $clave, string $nombre="") {
        parent::__construct($id,$clave,$nombre);
    }
    public function getDireccion(): \inmotek\model\inmueble\Localizacion {
        return $this->direccion;
    }

    public function setDireccion(\inmotek\model\inmueble\Localizacion $direccion):self {
        $this->direccion = $direccion;
        return $this;
    }

    /**
     * Get relacionados con las comunicaciones (telefono, fax ...)
     *
     * @return  ComunicacionList
     */ 
    public function getComunicacion() : ComunicacionList
    {
        return $this->comunicacion;
    }

    /**
     * Set relacionados con las comunicaciones (telefono, fax ...)
     *
     * @param  ComunicacionList  $comunicacion  Datos relacionados con las comunicaciones (telefono, fax ...)
     *
     * @return  self
     */ 
    public function setComunicacion(ComunicacionList $comunicacion) : self
    {
        $this->comunicacion = $comunicacion;

        return $this;
    }
}
