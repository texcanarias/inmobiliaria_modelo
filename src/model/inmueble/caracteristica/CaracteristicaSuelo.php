<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble\caracteristica;

class CaracteristicaSuelo  extends Caracteristica{
    static $TIPO_TARIMA = 1;
    static $TIPO_PARQUET = 2;
    static $TIPO_TERRAZO = 3;
    static $TIPO_RESERVADO = 4;
    static $TIPO_MOQUETA = 5;
    static $TIPO_SINTASOL = 6;
    static $TIPO_GRES = 7;
    static $TIPO_MARMOL = 8;
    static $TIPO_ROBLE = 9;
    static $TIPO_PINO = 10;
    static $TIPO_ELONDO = 11;
    static $TIPO_PARQUET_X = 12; //@todo TIPO_PARQUET está dos veces. Ver que pasa con esto
    static $TIPO_CORCHO = 13;
    static $TIPO_HIDRA = 14;
    static $TIPO_NOGAL = 15;
    static $TIPO_LAMINADO = 16;
    static $TIPO_RESINA = 17;
    static $TIPO_EUCALIPTO = 18;
    static $TIPO_VINILO = 19;

    static $ESTADO_NUEVO = 1;
    static $ESTADO_BUEN_ESTADO = 2;
    static $ESTADO_NECESITA_BARNIZAR = 3;
    static $ESTADO_NECESITA_BARNIZAR_Y_LIJAR = 4;
    static $ESTADO_MUY_DETERIORADO = 5;
    static $ESTADO_REGULAR = 7;
 
}