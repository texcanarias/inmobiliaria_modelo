<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\ces;

/**
 * Modelo base para sostener la persistencia
 */
class Ces
{
    /**
     * Exento de certificación energética
     * @var bool
     *
     */
    private bool $exentoCes = false;

    /**
     * En trámite de sacarse el certificado
     * @var boolean
     *
     */
    private bool $enProceso = false;

    /**
     * Indicador numérico de CO2
     * @var CesCore
     *
     */
    private ?CesCore $co2 = null;

    /**
     * Indicador  de Kw
     * @var CesCore
     *
     */
    private ?CesCore $kw = null;

    /**
     * Número de registro de certificación energética
     * @var string
     *
     */
    private string $registryNumber = "";

    /**
     * Información adicional a la centificación energética
     * @var string
     *
     */
    private string $info = "";

    /**
     * Fecha de caducidad
     * @var \DateTime
     */
    private ?\DateTime $caducidad = null;

    private function __construct(bool $exentoCes, CesCore $co2, CesCore $kw, string $registryNumber, string $info, \DateTime $caducidad, bool $enProceso = false)
    {
        try {
            $this->exentoCes = $exentoCes;
            $this->co2 = $co2;
            $this->kw = $kw;
            $this->registryNumber = $registryNumber;
            $this->info = $info;
            $this->enProceso = $enProceso;
            $this->caducidad = $caducidad;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public static function factory(bool $exentoCes, CesCore $co2, CesCore $kw, string $registryNumber, string $info, \DateTime $caducidad, bool $enProceso = false)
    {
        try {
            $c = new self($exentoCes, $co2, $kw, $registryNumber, $info, $caducidad, $enProceso);
            return $c;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }


    /**
     * Get exento de certificación energética
     *
     * @return  bool
     */
    public function getExentoCes() : bool
    {
        return $this->exentoCes;
    }



    /**
     * Get indicador numérico de CO2
     *
     * @return  CesCore
     */
    public function getCo2() : CesCore
    {
        return $this->co2;
    }

    /**
     * Get indicador de Kw
     *
     * @return  CesCore
     */
    public function getKw() : CesCore
    {
        return $this->kw;
    }

    /**
     * Get número de registro de certificación energética
     *
     * @return  string
     */
    public function getRegistryNumber() : string
    {
        return $this->registryNumber;
    }

    /**
     * Get información adicional a la centificación energética
     *
     * @return  string
     */
    public function getInfo() : string
    {
        return $this->info;
    }

    /**
     * Recupera si el valor está en proceso
     * @return bool
     */
    public function getEnProceso() : bool
    {
        return $this->enProceso;
    }

    /**
     * Get fecha de caducidad
     *
     * @return  \DateTime
     */ 
    public function getCaducidad() : \DateTime
    {
        return $this->caducidad;
    }

    /**
     * Set fecha de caducidad
     *
     * @param  \DateTime;  $caducidad  Fecha de caducidad
     *
     * @return  self
     */ 
    public function setCaducidad(\DateTime $caducidad)
    {
        $this->caducidad = $caducidad;

        return $this;
    }
}
