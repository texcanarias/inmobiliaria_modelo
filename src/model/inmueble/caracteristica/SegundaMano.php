<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble\caracteristica;

class SegundaMano extends CaracteristicaInt {
    static $ITEMS = ["NUEVA" => 0,
                    "SEGUNDA_MANO" => 1,
                    "SIN_DEFINIR" => null
    ];

    static $SIN_DEFINIR = 0;    
    static $NUEVA = 1;
    static $SEGUNDA_MANO = 2;


    use TraitFactory;

    protected function isCorrecto(int $item)
    {
        return self::$SIN_DEFINIR <= $item AND self::$SEGUNDA_MANO >= $item;
    }

}
