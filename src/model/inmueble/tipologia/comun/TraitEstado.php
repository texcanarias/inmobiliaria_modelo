<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\tipologia\comun;

trait TraitEstado
{
    static $ESTADO_REFORMA_INTEGRAL = 1;
    static $ESTADO_REFORMA_NECESARIA = 2;
    static $ESTADO_BUEN_ESTADO = 3;
    static $ESTADO_MUY_BUEN_ESTADO = 4;
    static $ESTADO_RECIEN_REFORMADO = 5;
    static $ESTADO_NUEVO_O_SEMINUEVO = 6;

    private ?int $estado = null;

    /**
     * Obtiene el estado del inmueble
     * @return int
     */
    public function getEstado() : ?int
    {
        return $this->estado;
    }

    /**
     * Set el valor del estado
     * 
     * @param int Valor del estado
     * 
     * @return  self
     */
    public function setEstado(?int $valor) : self
    {
        if(null == $valor){
            return $this;
        }
        
        $isDentroRango = 6 >= $valor && 1 <= $valor;
        if (!$isDentroRango) {
            throw new \Exception("El estado del inmueble está fuera de rango.");
        }
        $this->estado = $valor;

        return $this;
    }
}
