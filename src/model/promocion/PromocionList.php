<?php 
declare(strict_types = 1);
namespace inmotek\model\promocion;
use \inmotek\model\base\ArrayStorage;

class PromocionList extends ArrayStorage {

    public function __construct() {
        parent::__construct('\inmotek\model\promocion\Promocion');
    }
}
