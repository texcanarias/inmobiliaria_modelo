<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble\caracteristica;

class Caracteristica{

    /**
     * tipo de caracteristica
     * @var int
     */
    private ?int $tipo = null;

    /**
     * testado de la caracteristica
     * @var int
     */
    private ?int $estado = null;

    /**
     * Get tipo de caracteristica
     *
     * @return  int
     */ 
    public function getTipo() : ?int
    {
        return $this->tipo;
    }

    /**
     * Set tipo de caracteristica
     *
     * @param  int  $tipo  tipo de caracteristica
     *
     * @return  self
     */ 
    public function setTipo(?int $tipo) : self
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get testado de la caracteristica
     *
     * @return  int
     */ 
    public function getEstado() : ?int
    {
        return $this->estado;
    }

    /**
     * Set testado de la caracteristica
     *
     * @param  int  $estado  testado de la caracteristica
     *
     * @return  self
     */ 
    public function setEstado(?int $estado) : self
    {
        $this->estado = $estado;

        return $this;
    }
}


