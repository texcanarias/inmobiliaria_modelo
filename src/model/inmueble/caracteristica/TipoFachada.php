<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble\caracteristica;

class TipoFachada  extends CaracteristicaInt {
    static $ITEMS =["Indefinido" => "0",
                    "Caravista" => "1",
                    "Raseada" => "2", 
                    "Plaqueta" => "3",
                    "Piedra" => "4",
                    "Monocapa" => "5", 
                    "Otros" => "6",
                    "Ventilada" => "7",
                    "Gresite" => "8",
                    "SATE" => "9"];

    static $INDEFINIDO = 0;
    static $CARAVISTA = 1;
    static $RASEADA = 2; 
    static $PLAQUETA = 3;
    static $PIEDRA = 4;
    static $MONOCAPA = 5; 
    static $OTROS = 6;
    static $VENTILADA = 7;
    static $GRESITE = 8;
    static $SATE = 9;

    use TraitFactory;

    protected function isCorrecto(int $item)
    {
        return self::$INDEFINIDO <= $item AND self::$SATE >= $item;
    }
}


