<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble;

class Inmueble
 extends \inmotek\model\base\DuplaIdNombre {
    
    /**
     * Identificador de la inmobiliaria
     * @var \inmotek\model\inmobiliaria\Inmobiliaria 
     *
     */                    
    private ?\inmotek\model\inmobiliaria\Inmobiliaria $inmobiliaria = null;  

    /**
     * Otro identificador del inmueble
     * @var string
     *
     */                                                       
    private string $ref0 = "";
       
    /**
     * Y otro identificador más del inmueble
     * @var string
     */
    private string $ref1 = "";

    /**
     * Operacines disponibles para el inmueble
     * @var \inmotek\model\inmueble\trade\TradeList
     */                    
    private ?\inmotek\model\inmueble\trade\TradeList $trade;

    /**
     * Tipologia del inmueble
     * @var \inmotek\model\inmueble\tipologia\InterfaceTipologia
     */
    private \inmotek\model\inmueble\tipologia\InterfaceTipologia $tipologia;


    /**
     * Localizacion del inmueble
     * @var \inmotek\model\inmueble\localizacion
     */
    private ?\inmotek\model\inmueble\localizacion $localizacion;
  
    /**
     * Fecha en la que se considera que el inmueble está disponible.
     * @var \DateTime
     */
    private ?\DateTime $disponibilidadAPartir; 


    /**
     * @var \inmotek\model\base\LogRegistro Fecha en las que se actualiza el inmueble
     */
    private \inmotek\model\base\LogRegistro $logRegistro;


    /**
     * @var \inmotek\model\inmobiliaria\oficina\usuario\Usuario Captador
     */
    private ?\inmotek\model\inmobiliaria\oficina\usuario\Usuario $captador = null;
    
    /**
     *
     * @var \inmotek\model\inmobiliaria\oficina\usuario\UsuarioList Lisado de gestionadores
     */
    private ?\inmotek\model\inmobiliaria\oficina\usuario\UsuarioList $gestionador = null;
    

    /**
     * Fotos del inmueble
     * @var \inmotek\model\inmueble\media\ImagenList
     * )
     */
    private ?\inmotek\model\inmueble\media\ImagenList $imagen;

    /**
     * Videos del inmueble
     * @var \inmotek\model\inmueble\media\VideoList
     */
    private ?\inmotek\model\inmueble\media\VideoList $video;

    /**
     * Documentos sobre inmueble
     * @var  \inmotek\model\inmueble\media\DocumentoList
     */
    private ?\inmotek\model\inmueble\media\DocumentoList $documento;

    /**
     * Planos sobre inmueble
     * @var \inmotek\model\inmueble\media\PlanoList
     */
    private ?\inmotek\model\inmueble\media\PlanoList $plano;

    /**
     * Textos de los inmuebles
     * @var \inmotek\model\inmueble\i18n\TextoList
     */    
    private ?\inmotek\model\inmueble\i18n\TextoList $textos;

    /**
     * Datos de configuracion del portal
     * @var \inmotek\model\portal\PortalVisibilidad
     */
    private ?\inmotek\model\portal\PortalVisibilidad $portalVisibilidad;

    protected function __construct(?int $id = null, string $clave , string $name) {
        parent::__construct($id, $clave, $name);
        $this->trade = new \inmotek\model\inmueble\trade\TradeList();
    }
    
    public function factoryInmueble(?int $id, string $name, \inmotek\model\inmobiliaria\Inmobiliaria $inmobiliaria){
        $i = new self($id, $inmobiliaria->getDb(), $name);
        $i->setInmobiliaria($inmobiliaria);
        $i->setRef0();
        $i->setRef1();
        return $i;
    }

    public function getReferencia() : string
    {
        return $this->name;
    }

    private function setRef0() : self {
        $this->ref0 = $this->inmobiliaria->getDb() . "-" . $this->id;
        return $this;
    }

    private function setRef1() : self{
        if("" != $this->inmobiliaria->getLetra()){
            $this->ref1 = $this->inmobiliaria->getLetra() . "-" . $this->id;
        }
        return $this;
    }

    public function getRef0() : string{
        return $this->ref0;
    }

    public function getRef1() : string{
        return $this->ref1;
    }

    /**
     * Registro de la inmobiliaria asignada al inmueble
     */
    private function setInmobiliaria(\inmotek\model\inmobiliaria\Inmobiliaria $inmobiliaria) : self
    {
        $this->inmobiliaria = $inmobiliaria;
        return $this;
    }

    /**
     * Obtener el valor de la inmobiliaria asignada
     */ 
    public function getInmobiliaria() : \inmotek\model\inmobiliaria\Inmobiliaria
    {
        return $this->inmobiliaria;
    }

    
    /**
     * Get localizacion del inmueble
     *
     * @return  \inmotek\model\inmueble\localizacion
     */ 
    public function getLocalizacion() : \inmotek\model\inmueble\localizacion
    {
        return $this->localizacion;
    }

    /**
     * Set localizacion del inmueble
     *
     * @param  \inmotek\model\inmueble\localizacion  $localizacion  Localizacion del inmueble
     *
     * @return  self
     */ 
    public function setLocalizacion(\inmotek\model\inmueble\localizacion $localizacion) : self
    {
        $this->localizacion = $localizacion;
        return $this;
    }


    public function setTrade(\inmotek\model\inmueble\trade\TradeList $trade) : self{
        $this->trade = $trade;
        return $this;
    }

    /**
     * Recupera el tipo de transaccion 
     * 
     * @return \inmotek\model\inmueble\trade\TradeList
     */
    public function getTrade() : \inmotek\model\inmueble\trade\TradeList{
        return $this->trade;
    }

    /**
     * Registro de la tipologia
     * @param \inmotek\model\inmueble\tipologia\InterfaceTipologia $tipologia
     */
    public function setTipologia(\inmotek\model\inmueble\tipologia\InterfaceTipologia $tipologia) : self{
        $this->tipologia = $tipologia;
        return $this;
    }
    
    /**
     * Recupera la tipología del inmueble
     * 
     * @return \inmotek\model\inmueble\tipologia\InterfaceTipologia
     */
    public function getTipologia() : \inmotek\model\inmueble\tipologia\InterfaceTipologia {
        return $this->tipologia;
    }
   
    public function setDisponibilidadAPartir(\DateTime $item) : self{
        $this->disponibilidadAPartir = $item;
        return $this;
    }

    public function getDisponibilidadAPartir() : \DateTime{
        return $this->disponibilidadAPartir;
    }

    /**
     * Get fecha en las que se actualiza el inmueble
     *
     * @return  \inmotek\model\base\LogRegistro
     */ 
    public function getLogRegistro() : \inmotek\model\base\LogRegistro
    {
        return $this->logRegistro;
    }

    /**
     * Set fecha en las que se actualiza el inmueble
     *
     * @param  \inmotek\model\base\LogRegistro  $logRegistro  Fecha en las que se actualiza el inmueble
     *
     * @return  self
     */ 
    public function setLogRegistro(\inmotek\model\base\LogRegistro $logRegistro) : self
    {
        $this->logRegistro = $logRegistro;

        return $this;
    }

    /**
     * Get listado de gestionadores
     *
     * @return  \inmotek\model\inmobiliaria\oficina\usuario\UsuarioList
     */ 
    public function getGestionador() : ?\inmotek\model\inmobiliaria\oficina\usuario\UsuarioList
    {
        if(null == $this->gestionador){
            $this->gestionador = new \inmotek\model\inmobiliaria\oficina\usuario\UsuarioList();
        }
        return $this->gestionador;
    }

    /**
     * Get captador
     *
     * @return  \inmotek\model\inmobiliaria\oficina\usuario\Usuario
     */ 
    public function getCaptador() : ?\inmotek\model\inmobiliaria\oficina\usuario\Usuario
    {
        return $this->captador;
    }

    public function setGestionador(?\inmotek\model\inmobiliaria\oficina\usuario\UsuarioList $items) : self{
        $this->gestionador = $items;
        return $this;
    }

    public function setCaptador(?\inmotek\model\inmobiliaria\oficina\usuario\Usuario $item) : self{
        $this->captador = $item;
        return $this;
    } 

    /**
     * Get Imagenes del inmueble
     *
     * @return  \inmotek\model\inmueble\media\ImagenList
     */ 
    public function getImagen() : \inmotek\model\inmueble\media\ImagenList
    {
        return $this->imagen;
    }

    /**
     * Set Imagenes del inmueble
     *
     * @param  \inmotek\model\inmueble\media\ImagenList
     * 
     * @return $this
     */ 
    public function setImagen(\inmotek\model\inmueble\media\ImagenList $item) : self
    {
        $this->imagen = $item;
        return $this;
    }


    /**
     * Get videos del inmueble
     *
     * @param  \inmotek\model\inmueble\media\VideoList
     * 
     * @return $this
     */ 
    public function setVideo(\inmotek\model\inmueble\media\VideoList $item) : self
    {
        $this->video = $item;
        return $this;
    }

    /**
     * Get videos del inmueble
     *
     * @return  \inmotek\model\inmueble\media\VideoList
     */ 
    public function getVideo() : \inmotek\model\inmueble\media\VideoList
    {
        return $this->video;
    }


    /**
     * Get documentos sobre inmueble
     *
     * @return  \inmotek\model\inmueble\media\DocumentoList
     */ 
    public function getDocumento() : \inmotek\model\inmueble\media\DocumentoList
    {
        return $this->documento;
    }

    /**
     * Get documentos del inmueble
     *
     * @param  \inmotek\model\inmueble\media\DocumentoList
     * 
     * @return $this
     */ 
    public function setDocumento(\inmotek\model\inmueble\media\DocumentoList $item) : self {
        $this->documento = $item;
        return $this;
    }

    /**
     * Get planos sobre inmueble
     *
     * @return  \inmotek\model\inmueble\media\PlanoList
     */ 
    public function getPlano() : \inmotek\model\inmueble\media\PlanoList
    {
        return $this->plano;
    }

    public function setPlano( \inmotek\model\inmueble\media\PlanoList $item) : self{
        $this->plano = $item;
        return $this;
    }    


    /**
     * Obtener los textos de los inmuebles
     * 
     * @return \inmotek\model\inmueble\i18n\TextoList
     */    
    public function getTextos() : \inmotek\model\inmueble\i18n\TextoList
    {
        return $this->textos;
    }

    public function setTextos(\inmotek\model\inmueble\i18n\TextoList $textos) : self
    {
        $this->textos = $textos;
        return $this;
    }

    /**
     * Get datos de configuracion del portal
     *
     * @return  \inmotek\model\portal\PortalVisibilidad
     */ 
    public function getPortalVisibilidad() : \inmotek\model\portal\PortalVisibilidad
    {
        return $this->portalVisibilidad;
    }

    /**
     * Set datos de configuracion del portal
     *
     * @param  \inmotek\model\portal\PortalVisibilidad  $portalVisibilidad  Datos de configuracion del portal
     *
     * @return  self
     */ 
    public function setPortalVisibilidad(\inmotek\model\portal\PortalVisibilidad $portalVisibilidad) : self
    {
        $this->portalVisibilidad = $portalVisibilidad;

        return $this;
    }
}
