<?php 
declare(strict_types = 1);
namespace inmotek\model\base;

/**
 * Modelo base para sostener la persistencia
 */
class ModelEntidad 
    implements InterfaceEntidad
{
    
    use \inmotek\model\base\TraitToString;
    
    /**
     * Nombre de la base de datos
     * @var string
     */
    protected string $db = "";


    /**
     * Identificador único en la tabla
     * @var int
     *
     */
    protected ?int $id = null;
        
    protected function __construct(?int $id = null, string $db = "") {
        $this->id = $id;
        $this->db = $db;
    }

    public function factoryModelEntidad(?int $id = null , string $db = "") : self{
        return new self($id, $db);
    }

    public function getId() : ?int {
        return $this->id;
    }

    public function setId(?int $id = null) : self {
        $this->id = $id;
        return $this;
    }

    public function getDb() : string{
        return $this->db;
    }

    public function setDb(string $db) : self{
        $this->db = $db;
        return $this;
    }

    public function isNew():bool{
        return null == $this->id;
    }
            
}