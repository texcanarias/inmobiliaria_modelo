<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\tipologia\comun;

use \inmotek\model\inmueble\caracteristica\{SegundaMano,Amueblado,AireAcondicionado,Calefaccion,
AguaCaliente,Aislamiento,DisponibilidadTrastero,DisponibilidadGarage,
CaracteristicaSuelo,CaracteristicaPared,CaracteristicaPuerta,CaracteristicaVentana};


trait TraitVivienda{
    /**
     * Tiene balcón
     * @var bool
     */
    private ?bool $balcon = null;

    /**
     * Tiene terraza
     * @var bool
     */
    private ?bool $terraza = null;

    /**
     * Tiene armario empotrado
     * @var bool
     */
    private ?bool $armarioEmpotrado = null;

    /**
     * Número de habitaciones
     * @var int
     */
    private ?int $numeroHabitaciones = null;

    /**
     * Segunda mano
     * @var SegundaMano
     */
    private ?SegundaMano $segundaMano = null;


    /**
     * Amueblado
     * @var Amueblado
     */
    private ?Amueblado $amueblado = null; 

    /**
     * Aire acondicionado
     * @var AireAcondicionado;
     */
    private ?AireAcondicionado $aireAcondicionado = null;

    /**
     * Calefaccion
     * @var Calefaccion
     */
    private ?Calefaccion $calefaccion = null;

    /**
     * Agua caliente
     * @var ?AguaCaliente
     */
    private ?AguaCaliente $aguaCaliente = null;

    /**
     * Aislamiento
     * @var ?Aislamiento
     */
    private ?Aislamiento $aislamiento = null;

    /**
     * Tiene chimenea
     * @var bool
     */
    private ?bool $chimenea = null;

    /**
     * Cocina equipada
     * @var bool
     */
    private ?bool $cocinaEquipada = null;

    /**
     * Con jardín
     * @var bool
     */
    private ?bool $jardin =  null;

    /**
     * Se permiten animales
     * @var bool
     */
    private ?bool $animalesPermitidos = null;

    /**
     * Disponibilidad de trastero
     * @var DisponibilidadTrastero
     */
    private ?DisponibilidadTrastero $disponibilidadTrastero = null;     

    /**
     * Disponibilidad de garage
     * @var DisponibilidadGarage
     */
    private ?DisponibilidadGarage $disponibilidadGarage = null;
    
    /**
     * Disponibilidad de techo altos
     * @var ?bool 
     */
    private ?bool $techosAltos = null;

    /**
     * Suelo
     * @var ?CaracteristicaSuelo
     */
    private ?CaracteristicaSuelo $caracteristicaSuelo = null;

    /**
     * Pared
     * @var ?CaracteristicaPared
     */
    private ?CaracteristicaPared $caracteristicaPared = null;

    /**
     * Puerta
     * @var ?CaracteristicaPuerta
     */
    private ?CaracteristicaPuerta $caracteristicaPuerta = null;

    /**
     * Ventana
     * @var ?CaracteristicaVentana
     */
    private ?CaracteristicaVentana $caracteristicaVentana = null;

    /**
     * Vistas al mar
     * @var ?bool
     */
    private ?bool $vistasAlMar = null;

    /**
     * Placas solares
     * @var ?bool
     */
    private ?bool $placasSolares = null;

    /**
     * Get tiene balcón
     *
     * @return  bool
     */ 
    public function getBalcon() : ?bool
    {
        return $this->balcon;
    }

    /**
     * Set tiene balcón
     *
     * @param  bool  $balcon  Tiene balcón
     *
     * @return  self
     */ 
    public function setBalcon(?bool $balcon) : self
    {
        $this->balcon = $balcon;

        return $this;
    }

    

    /**
     * Get tiene terraza
     *
     * @return  bool
     */ 
    public function getTerraza() : ?bool
    {
        return $this->terraza;
    }

    /**
     * Set tiene terraza
     *
     * @param  bool  $terraza  Tiene terraza
     *
     * @return  self
     */ 
    public function setTerraza(?bool $terraza) : self
    {
        $this->terraza = $terraza;

        return $this;
    }

    /**
     * Get tiene armario empotrado
     *
     * @return  bool
     */ 
    public function getArmarioEmpotrado() : ?bool
    {
        return $this->armarioEmpotrado;
    }

    /**
     * Set tiene armario empotrado
     *
     * @param  bool  $armarioEmpotrado  Tiene armario empotrado
     *
     * @return  self
     */ 
    public function setArmarioEmpotrado(?bool $armarioEmpotrado) : self
    {
        $this->armarioEmpotrado = $armarioEmpotrado;

        return $this;
    }

    /**
     * Get número de habitaciones
     *
     * @return  int
     */ 
    public function getNumeroHabitaciones() : ?int
    {
        return $this->numeroHabitaciones;
    }

    /**
     * Set número de habitaciones
     *
     * @param  int  $numeroHabitaciones  Número de habitaciones
     *
     * @return  self
     */ 
    public function setNumeroHabitaciones(?int $numeroHabitaciones) : self
    {
        $this->numeroHabitaciones = $numeroHabitaciones;

        return $this;
    }

    /**
     * Get segunda mano
     * @return bool
     */ 
    public function getSegundaMano() : ?SegundaMano
    {
        return $this->segundaMano;
    }

    /**
     * Set segunda mano
     *@param bool $segundaMano
     * @return  self
     */ 
    public function setSegundaMano(?SegundaMano $segundaMano) : self
    {
        $this->segundaMano = $segundaMano;

        return $this;
    }

    /**
     * Get amueblado
     *
     * @return  Amueblado
     */ 
    public function getAmueblado() : Amueblado
    {
        return $this->amueblado;
    }

    /**
     * Set amueblado
     *
     * @param  Amueblado  $amueblado  Amueblado
     *
     * @return  self
     */ 
    public function setAmueblado(?Amueblado $amueblado) : self
    {
        $this->amueblado = $amueblado;

        return $this;
    }

    /**
     * Get aire acondicionado
     *
     * @return AireAcondicionado;
     */ 
    public function getAireAcondicionado() : ?AireAcondicionado
    {
        return $this->aireAcondicionado;
    }

    /**
     * Set aire acondicionado
     *
     * @param  AireAcondicionado;  $aireAcondicionado  Aire acondicionado
     *
     * @return  self
     */ 
    public function setAireAcondicionado(?AireAcondicionado $aireAcondicionado) : self
    {
        $this->aireAcondicionado = $aireAcondicionado;

        return $this;
    }

    /**
     * Get calefaccion
     *
     * @return  Calefaccion
     */ 
    public function getCalefaccion() : Calefaccion
    {
        return $this->calefaccion;
    }

    /**
     * Set calefaccion
     *
     * @param Calefaccion  $calefaccion  Calefaccion
     *
     * @return  self
     */ 
    public function setCalefaccion(?Calefaccion $calefaccion) : self
    {
        $this->calefaccion = $calefaccion;

        return $this;
    }

    /**
     * Get agua caliente
     */ 
    public function getAguaCaliente() : ?AguaCaliente
    {
        return $this->aguaCaliente;
    }

    /**
     * Set agua caliente
     *
     * @return  self
     */ 
    public function setAguaCaliente(?AguaCaliente $aguaCaliente) : self
    {
        $this->aguaCaliente = $aguaCaliente;

        return $this;
    }

    /**
     * Get aislamiento
     */ 
    public function getAislamiento() : ?Aislamiento
    {
        return $this->aislamiento;
    }

    /**
     * Set aislamiento
     *
     * @return  self
     */ 
    public function setAislamiento(?Aislamiento $aislamiento) : self
    {
        $this->aislamiento = $aislamiento;

        return $this;
    }

    /**
     * Get tiene chimenea
     *
     * @return  bool
     */ 
    public function getChimenea() : bool
    {
        return $this->chimenea;
    }

    /**
     * Set tiene chimenea
     *
     * @param  bool  $chimenea  Tiene chimenea
     *
     * @return  self
     */ 
    public function setChimenea(?bool $chimenea) : self
    {
        $this->chimenea = $chimenea;

        return $this;
    }

    /**
     * Get cocina equipada
     *
     * @return  bool
     */ 
    public function getCocinaEquipada() : bool
    {
        return $this->cocinaEquipada;
    }

    /**
     * Set cocina equipada
     *
     * @param  bool  $cocinaEquipada  Cocina equipada
     *
     * @return  self
     */ 
    public function setCocinaEquipada(?bool $cocinaEquipada) : self
    {
        $this->cocinaEquipada = $cocinaEquipada;

        return $this;
    }

    /**
     * Get con jardín
     *
     * @return  bool
     */ 
    public function getJardin() : ?bool
    {
        return $this->jardin;
    }

    /**
     * Set con jardín
     *
     * @param  bool  $jardin  Con jardín
     *
     * @return  self
     */ 
    public function setJardin(?bool $jardin) : self
    {
        $this->jardin = $jardin;

        return $this;
    }


    /**
     * Get se permiten animales
     *
     * @return  bool
     */ 
    public function getAnimalesPermitidos() : ?bool
    {
        return $this->animalesPermitidos;
    }

    /**
     * Set se permiten animales
     *
     * @param  bool  $animalesPermitidos  Se permiten animales
     *
     * @return  self
     */ 
    public function setAnimalesPermitidos(?bool $animalesPermitidos) : self
    {
        $this->animalesPermitidos = $animalesPermitidos;

        return $this;
    }

    /**
     * Get disponibilidad de trastero
     */ 
    public function getDisponibilidadTrastero() : ?DisponibilidadTrastero
    {
        return $this->disponibilidadTrastero;
    }

    /**
     * Set disponibilidad de trastero
     *
     * @return  self
     */ 
    public function setDisponibilidadTrastero(?DisponibilidadTrastero $disponibilidadTrastero) : self
    {
        $this->disponibilidadTrastero = $disponibilidadTrastero;

        return $this;
    }

    /**
     * Get disponibilidad de garage
     *
     * @return DisponibilidadGarage
     */ 
    public function getDisponibilidadGarage() : ?DisponibilidadGarage
    {
        return $this->disponibilidadGarage;
    }

    /**
     * Set disponibilidad de garage
     *
     * @param  DisponibilidadGarage  $disponibilidadGarage  Disponibilidad de garage
     *
     * @return  self
     */ 
    public function setDisponibilidadGarage(?DisponibilidadGarage $disponibilidadGarage) : self
    {
        $this->disponibilidadGarage = $disponibilidadGarage;

        return $this;
    }

    /**
     * Get disponibilidad de techo altos
     *
     * @return  ?bool
     */ 
    public function getTechosAltos() : ?bool
    {
        return $this->techosAltos;
    }

    /**
     * Set disponibilidad de techo altos
     *
     * @param  ?bool  $techosAltos  Disponibilidad de techo altos
     *
     * @return  self
     */ 
    public function setTechosAltos(?bool $techosAltos) : self
    {
        $this->techosAltos = $techosAltos;

        return $this;
    }

    /**
     * Get suelo
     *
     * @return  ?CaracteristicaSuelo
     */ 
    public function getCaracteristicaSuelo() : ?CaracteristicaSuelo
    {
        return $this->caracteristicaSuelo;
    }

    /**
     * Set suelo
     *
     * @param  ?CaracteristicaSuelo  $caracteristicaSuelo  Suelo
     *
     * @return  self
     */ 
    public function setCaracteristicaSuelo(?CaracteristicaSuelo $caracteristicaSuelo) : self
    {
        $this->caracteristicaSuelo = $caracteristicaSuelo;

        return $this;
    }

    /**
     * Get pared
     *
     * @return  ?CaracteristicaPared
     */ 
    public function getCaracteristicaPared() : ?CaracteristicaPared
    {
        return $this->caracteristicaPared;
    }

    /**
     * Set pared
     *
     * @param  ?CaracteristicaPared  $caracteristicaPared  Pared
     *
     * @return  self
     */ 
    public function setCaracteristicaPared(?CaracteristicaPared $caracteristicaPared) : self 
    {
        $this->caracteristicaPared = $caracteristicaPared;

        return $this;
    }

    /**
     * Get puerta
     *
     * @return  ?CaracteristicaPuerta
     */ 
    public function getCaracteristicaPuerta() : ?CaracteristicaPuerta
    {
        return $this->caracteristicaPuerta;
    }

    /**
     * Set puerta
     *
     * @param  ?CaracteristicaPuerta  $caracteristicaPuerta  Puerta
     *
     * @return  self
     */ 
    public function setCaracteristicaPuerta(?CaracteristicaPuerta $caracteristicaPuerta) : self
    {
        $this->caracteristicaPuerta = $caracteristicaPuerta;

        return $this;
    }

    /**
     * Get ventana
     *
     * @return  ?CaracteristicaVentana
     */ 
    public function getCaracteristicaVentana() : ?CaracteristicaVentana
    {
        return $this->caracteristicaVentana;
    }

    /**
     * Set ventana
     *
     * @param  ?CaracteristicaVentana  $caracteristicaVentana  Ventana
     *
     * @return  self
     */ 
    public function setCaracteristicaVentana(?CaracteristicaVentana $caracteristicaVentana) : self
    {
        $this->caracteristicaVentana = $caracteristicaVentana;

        return $this;
    }

    /**
     * Get vistas al mar
     *
     * @return  ?bool
     */ 
    public function getVistasAlMar() : ?bool
    {
        return $this->vistasAlMar;
    }

    /**
     * Set vistas al mar
     *
     * @param  ?bool  $vistasAlMar  Vistas al mar
     *
     * @return  self
     */ 
    public function setVistasAlMar(?bool $vistasAlMar) : self
    {
        $this->vistasAlMar = $vistasAlMar;

        return $this;
    }

    /**
     * Get placas solares
     *
     * @return  ?bool
     */ 
    public function getPlacasSolares() : ?bool
    {
        return $this->placasSolares;
    }

    /**
     * Set placas solares
     *
     * @param  ?bool  $placasSolares  Placas solares
     *
     * @return  self
     */ 
    public function setPlacasSolares(?bool $placasSolares) : self
    {
        $this->placasSolares = $placasSolares;

        return $this;
    }
}