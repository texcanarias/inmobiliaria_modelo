<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble\i18n;

use inmotek\model\base\DuplaIdNombre;

class Texto extends DuplaIdNombre {
  
    public function factoryTexto(?int $id, string $clave, string $name){
        return new self($id, $clave, $name);
    }
}