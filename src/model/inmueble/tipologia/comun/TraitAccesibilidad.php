<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\tipologia\comun;

trait TraitAccesibilidad
{
    /**
     * Acceso adaptado a minusvalia
     * @bool
     */
    private ?bool $accesiblidadAcceso = null;

    /**
     * Uso adaptado a minusvalidos
     * @bool
     */
    private ?bool $accesibilidadUso = null;

    

    /**
     * Get acceso adaptado a minusvalia
     */ 
    public function getAccesiblidadAcceso() : ?bool
    {
        return $this->accesiblidadAcceso;
    }

    /**
     * Set acceso adaptado a minusvalia
     *
     * @return  self
     */ 
    public function setAccesiblidadAcceso(?bool $accesiblidadAcceso) : self
    {
        $this->accesiblidadAcceso = $accesiblidadAcceso;

        return $this;
    }

    /**
     * Get uso adaptado a minusvalidos
     */ 
    public function getAccesibilidadUso() : ?bool
    {
        return $this->accesibilidadUso;
    }

    /**
     * Set uso adaptado a minusvalidos
     *
     * @return  self
     */ 
    public function setAccesibilidadUso(?bool $accesibilidadUso) : self
    {
        $this->accesibilidadUso = $accesibilidadUso;

        return $this;
    }
}