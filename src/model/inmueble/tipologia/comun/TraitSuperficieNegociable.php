<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\tipologia\comun;

trait TraitSuperficieNegociable{
    /** 
     * Superficie negociable
     * @var int
     */
    private ?int $superficieNegociable = null;

    public function getSuperficieNegociable() : ?int{
        return $this->superficieNegociable;
    }

    public function setSuperficieNegociable(?int $superficieNegociable){
        if(null == $superficieNegociable){
            return $this;
        }

        if(0 <= $superficieNegociable ){
            $this->superficieNegociable = $superficieNegociable;
        }else{
            throw new \Exception("Error en la superficie negociable debe ser mayor que 0");
        }
        return $this;
    }
}