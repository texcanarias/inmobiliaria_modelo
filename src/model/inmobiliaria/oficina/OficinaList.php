<?php 
declare(strict_types = 1);
namespace inmotek\model\inmobiliaria\oficina;

use \inmotek\model\base\ArrayStorage;

class OficinaList extends ArrayStorage {

    public function __construct() {
        parent::__construct('\inmotek\model\inmobiliaria\oficina\Oficina');
    }
}
