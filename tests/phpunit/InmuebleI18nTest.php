<?php namespace inmotek_test\phpunit;

use PHPUnit\Framework\TestCase;
use inmotek\model\inmobiliaria\Inmobiliaria;
use inmotek\model\inmueble\i18n\Texto;
use inmotek\model\inmueble\i18n\TextoList;
use inmotek\model\inmueble\i18n\TraduccionList;

final class inmuebleI18nTest extends TestCase {

    private function getInmu(){
        $inmo = new Inmobiliaria(1,'PRO', "Pruebas y text,S.A.");
        $inmo->setLetra('PR');
        $inmo->setToken('zztop123');
        $inmo->setDemo(false);
        $inmo->setSistema(true);        

        $direccion = \inmotek\model\inmueble\Localizacion::factoryLocalizacion('test','ES', 35, 'Las Palmas', 35012, 'LPGC', '35012', 20.00, 20.01, 'Don Pío Coronado', '172', 'Unica', '2º', 'A');
        $inmo->setDireccion($direccion);

        $usuarios = new \inmotek\model\inmobiliaria\oficina\usuario\UsuarioList();
        $usuarios[] = new \inmotek\model\inmobiliaria\oficina\usuario\Usuario(1, "Roberto Fernandez");
        $usuarios[0]->setDireccion($direccion);

        $oficinas = new \inmotek\model\inmobiliaria\oficina\OficinaList();
        $oficinas[] = new \inmotek\model\inmobiliaria\oficina\Oficina(1,"Central");
        $oficinas[0]->setDireccion($direccion);
        $oficinas[0]->setPrincipal(true);
        $oficinas[0]->setOrden(0);
        $oficinas[0]->setUsuarios($usuarios);
        $inmo->setOficinas($oficinas);


        $inmueble = \inmotek\model\inmueble\Inmueble::factoryInmueble(1, "Casa palaciega",$inmo);
        return $inmueble;
    }

    public function testI18n(){
        $es = "Esto es un texto";
        $en = "This's a text";
        $ca = "Això és un text";
        $eu = "Hau testu bat da";
        $fr = "Ceci est un texte";

        $traduccion = new TraduccionList();
        $traduccion[TraduccionList::$ES] = Texto::factoryTexto(1, 'test', $es);
        $traduccion[TraduccionList::$EN] = Texto::factoryTexto(2, 'test', $en);
        $traduccion[TraduccionList::$CA] = Texto::factoryTexto(3, 'test', $ca);
        $traduccion[TraduccionList::$EU] = Texto::factoryTexto(4, 'test', $eu);
        $traduccion[TraduccionList::$FR] = Texto::factoryTexto(5, 'test', $fr);

        $coleccion = new TextoList();
        $coleccion[TextoList::$NOTAS_ENVIO] = $traduccion;

        $this->assertTrue( "Esto es un texto" == $coleccion[TextoList::$NOTAS_ENVIO][TraduccionList::$ES]->getName() );
        $this->assertTrue( "This's a text" == $coleccion[TextoList::$NOTAS_ENVIO][TraduccionList::$EN]->getName() );

        $inmu = $this->getInmu();
        $inmu->setTextos($coleccion);
        $this->assertTrue( "Esto es un texto" == $inmu->getTextos()[TextoList::$NOTAS_ENVIO][TraduccionList::$ES]->getName() );
        $this->assertTrue( "This's a text" == $inmu->getTextos()[TextoList::$NOTAS_ENVIO][TraduccionList::$EN]->getName() );


    }
}
