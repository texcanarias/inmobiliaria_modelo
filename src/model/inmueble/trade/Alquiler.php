<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\trade;

class Alquiler extends Trade{
    static $PERIODO_DIA = 1;
    static $PERIODO_SEMANA = 2;
    static $PERIODO_QUINCENA = 3;
    static $PERIODO_MES = 4;
    static $PERIODO_HORA = 5;
    static $PERIODO_ANYO = 6;

    static $FIANZA_PORCENTAJE = 1; //pc
    static $FIANZA_EUROS = 2; //e
    static $FIANZA_MESES = 3; //m
    static $FIANZA_NO = 4; //nr

    static $RESERVA_PORCENTAJE = 1; //pc
    static $RESERVA_EUROS = 2; //e
    static $RESERVA_MESES = 3; //m
    static $RESERVA_NO = 4; //nr

    /**
     * Periodo del alquiler
     * @var int
     */
    private $periodo;

    /**
     * Fianza periodo
     * @var int
     */
    private $fianza_periodo;

    /**
     * Fianza total
     * @var float
     */
    private $fianza_total;

    /**
     * Aval periodo
     * @var int
     */
    private $aval_periodo;

    /**
     * Aval total
     * @var float
     */
    private $aval_total;

    /**
     * Reserva periodo
     * @var int
     */
    private $reserva_periodo;

    /**
     * Reserva total
     * @var float
     */
    private $reserva_total;

    /**
     * Opcion de compra
     * @var bool
     */
    private $opcionCompra;

    /**
     * Amueblado
     * @var \inmotek\model\inmueble\caracteristica\Amueblado
     */
    private $amueblado;

    public function setOpcionCompra(bool $opcion){
        $this->opcionCompra = $opcion;
        return $this;
    }

    public function getOpcionCompra() : bool{
        return $this->opcionCompra;
    }

    /**
     * Get periodo del alquiler
     *
     * @return  int
     */ 
    public function getPeriodo() : int
    {
        return $this->periodo;
    }

    /**
     * Set periodo del alquiler
     *
     * @param  int  $periodo  Periodo del alquiler
     *
     * @return  self
     */ 
    public function setPeriodo(int $periodo)
    {
        $this->periodo = $periodo;

        return $this;
    }

    /**
     * Get fianza periodo
     *
     * @return  int
     */ 
    public function getFianza_periodo() : int
    {
        return $this->fianza_periodo;
    }

    /**
     * Set fianza periodo
     *
     * @param  int  $fianza_periodo  Fianza periodo
     *
     * @return  self
     */ 
    public function setFianza_periodo(int $fianza_periodo)
    {
        $this->fianza_periodo = $fianza_periodo;

        return $this;
    }

    /**
     * Get fianza total
     *
     * @return  float
     */ 
    public function getFianza_total() : float
    {
        return $this->fianza_total;
    }

    /**
     * Set fianza total
     *
     * @param  float  $fianza_total  Fianza total
     *
     * @return  self
     */ 
    public function setFianza_total(float $fianza_total)
    {
        $this->fianza_total = $fianza_total;

        return $this;
    }

    /**
     * Get aval periodo
     *
     * @return  int
     */ 
    public function getAval_periodo() : int
    {
        return $this->aval_periodo;
    }

    /**
     * Set aval periodo
     *
     * @param  int  $aval_periodo  Aval periodo
     *
     * @return  self
     */ 
    public function setAval_periodo(int $aval_periodo)
    {
        $this->aval_periodo = $aval_periodo;

        return $this;
    }

    /**
     * Get aval total
     *
     * @return  float
     */ 
    public function getAval_total() : float
    {
        return $this->aval_total;
    }

    /**
     * Set aval total
     *
     * @param  float  $aval_total  Aval total
     *
     * @return  self
     */ 
    public function setAval_total(float $aval_total)
    {
        $this->aval_total = $aval_total;

        return $this;
    }

    /**
     * Get reserva periodo
     *
     * @return  int
     */ 
    public function getReserva_periodo() : int
    {
        return $this->reserva_periodo;
    }

    /**
     * Set reserva periodo
     *
     * @param  int  $reserva_periodo  Reserva periodo
     *
     * @return  self
     */ 
    public function setReserva_periodo(int $reserva_periodo)
    {
        $this->reserva_periodo = $reserva_periodo;

        return $this;
    }

    /**
     * Get reserva total
     *
     * @return  float
     */ 
    public function getReserva_total() : float
    {
        return $this->reserva_total;
    }

    /**
     * Set reserva total
     *
     * @param  float  $reserva_total  Reserva total
     *
     * @return  self
     */ 
    public function setReserva_total(float $reserva_total)
    {
        $this->reserva_total = $reserva_total;

        return $this;
    }

    

    /**
     * Get amueblado
     *
     * @return  \inmotek\model\inmueble\caracteristica\Amueblado
     */ 
    public function getAmueblado() : \inmotek\model\inmueble\caracteristica\Amueblado
    {
        return $this->amueblado;
    }

    /**
     * Set amueblado
     *
     * @param  \inmotek\model\inmueble\caracteristica\Amueblado  $amueblado  Amueblado
     *
     * @return  self
     */ 
    public function setAmueblado(\inmotek\model\inmueble\caracteristica\Amueblado $amueblado)
    {
        $this->amueblado = $amueblado;

        return $this;
    }
}