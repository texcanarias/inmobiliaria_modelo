<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\caracteristica;

class Volumen
{

    /**
     * Largo de la plaza en metros
     * @var int
     */
    private int $profundidad = 0;

    /**
     * Alto de la plaza en metros
     * @var int
     */
    private int $altura = 0;

    /**
     * Ancho de la plaza en metros
     * @var int
     */
    private int $anchura = 0;


    private function __construct( int $profundidad = 0, int $anchura = 0, int $altura = 0 )
    {
        try {
            $this->setProfundidad($profundidad);
            $this->setAltura($altura);
            $this->setAnchura($anchura);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function factory( int $profundidad = 0, int $anchura = 0, int $altura = 0 ){
        try {
            return new self($profundidad,$anchura,$altura);
        } catch (\Exception $ex) {
            throw $ex;
        }

    }

    /**
     * Get largo de la plaza en metros
     *
     * @return  int
     */
    public function getProfundidad() : int
    {
        return $this->profundidad;
    }

    /**
     * Set largo de la plaza en metros
     *
     * @param  int  $largo  Largo de la plaza en metros
     *
     * @return  self
     */
    public function setProfundidad(int $profundidad)
    {
        if (0 > $profundidad) {
            throw new \Exception("La profundidad debe ser mayor que 0");
        }
        $this->profundidad = $profundidad;

        return $this;
    }

    /**
     * Get alto de la plaza en metros
     *
     * @return  int
     */
    public function getAltura() : int
    {
        return $this->altura;
    }

    /**
     * Set alto de la plaza en metros
     *
     * @param  int  $alto  Alto de la plaza en metros
     *
     * @return  self
     */
    public function setAltura(int $altura)
    {
        if (0 > $altura) {
            throw new \Exception("El alto debe ser mayor que 0");
        }
        $this->altura = $altura;

        return $this;
    }

    /**
     * Get ancho de la plaza en metros
     *
     * @return  int
     */
    public function getAnchura() : int
    {
        return $this->anchura;
    }

    /**
     * Set ancho de la plaza en metros
     *
     * @param  int  $ancho  Ancho de la plaza en metros
     *
     * @return  self
     */
    public function setAnchura(int $anchura)
    {
        if (0 > $anchura) {
            throw new \Exception("La anchura debe ser mayor que 0");
        }
        $this->anchura = $anchura;

        return $this;
    }

    /**
     * Recupera la superficie en m2
     * @return int
     */
    public function getSuperficie(): int
    {
        return $this->anchura * $this->profundidad;
    }

    /**
     * Recupera el volumen en m2
     * @return int
     */
    public function getVolumen(): int
    {
        return $this->anchura * $this->profundidad * $this->altura;
    }
}
