<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\tipologia\comun;

trait TraitAnyoConstruccion{
    /**
     * Año de construcción del inmueble
     * @var int
     */
    private ?int $anyoConstruccion = null;

    public function getAnyoConstruccion() : ?int{
        return $this->anyoConstruccion;
    }

    public function setAnyoConstruccion(?int $anyoConstruccion) : self{
        if(null == $anyoConstruccion){
            return $this;
        }

        if(1500 <= $anyoConstruccion && (date('Y') + 5) >= $anyoConstruccion){
            $this->anyoConstruccion = $anyoConstruccion;
        }
        else{
            throw new \Exception("Error en la fecha de construcción debe estar entre el año actual y el 1500");
        }
        return $this;
    }
}