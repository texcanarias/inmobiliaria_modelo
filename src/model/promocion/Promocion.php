<?php 
declare(strict_types = 1);
namespace inmotek\model\promocion;

use \inmotek\model\base\comunicacion\ComunicacionList;

class Promocion
 extends \inmotek\model\base\DuplaIdNombre {
    
    /**
     * Identificador de la inmobiliaria
     * @var \inmotek\model\inmobiliaria\Inmobiliaria 
     *
     */                    
    private ?\inmotek\model\inmobiliaria\Inmobiliaria $inmobiliaria = null;  
      
    /**
     * Localizacion de la promocion
     * @var \inmotek\model\inmueble\localizacion
     */
    private ?\inmotek\model\inmueble\localizacion $localizacion;
  

    /**
     * @var \inmotek\model\base\LogRegistro Fecha en las que se actualiza la promocion
     */
    private \inmotek\model\base\LogRegistro $logRegistro;


    /**
     * @var inmotek\model\inmobiliaria\oficina\Oficina Oficina de la que depende la promocion
     */
    private ?\inmotek\model\inmobiliaria\oficina\Oficina $oficina;

    /**
     * @var \inmotek\model\inmobiliaria\oficina\usuario\Usuario Captador
     */
    private ?\inmotek\model\inmobiliaria\oficina\usuario\Usuario $captador = null;
    
    /**
     *
     * @var \inmotek\model\inmobiliaria\oficina\usuario\UsuarioList Lisado de gestionadores
     */
    private ?\inmotek\model\inmobiliaria\oficina\usuario\UsuarioList $gestionador = null;
    

    /**
     * Fotos de la promocion
     * @var \inmotek\model\inmueble\media\ImagenList
     * )
     */
    private ?\inmotek\model\inmueble\media\ImagenList $imagen;

    /**
     * Videos del inmueble
     * @var \inmotek\model\inmueble\media\VideoList
     */
    private ?\inmotek\model\inmueble\media\VideoList $video;

    /**
     * Documentos sobre inmueble
     * @var  \inmotek\model\inmueble\media\DocumentoList
     */
    private ?\inmotek\model\inmueble\media\DocumentoList $documento;

    /**
     * Planos sobre inmueble
     * @var \inmotek\model\inmueble\media\PlanoList
     */
    private ?\inmotek\model\inmueble\media\PlanoList $plano;

    /**
     * Textos de los inmuebles
     * @var \inmotek\model\promocion\i18n\TextoList
     */    
    private ?\inmotek\model\promocion\i18n\TextoList $textos;

    /**
     * Listado de inmuebles relacionados con la promoción
     * @var \inmotek\model\inmueble\InmuebleList
     */
    private ?\inmotek\model\inmueble\InmuebleList $inmuebles;


    /**
     * Nombre de la promotora
     * @var ?string 
     */
    private ?string $promotora;

    /**
     * Nombre de la financiera
     * @var ?string 
     */
    private ?string $financiera;

    /**
     * Fase de la promocion
     * @var ?string
     */
    private ?string $fase;

    
    private ?ComunicacionList $comunicaciones;

    protected function __construct(?int $id = null, string $clave , string $name) {
        parent::__construct($id, $clave, $name);
    }
    
    /**
     * Factoria de la promociones
     * @param int $id Identificador de la promocion
     * @param string $name 
     * @param \inmotek\model\inmobiliaria\Inmobiliaria $inmobiliaria
     * 
     * @return Promocion
     */
    public function factoryPromocion(?int $id, string $name, \inmotek\model\inmobiliaria\Inmobiliaria $inmobiliaria){
        $i = new self($id, $inmobiliaria->getDb(), $name);
        $i->setInmobiliaria($inmobiliaria);
        return $i;
    }

    public function getReferencia() : string
    {
        return $this->name;
    }

    /**
     * Registro de la inmobiliaria asignada al inmueble
     */
    private function setInmobiliaria(\inmotek\model\inmobiliaria\Inmobiliaria $inmobiliaria) : self
    {
        $this->inmobiliaria = $inmobiliaria;
        return $this;
    }

    /**
     * Obtener el valor de la inmobiliaria asignada
     */ 
    public function getInmobiliaria() : \inmotek\model\inmobiliaria\Inmobiliaria
    {
        return $this->inmobiliaria;
    }

    
    /**
     * Get localizacion de la promocion
     *
     * @return  \inmotek\model\inmueble\localizacion
     */ 
    public function getLocalizacion() : \inmotek\model\inmueble\localizacion
    {
        return $this->localizacion;
    }

    /**
     * Set localizacion de la promocion
     *
     * @param  \inmotek\model\inmueble\localizacion  $localizacion  Localizacion de la promocion
     *
     * @return  self
     */ 
    public function setLocalizacion(\inmotek\model\inmueble\localizacion $localizacion) : self
    {
        $this->localizacion = $localizacion;
        return $this;
    }

    /**
     * Get fecha en las que se actualiza la promocion
     *
     * @return  \inmotek\model\base\LogRegistro
     */ 
    public function getLogRegistro() : \inmotek\model\base\LogRegistro
    {
        return $this->logRegistro;
    }

    /**
     * Set fecha en las que se actualiza la promocion
     *
     * @param  \inmotek\model\base\LogRegistro  $logRegistro  Fecha en las que se actualiza la promocion
     *
     * @return  self
     */ 
    public function setLogRegistro(\inmotek\model\base\LogRegistro $logRegistro) : self
    {
        $this->logRegistro = $logRegistro;

        return $this;
    }

    /**
     * Get listado de gestionadores
     *
     * @return  \inmotek\model\inmobiliaria\oficina\usuario\UsuarioList
     */ 
    public function getGestionador() : \inmotek\model\inmobiliaria\oficina\usuario\UsuarioList
    {
        if(null == $this->gestionador){
            $this->gestionador = new \inmotek\model\inmobiliaria\oficina\usuario\UsuarioList();
        }
        return $this->gestionador;
    }

    /**
     * Get captador
     *
     * @return  \inmotek\model\inmobiliaria\oficina\usuario\Usuario
     */ 
    public function getCaptador() : \inmotek\model\inmobiliaria\oficina\usuario\Usuario
    {
        return $this->captador;
    }

    public function setGestionador(?\inmotek\model\inmobiliaria\oficina\usuario\UsuarioList $items) : self{
        $this->gestionador = $items;
        return $this;
    }

    public function setCaptador(?\inmotek\model\inmobiliaria\oficina\usuario\Usuario $item) : self{
        $this->captador = $item;
        return $this;
    } 

    /**
     * Get Imagenes de la promocion
     *
     * @return  \inmotek\model\inmueble\media\ImagenList
     */ 
    public function getImagen() : ?\inmotek\model\inmueble\media\ImagenList
    {
        return $this->imagen;
    }

    /**
     * Set Imagenes de la promocion
     *
     * @param  \inmotek\model\inmueble\media\ImagenList
     * 
     * @return $this
     */ 
    public function setImagen(?\inmotek\model\inmueble\media\ImagenList $item) : self
    {
        $this->imagen = $item;
        return $this;
    }


    /**
     * Get videos de la promocion
     *
     * @param  \inmotek\model\inmueble\media\VideoList
     * 
     * @return $this
     */ 
    public function setVideo(?\inmotek\model\inmueble\media\VideoList $item) : self
    {
        $this->video = $item;
        return $this;
    }

    /**
     * Get videos de la promocion
     *
     * @return  \inmotek\model\inmueble\media\VideoList
     */ 
    public function getVideo() : ?\inmotek\model\inmueble\media\VideoList
    {
        return $this->video;
    }


    /**
     * Get documentos sobre la promocion
     *
     * @return  \inmotek\model\inmueble\media\DocumentoList
     */ 
    public function getDocumento() : ?\inmotek\model\inmueble\media\DocumentoList
    {
        return $this->documento;
    }

    /**
     * Get documentos de la promocion
     *
     * @param  \inmotek\model\inmueble\media\DocumentoList
     * 
     * @return $this
     */ 
    public function setDocumento(?\inmotek\model\inmueble\media\DocumentoList $item) : self {
        $this->documento = $item;
        return $this;
    }

    /**
     * Get planos sobre la promocion
     *
     * @return  \inmotek\model\inmueble\media\PlanoList
     */ 
    public function getPlano() : ?\inmotek\model\inmueble\media\PlanoList
    {
        return $this->plano;
    }

    public function setPlano( ?\inmotek\model\inmueble\media\PlanoList $item) : self{
        $this->plano = $item;
        return $this;
    }    


    /**
     * Obtener los textos de la promocion
     * 
     * @return \inmotek\model\promocion\i18n\TextoList
     */    
    public function getTextos() : ?\inmotek\model\promocion\i18n\TextoList
    {
        return $this->textos;
    }

    public function setTextos(?\inmotek\model\promocion\i18n\TextoList $textos) : self
    {
        $this->textos = $textos;
        return $this;
    }

    /**
     * Get listado de inmuebles relacionados con la promoción
     *
     * @return  \inmotek\model\inmueble\InmuebleList
     */ 
    public function getInmuebles() : \inmotek\model\inmueble\InmuebleList
    {
        return $this->inmuebles;
    }

    /**
     * Set listado de inmuebles relacionados con la promoción
     *
     * @param  \inmotek\model\inmueble\InmuebleList  $inmueble  Listado de inmuebles relacionados con la promoción
     *
     * @return  self
     */ 
    public function setInmuebles(?\inmotek\model\inmueble\InmuebleList $inmuebles) : self
    {
        $this->inmuebles = $inmuebles;

        return $this;
    }

    /**
     * Get oficina de la que depende la promocion
     *
     * @return  inmotek\model\inmobiliaria\oficina\Oficina
     */ 
    public function getOficina() : ?\inmotek\model\inmobiliaria\oficina\Oficina
    {
        return $this->oficina;
    }

    /**
     * Set oficina de la que depende la promocion
     *
     * @param  inmotek\model\inmobiliaria\oficina\Oficina  $oficina  Oficina de la que depende la promocion
     *
     * @return  self
     */ 
    public function setOficina(?\inmotek\model\inmobiliaria\oficina\Oficina $oficina) : self
    {
        $this->oficina = $oficina;

        return $this;
    }

    /**
     * Get nombre de la promotora
     *
     * @return  ?string
     */ 
    public function getPromotora() : ?string
    {
        return $this->promotora;
    }

    /**
     * Set nombre de la promotora
     *
     * @param  ?string  $promotora  Nombre de la promotora
     *
     * @return  self
     */ 
    public function setPromotora(?string $promotora) : self
    {
        $this->promotora = $promotora;

        return $this;
    }

    /**
     * Get nombre de la financiera
     *
     * @return  ?string
     */ 
    public function getFinanciera() : ?string
    {
        return $this->financiera;
    }

    /**
     * Set nombre de la financiera
     *
     * @param  ?string  $financiera  Nombre de la financiera
     *
     * @return  self
     */ 
    public function setFinanciera(?string $financiera) : self
    {
        $this->financiera = $financiera;

        return $this;
    }

    /**
     * Get fase de la promocion
     *
     * @return  ?string
     */ 
    public function getFase() : ?string
    {
        return $this->fase;
    }

    /**
     * Set fase de la promocion
     *
     * @param  ?string  $fase  Fase de la promocion
     *
     * @return  self
     */ 
    public function setFase(?string $fase) : self
    {
        $this->fase = $fase;

        return $this;
    }

    /**
     * Get the value of comunicaciones
     */ 
    public function getComunicaciones() : ?ComunicacionList
    {
        return $this->comunicaciones;
    }

    /**
     * Set the value of comunicaciones
     *
     * @return  self
     */ 
    public function setComunicaciones(?ComunicacionList $comunicaciones) : self
    {
        $this->comunicaciones = $comunicaciones;

        return $this;
    }
}
