<?php 
declare(strict_types = 1);
namespace inmotek\model\cms;

use \inmotek\model\inmobiliaria\Inmobiliaria;
use \inmotek\model\base\LogRegistro;

class Cms 
    implements InterfaceCms{
        
    /**
     * Identificador de la inmobiliaria
     * @var int
     */
    private int $idInmobiliaria;

    /**
     * Identificador de la inmobiliaria
     * @var ?Inmobiliaria 
     *
     */                    
    private ?Inmobiliaria $inmobiliaria = null;  

    /**
     * Dominio
     * @var string
     */
    private string $domain = "";

    /**
     * Subdominio
     * @var string
     */
    private string $subdomain = "";

    /**
     * Arreglo con los idiomas en los que se puede escribir en la web
     *
     * @var array
     */
    private $idiomas = [];

    /**
     * Fechas de registro del CMS Creation Update Delete
     * 
     * @var LogRegistro 
     */
    private LogRegistro $logRegistro;


    /**
     * Get identificador de la inmobiliaria
     *
     * @return  int
     */ 
    public function getIdInmobiliaria() : int
    {
        return $this->idInmobiliaria;
    }

    /**
     * Set identificador de la inmobiliaria
     *
     * @param  int  $idInmobiliaria  Identificador de la inmobiliaria
     *
     * @return  self
     */ 
    public function setIdInmobiliaria(int $idInmobiliaria) : self
    {
        $this->idInmobiliaria = $idInmobiliaria;

        return $this;
    }

    /**
     * Get the value of inmobiliaria
     */ 
    public function getInmobiliaria() : Inmobiliaria
    {
        return $this->inmobiliaria;
    }

    /**
     * Set the value of inmobiliaria
     *
     * @return  self
     */ 
    public function setInmobiliaria(Inmobiliaria $inmobiliaria) : self
    {
        $this->inmobiliaria = $inmobiliaria;

        return $this;
    }

    /**
     * Get dominio
     *
     * @return  string
     */ 
    public function getDomain() : string
    {
        return $this->domain;
    }

    /**
     * Set dominio
     *
     * @param  string  $domain  Dominio
     *
     * @return  self
     */ 
    public function setDomain(string $domain) : self
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Get subdominio
     *
     * @return  string
     */ 
    public function getSubdomain() : string
    {
        return $this->subdomain;
    }

    /**
     * Set subdominio
     *
     * @param  string  $subdomain  Subdominio
     *
     * @return  self
     */ 
    public function setSubdomain(string $subdomain) : self
    {
        $this->subdomain = $subdomain;

        return $this;
    }

    /**
     * Get arreglo con los idiomas en los que se puede escribir en la web
     *
     * @return  array
     */ 
    public function getIdiomas()
    {
        return $this->idiomas;
    }

    /**
     * Set arreglo con los idiomas en los que se puede escribir en la web
     *
     * @param  array  $idiomas  Arreglo con los idiomas en los que se puede escribir en la web
     *
     * @return  self
     */ 
    public function setIdiomas(array $idiomas)
    {
        $this->idiomas = $idiomas;

        return $this;
    }

    /**
     * Get fechas de registro del CMS Creation Update Delete
     *
     * @return  LogRegistro
     */ 
    public function getLogRegistro() : LogRegistro
    {
        return $this->logRegistro;
    }

    /**
     * Set fechas de registro del CMS Creation Update Delete
     *
     * @param  \inmotek\model\base\LogRegistro  $logRegistro  Fechas de registro del CMS Creation Update Delete
     *
     * @return  self
     */ 
    public function setLogRegistro(LogRegistro $logRegistro)
    {
        $this->logRegistro = $logRegistro;

        return $this;
    }
}