<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\tipologia;

class Trastero implements InterfaceTipologia{
    public static $SUBTIPO_TRASTERO = 0;
  
    use comun\TraitSubtipo;
    use comun\TraitEstado;    
    use comun\TraitSuperficieConstruida;
    use comun\TraitSeguridad;

    /**
     * Acceso 24 horas
     * @var bool
     */
    private ?bool $acceso24h = null;

    /**
     * Altura del trastero
     * @var int
     */
    private ?int $altura = null;

    /**
     * Muelle de carga
     * @var bool
     */
    private ?bool $muelleCarga = null;

    /**
    * @var bool Si hay preinstalacion electrica
    */
   private ?bool $electricidad = null;

    function __construct()
    {
        $this->subtipo = self::$SUBTIPO_TRASTERO;
    }

    public function getTipologia(){
        return $this;
    }

    public function getTipo() : string{
        return "Trastero";
    }

    /**
     * Get acceso 24 horas
     *
     * @return  bool
     */ 
    public function getAcceso24h() : ?bool
    {
        return $this->acceso24h;
    }

    /**
     * Set acceso 24 horas
     *
     * @param  bool  $acceso24h  Acceso 24 horas
     *
     * @return  self
     */ 
    public function setAcceso24h(?bool $acceso24h) : self
    {
        $this->acceso24h = $acceso24h;

        return $this;
    }

    /**
     * Get altura del trastero
     *
     * @return  int
     */ 
    public function getAltura() : ?int
    {
        return $this->altura;
    }

    /**
     * Set altura del trastero
     *
     * @param  int  $altura  Altura del trastero
     *
     * @return  self
     */ 
    public function setAltura(?int $altura) : self
    {
        $this->altura = $altura;

        return $this;
    }

    /**
     * Get muelle de carga
     *
     * @return  bool
     */ 
    public function getMuelleCarga() : ?bool
    {
        return $this->muelleCarga;
    }

    /**
     * Set muelle de carga
     *
     * @param  bool  $muelleCarga  Muelle de carga
     *
     * @return  self
     */ 
    public function setMuelleCarga(?bool $muelleCarga) : self
    {
        $this->muelleCarga = $muelleCarga;

        return $this;
    }


   /**
    * Get si hay preinstalacion electrica
    *
    * @return  bool
    */ 
   public function getElectricidad() : ?bool
   {
      return $this->electricidad;
   }

   /**
    * Set si hay preinstalacion electrica
    *
    * @param  bool  $electricidad  Si hay preinstalacion electrica
    *
    * @return  self
    */ 
   public function setElectricidad(?bool $electricidad) : self
   {
      $this->electricidad = $electricidad;

      return $this;
   }
}
