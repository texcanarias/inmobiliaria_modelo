<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\tipologia\comun;

use \inmotek\model\inmueble\ces\Ces;

trait TraitCes{
    /**
     * Valores del certificado CES para los inmuebles que lo necesiten
     * @var \inmotek\model\inmueble\ces\Ces
     */
    private ?Ces $ces;

    public function getCes() : ?Ces{
        return $this->ces;
    }

    public function setCes(?Ces $ces){
        $this->ces = $ces;
        return $this;
    }
}