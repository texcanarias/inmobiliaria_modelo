<?php 
declare(strict_types = 1);
namespace inmotek\model\inmobiliaria\cliente;

/**
 * Modelos para los intereses de los clientes.
 * El cliente expresa las preferencias sobre los inmuebles que quiere localizar
 */
class InteresesCliente extends \inmotek\model\base\ModelEntidad {

    /**
     * Identificador del cliente
     * @var integer
     *
     */
    private $id_cliente;

    /**
     * Identificador del usuario que ha recogido la petición
     * @var integer
     *
     */
    private $id_usuario;

    /**
     * Fecha de alta 
     * @var data
     *
     */
    private $fecha;

    /**
     * Fecha de caducidad de la petición
     * @var data
     *
     */
    private $caduca;

    /**
     * Identificación del país
     * @var integer
     *
     */
    private $id_geolocal0;

    /**
     * Identificación de la provincia
     * @var integer
     *
     */
    private $id_geolocal1;

    /**
     * Identificación de la localidad
     * @var array
     *
     */
    private $id_geolocal2;

    /**
     * Identificación de la zona
     * @var array
     *
     */
    private $id_geolocal3;

    /**
     * Identificación de la subzona
     * @var array
     *
     */
    private $id_geolocal4;

    /**
     * Coordenadas GPS
     * @var array
     *
     */
    private $coords;

    /**
     * Polígonos mysql
     * @var string
     *
     */
    private $polygons;

    /**
     * Listado de etiquetas vinculadas
     * @var array
     */
    private $etiquetas;

    /**
     * Tipo de operación disponible para el establecimiento. Estas pueden ser 'v' => 'venta', 'a' => 'alquiler','t' => 'temporal'
     * @var string
     *
     */
    private $operation;

    /**
     * Tipo de inmueble
     * @var string
     *
     */
    private $type;

    /**
     * Subtipo de inmueble
     * @var string
     *
     */
    private $subtype;
    private $pmin;
    private $pmax;
    private $m2min;
    private $m2max;
    private $temporal;
    private $habitaciones;
    private $topehabitaciones;
    private $banos;
    private $topebanos;
    private $calefaccion;
    private $aguacaliente;
    private $ascensor;
    private $mueblesalquiler;
    private $armarios;
    private $garaje;
    private $garajedirecto;
    private $terraza;
    private $balcon;
    private $terraza_balcon;
    private $trastero;
    private $camarote;
    private $trastero_camarote;
    private $accesominusvalidos;
    private $exterior;
    private $alarma;
    private $aire;
    private $grua;
    private $playa;
    private $ducha;
    private $oficinas;
    private $estudiante;
    private $extranjero;
    private $opcioncompra;
    private $tieneanimales;
    private $solovacio;
    private $notas;
    private $notas_cliente;
    private $recibiravisos;
    private $noprimeros;
    private $segundosin;
    private $alturamax;
    private $alturamin;
    private $conservacion;
    private $estructura;
    private $piscina;
    private $jardin;
    private $categoria;
    private $tipoterreno;
    private $diafano;
    private $incendio;
    private $salidahumos;
    private $primeralinea;
    private $trifasica;
    private $alcantarillado;
    private $urbanizado;
    private $vpo;
    private $cerrado;
    private $enbruto;
    private $chaflan;
    private $segundamano;
    private $techosaltos;
    private $vistasalmar;
    private $bano;
}