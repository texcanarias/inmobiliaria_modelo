<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\tipologia;

use \inmotek\model\inmueble\caracteristica\AireAcondicionado;
use \inmotek\model\inmueble\caracteristica\Calefaccion;
use \inmotek\model\inmueble\caracteristica\Volumen;

class Nave 
    implements InterfaceTipologia{

    public static $SUBTIPO_ALMACEN = 0;
    public static $SUBTIPO_GRANERO = 1;
    public static $SUBTIPO_NAVE = 2;
    public static $SUBTIPO_PABELLON = 3;

    use comun\TraitSubtipo;    
    use comun\TraitEstado;  
    use comun\TraitSuperficieConstruida;
    use comun\TraitSuperficieUtil;
    use comun\TraitSuperficieAjardinada;
    use comun\TraitBanyo;
    use comun\TraitPlantasConstruidas;

    /**
     * Aire acondicionado
     * @var \inmotek\model\inmueble\caracteristica\AireAcondicionado
     */
    private ?AireAcondicionado $aireAcondicionado = null;

    /**
     * Calefaccion
     * @var \inmotek\model\inmueble\caracteristica\Calefaccion
     */
    private ?Calefaccion $calefaccion =  null;


    /**
     * Cocina equipada
     * @var bool
     */
    private ?bool $cocinaEquipada = null;

    /**
     * Volumen cubico 
     * @var \inmotek\model\inmueble\caracteristica\Volumen
     */
    private ?Volumen $volumen = null;


    public function getTipologia() : self{
        return $this;
    }
    
    public function getTipo() : string{
        return "Nave";
    }

    /**
     * Get aire acondicionado
     */ 
    public function getAireAcondicionado() : ?AireAcondicionado
    {
        return $this->aireAcondicionado;
    }

    /**
     * Set aire acondicionado
     *
     * @return  self
     */ 
    public function setAireAcondicionado(?AireAcondicionado $aireAcondicionado) : self
    {
        $this->aireAcondicionado = $aireAcondicionado;

        return $this;
    }

    

    /**
     * Get cocina equipada
     *
     * @return  bool
     */ 
    public function getCocinaEquipada() : ?bool
    {
        return $this->cocinaEquipada;
    }

    /**
     * Set cocina equipada
     *
     * @param  bool  $cocinaEquipada  Cocina equipada
     *
     * @return  self
     */ 
    public function setCocinaEquipada(?bool $cocinaEquipada) : self
    {
        $this->cocinaEquipada = $cocinaEquipada;

        return $this;
    }


    /**
     * Get calefaccion
     *
     * @return  \inmotek\model\inmueble\caracteristica\Calefaccion
     */ 
    public function getCalefaccion() : ?Calefaccion
    {
        return $this->calefaccion;
    }

    /**
     * Set calefaccion
     *
     * @param  \inmotek\model\inmueble\caracteristica\Calefaccion  $calefaccion  Calefaccion
     *
     * @return  self
     */ 
    public function setCalefaccion(?Calefaccion $calefaccion) : self
    {
        $this->calefaccion = $calefaccion;

        return $this;
    }



    /**
     * Get volumen cubico
     *
     * @return  \inmotek\model\inmueble\caracteristica\Volumen
     */ 
    public function getVolumen() : ?Volumen
    {
        return $this->volumen;
    }

    /**
     * Set volumen cubico
     *
     * @param  \inmotek\model\inmueble\caracteristica\Volumen  $volumen  Volumen cubico
     *
     * @return  self
     */ 
    public function setVolumen(?Volumen $volumen) : self
    {
        $this->volumen = $volumen;

        return $this;
    }
}