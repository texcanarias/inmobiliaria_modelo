<?php 
declare(strict_types = 1);
namespace inmotek\model\inmobiliaria\portales;

/**
 * Datos de idealista relacionados con la inmobiliaria
 */
class Idealista{

    /**
     * @var bool Si está declarado para volvar en idealista (en la base de datos idealista)
     */
    private $activo;

    /**
     * @var bool Si los inmuebles van a la cartera de idealita (en la bse de datos idealista_cartera)
     */
    private $cartera;

    /**
     * @var bool Si los inmueble tienen como dato de contacto el gestionador (en la bse de datos idealista_mostrar_nombre_gestionadores)
     */
    private $mostrarGestionador;


    public function setActivo($item){
        $this->activo = $item;
        return $this;
    }

    public function setCartera($item){
        $this->cartera = $item;
        return $this;
    }

    public function setMostrarGestionador($item){
        $this->monstrarGestionador = $item;
        return $this;
    }

    public function getActivo(){
        return $this->activo;
    }

    public function getCartera(){
        return $this->cartera;
    }

    public function getMostrarGestionador(){
        return $this->mostrarGestionador;
    }
}