<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble\caracteristica;

class CaracteristicaPared extends Caracteristica{
    static $TIPO_GOTELE = 1;
    static $TIPO_LISA = 2;
    static $TIPO_PAPEL = 3;
    static $TIPO_ESTUCO = 4;
    
    static $ESTADO_RECIEN_PINTADA = 1;
    static $ESTADO_BUEN_ESTADO = 2;
    static $ESTADO_NECESARIO_PINTAR = 3;
    static $ESTADO_NECESARIO_RASEAR_Y_PINTAR = 4;
}