<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\tipologia\comun;

trait TraitSuperficieConstruida{
    /**
     * Superficie construida
     * @var int
     */
    private ?int $superficieConstruida = null;

    public function getSuperficieConstruida() : ?int{
        return $this->superficieConstruida;
    }

    public function setSuperficieConstruida(?int $superficieConstruida) : self{
        if(null == $superficieConstruida){
            return $this;
        }

        if(0 <= $superficieConstruida ){
            $this->superficieConstruida = $superficieConstruida;
        }else{
            throw new \Exception("Error en la superficie construida debe estar ser mayor que 0");
        }
        return $this;
    }
}