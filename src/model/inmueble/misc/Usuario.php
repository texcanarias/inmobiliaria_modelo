<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble;

/**
 * @SWG\Definition()
 */
class Usuario extends DuplaIdNombre{
    
    /**
     * Identificador del inmueble en el sistema original en el caso de una importación
     * @var int
     *
     */                                                           
    private $id_old;
    
    
    /**
     * Idioma de preferencia del cliente con el código ISO 639-1
     * @var string
     *
     */    
    private $language;
       
    
    /**
     * Fecha de nacimiento Formato ISO 8601
     * @var string
     *
     */        
    private $date;
            
    /**
     * Correo electrónico 
     * @var string
     *
     */                        
    private $email;
    
    /**
     * NIF de cliente
     * @var string
     *
     */                            
    private $nif;
    
    /**
     * Código postal del cliente
     * @var string
     *
     */                                
    private $zip;
    
    /**
     * Identificador del país
     * @var string
     *
     */                                    
    private $id_geolevel0;
    
    /**
     * Nombre del país
     * @var string
     *
     */                                        
    private $geolevel0;
    
    /**
     * Identificador de la provicia
     * @var string
     *
     */                                        
    private $id_geolevel1;    
    
    /**
     * Nombre de la provicia
     * @var string
     *
     */                                            
    private $geolevel1;
    
    /**
     * Identificador de la localidad
     * @var string
     *
     */                                            
    private $id_geolevel2;    
    
    /**
     * Nombre de la localidad
     * @var string
     *
     */                                                
    private $geolevel2;
    
    /**
     * Dirección
     * @var string
     *
     */                                                    
    private $address;
    
    private $telefono_trabajo;
    private $telefono_movil;
    private $telefono_casa;
    private $fax;
    
    /**
     * Comentarios
     * @var string
     *
     */                                                        
    private $coment;
    
    private $id_idioma;
    private $id_lang;
    
    /**
     * Si el usuario es administrador del sistema
     * @var type 
     *
     */
    private $esadmin;
    
    /**
     * Identificador de la oficina
     * @var int
     */
    private $id_oficina;
    
    /**
     * @var string Nombre de usuario del sistema
     */
    private $user;
    
    /**
     * @var string Password de usuario de sistema md5('admin1234')
     */
    private $pass;

    public function __construct() {
        parent::__construct();
        $this->pass = md5('admin1234'); //Clave que se le da a un usuario cuando se le da de alta por primera vez
    }
}