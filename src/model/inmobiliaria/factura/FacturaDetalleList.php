<?php 
declare(strict_types = 1);
namespace inmotek\model\inmobiliaria\factura;

use \inmotek\model\base\ArrayStorage;

class FacturaDetalleList extends ArrayStorage {

    public function __construct() {
        parent::__construct('\inmotek\model\inmobiliaria\factura\FacturaDetalle');
    }
}
