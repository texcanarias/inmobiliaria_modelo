<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\tipologia\comun;

trait TraitSuperficieAjardinada{
    /**
     * Superficie ajardinada
     * @var int
     */
    private ?int $superficieAjardinada = null;

    public function getSuperficieAjardinada() : ?int{
        return $this->superficieAjardinada;
    }

    public function setSuperficieAjardinada(?int $superficieAjardinada) : self
    {
        if(null == $superficieAjardinada){
            return $this;
        }

        if(0 <= $superficieAjardinada ){
            $this->superficieAjardinada = $superficieAjardinada;
        }else{
            throw new \Exception("Error en la superficie ajardinada debe estar ser mayor que 0");
        }
        return $this;
    }
}