<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\tipologia\comun;

trait TraitBanyo
{

    /**
     * Número de baños
     * @var int
     */
    private ?int $banyoNumero = null;

    /**
     * Baños adaptados a personas con minusvalias
     * @var bool
     */
    private ?bool $banyoAdaptado = null;



    /**
     * Get número de baños
     *
     * @return  int
     */
    public function getBanyoNumero() : ?int
    {
        return $this->banyoNumero;
    }

    /**
     * Set número de baños
     *
     * @param  int  $numero  Número de baños
     *
     * @return  self
     */
    public function setBanyoNumero(?int $banyoNumero) : self
    {
        if(null == $banyoNumero){
            return $this;
        }

        if (0 <= $banyoNumero) {
            $this->banyoNumero = $banyoNumero;
        } else {
            throw new \Exception("El numero de baños tiene que ser igual o mayor que 0");
        }

        return $this;
    }



    /**
     * Get baños adaptados a personas con minusvalias
     *
     * @return  bool
     */
    public function getBanyoAdaptado(): ?bool
    {
        return $this->banyoAdaptado;
    }

    /**
     * Set baños adaptados a personas con minusvalias
     *
     * @param  bool  $adaptado  Baños adaptados a personas con minusvalias
     *
     * @return  self
     */
    public function setBanyoAdaptado(?bool $banyoAdaptado)
    {
        if(null == $banyoAdaptado){
            return $this;
        }

        $this->banyoAdaptado = $banyoAdaptado;
        if($this->banyoAdaptado AND 0 == $this->banyoNumero){
            $this->banyoNumero = 1;
        }

        return $this;
    }
}
