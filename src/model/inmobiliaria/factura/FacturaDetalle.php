<?php 
declare(strict_types = 1);
namespace inmotek\model\inmobiliaria\factura;

/**
 * Detalle de la factura. 
 * Cada una de las lineas de las facturas.
 */
class FacturaDetalle extends \inmotek\model\base\ModelEntidad{
    /**
     * Identificador de la factura
     * @var integer
     *
     */
    private $id_factura;

    /**
     * Identificador del producto facturado
     * @var integer
     *
     */
    private $id_producto;

    /**
     * Concepto facturado 
     * @var string
     *
     */
    private $concepto;

    /**
     * Importe 
     * @var float
     *
     */
    private $importe;

    /**
     * Unidades facturadas
     * @var boolean
     *
     */
    private $unidades;

    /**
     * Total facturado
     * @var float
     *
     */
    private $total;

    /**
     * Tipo numeral del IVA
     * @var float 
     *
     */
    private $iva;
}
