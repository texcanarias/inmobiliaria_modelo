<?php 
declare(strict_types = 1);
namespace inmotek\model\inmobiliaria\portales;

class PortalesIdentificacion {

    static int $gratuito = 0;
    static int $idealista = 1;
    static int $pisoscom = 2;
    static int $tucasa = 3;
    static int $yaencontre = 4;
    static int $spainhouses = 5;
    static int $globaliza = 6;
    static int $inmogeo = 7;
    static int $facilisimo = 8;
    static int $ibuscando = 9;
    static int $interempresas = 10;
    static int $hogaria = 11;
    static int $globinmo = 12;
    static int $ofertapiso = 13;
    static int $nuroa = 14;
    static int $mundoanuncio = 15;
    static int $tevagustar = 16;
    static int $miparcela = 17;
    static int $casaconjardin = 18;
    static int $enalquiler = 19;
    static int $masprofesional = 20;
    static int $ventadepisos = 21;
    static int $mitula = 22;
    static int $expocasa = 23;
    static int $instalate = 24;
    static int $espacioinmueble = 25;
    static int $alnido = 26;
    static int $grupeate = 27;
    static int $granmanzana = 28;
    static int $zooter = 29;
    static int $bolsapisos = 30;
    static int $trovimap = 31;
    static int $inmoexpertos = 32;
    static int $urbaniza  = 33;
    static int $vivados = 35;
    static int $tupiso = 36;
    static int $general = 37;
    static int $inmotools = 38;
    static int $coapi = 39;
    static int $fotocasa = 40;
    static int $g15 = 41;
    static int $habitaclia = 44;
    static int $misviviendas = 45;
    static int $trovit = 46;
    static int $kyero = 47;
    static int $immoweb = 48;
    static int $vivastreet = 49;
    static int $anaconda = 50;
    static int $misoficinas = 52;
    static int $belbex = 53;
    static int $tuinper = 54;
    static int $james_edition = 55;
    static int $thinkspain = 56;
    static int $todopisospain = 57;

}
