<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\tipologia;

use \inmotek\model\inmueble\caracteristica\AdmiteEstudiantes;

class Habitacion 
    implements InterfaceTipologia{

    public static $SUBTIPO_ESTUDIANTE = 0;
    public static $SUBTIPO_FAMILIA = 1;

    use comun\TraitSubtipo;    
    use comun\TraitSuperficieConstruida;
    use comun\TraitSuperficieUtil;
    
    /**
     * Admite estudiantes
     * @var \inmotek\model\inmueble\caracteristica\AdmiteEstudiantes
     */
    private ?AdmiteEstudiantes $admiteEstudiantes;

    public function getTipologia(){
        return $this;
    }

    public function getTipo() : string{
        return "Habitacion";
    }

    /**
     * Get admite estudiantes
     *
     * @return  \inmotek\model\inmueble\caracteristica\AdmiteEstudiantes
     */ 
    public function getAdmiteEstudiantes() : ?AdmiteEstudiantes
    {
        return $this->admiteEstudiantes;
    }

    /**
     * Set admite estudiantes
     *
     * @param  \inmotek\model\inmueble\caracteristica\AdmiteEstudiantes  $admiteEstudiantes  Admite estudiantes
     *
     * @return  self
     */ 
    public function setAdmiteEstudiantes(?AdmiteEstudiantes $admiteEstudiantes) : self
    {
        $this->admiteEstudiantes = $admiteEstudiantes;

        return $this;
    }
}