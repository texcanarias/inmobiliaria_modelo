<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\tipologia;

use \inmotek\model\inmueble\caracteristica\Volumen;

class Garaje 
    implements InterfaceTipologia{

    public static $SUBTIPO_COCHE = 0;
    public static $SUBTIPO_MOTO = 1;

    use comun\TraitSubtipo; 
    use comun\TraitEstado;     
    use comun\TraitSuperficieConstruida;
    use comun\TraitAscensor;
    use comun\TraitSeguridad;
    
    public function getTipologia(){
        return $this;
    }

    public function getTipo() : string{
        return "Garaje";
    }

    
    /**
     * Puerta automática
     * @var bool
     */
    private ?bool $puertaAutomatica = null;

    /**
     * Plaza cubierta
     * @var bool
     */
    private ?bool $plazaCubierta = null;


    /**
     * Número de plazas
     * @var int
     */
    private ?int $numeroPlazas = null;
    
    /**
     * Volumen cubico 
     * @var \inmotek\model\inmueble\caracteristica\Volumen
     */
    private ?Volumen $volumen = null;

    /**
     * Get puerta automática
     *
     * @return  bool
     */ 
    public function getPuertaAutomatica() : ?bool
    {
        return $this->puertaAutomatica;
    }

    /**
     * Set puerta automática
     *
     * @param  bool  $puertaAutomatica  Puerta automática
     *
     * @return  self
     */ 
    public function setPuertaAutomatica(?bool $puertaAutomatica) : self
    {
        $this->puertaAutomatica = $puertaAutomatica;

        return $this;
    }

    /**
     * Get plaza cubierta
     *
     * @return  bool
     */ 
    public function getPlazaCubierta() : ?bool
    {
        return $this->plazaCubierta;
    }

    /**
     * Set plaza cubierta
     *
     * @param  bool  $plazaCubierta  Plaza cubierta
     *
     * @return  self
     */ 
    public function setPlazaCubierta(?bool $plazaCubierta) : self
    {
        $this->plazaCubierta = $plazaCubierta;

        return $this;
    }


    /**
     * Get número de plazas
     *
     * @return  int
     */ 
    public function getNumeroPlazas() : ?int
    {
        return $this->numeroPlazas;
    }

    /**
     * Set número de plazas
     *
     * @param  int  $numeroPlazas  Número de plazas
     *
     * @return  self
     */ 
    public function setNumeroPlazas(?int $numeroPlazas) : self
    {
        $this->numeroPlazas = $numeroPlazas;

        return $this;
    }



    /**
     * Get volumen
     *
     * @return  \inmotek\model\inmueble\caracteristica\Volumen
     */ 
    public function getVolumen() : ?Volumen
    {
        return $this->volumen;
    }

    /**
     * Set volumen
     *
     * @param  \inmotek\model\inmueble\caracteristica\Volumen  $volumen  Volumen
     *
     * @return  self
     */ 
    public function setVolumen(?Volumen $volumen) : self
    {
        $this->volumen = $volumen;

        return $this;
    }
}