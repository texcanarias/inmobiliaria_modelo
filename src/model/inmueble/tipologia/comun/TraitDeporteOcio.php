<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\tipologia\comun;

trait TraitDeporteOcio{
    /**
     * Con piscina
     * @var ?bool
     */
    private ?bool $piscina = null;

    /**
     * Con pista de tenis
     * @var ?bool
     */
    private ?bool $tenis = null;

    /**
     * Con gimnasio
     * @var ?bool
     */
    private ?bool $gimnasio = null;

    /**
     * Con pista de padel
     * @var ?bool
     */
    private ?bool $padel = null;

    /**
     * Con club social / deportivo
     * @var ?bool
     */
    private ?bool $club = null;

    /**
     * Con juegos
     * @var ?bool
     */
    private ?bool $juegos = null;

    /**
     * Get con piscina
     * @return  bool
     */ 
    public function getPiscina() : ?bool
    {
        return $this->piscina;
    }

    /**
     * Set con piscina
     *
     * @param  bool  $piscina  Con piscina
     *
     * @return  self
     */ 
    public function setPiscina(?bool $piscina) : self
    {
        $this->piscina = $piscina;

        return $this;
    }


    /**
     * Get con pista de tenis
     *
     * @return  ?bool
     */ 
    public function getTenis() : ?bool
    {
        return $this->tenis;
    }

    /**
     * Set con pista de tenis
     *
     * @param  ?bool  $tenis  Con pista de tenis
     *
     * @return  self
     */ 
    public function setTenis(?bool $tenis) : self
    {
        $this->tenis = $tenis;

        return $this;
    }

    /**
     * Get con gimnasio
     *
     * @return  ?bool
     */ 
    public function getGimnasio() : ?bool
    {
        return $this->gimnasio;
    }

    /**
     * Set con gimnasio
     *
     * @param  ?bool  $gimnasio  Con gimnasio
     *
     * @return  self
     */ 
    public function setGimnasio(?bool $gimnasio) : self
    {
        $this->gimnasio = $gimnasio;

        return $this;
    }

    /**
     * Get con pista de padel
     *
     * @return  ?bool
     */ 
    public function getPadel() : ?bool
    {
        return $this->padel;
    }

    /**
     * Set con pista de padel
     *
     * @param  ?bool  $padel  Con pista de padel
     *
     * @return  self
     */ 
    public function setPadel(?bool $padel) : self
    {
        $this->padel = $padel;

        return $this;
    }

    /**
     * Get con club social / deportivo
     *
     * @return  ?bool
     */ 
    public function getClub() : ?bool
    {
        return $this->club;
    }

    /**
     * Set con club social / deportivo
     *
     * @param  ?bool  $club  Con club social / deportivo
     *
     * @return  self
     */ 
    public function setClub(?bool $club) : self
    {
        $this->club = $club;

        return $this;
    }

    /**
     * Get con juegos
     *
     * @return  ?bool
     */ 
    public function getJuegos() : ?bool
    {
        return $this->juegos;
    }

    /**
     * Set con juegos
     *
     * @param  ?bool  $juegos  Con juegos
     *
     * @return  self
     */ 
    public function setJuegos(?bool $juegos) : self
    {
        $this->juegos = $juegos;

        return $this;
    }
}