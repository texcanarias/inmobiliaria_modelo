<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\tipologia;

use \inmotek\model\inmueble\caracteristica\AireAcondicionado;
use \inmotek\model\inmueble\caracteristica\Calefaccion;
use \inmotek\model\inmueble\caracteristica\DisponibilidadTrastero;

class Local 
    implements InterfaceTipologia{

    public static $SUBTIPO_ALMACEN = 0;
    public static $SUBTIPO_BAR = 1;
    public static $SUBTIPO_BODEGA = 2;
    public static $SUBTIPO_LOCAL = 3;
    public static $SUBTIPO_LONJA = 4;
    public static $SUBTIPO_RESTAURANTE = 5;
    public static $SUBTIPO_TXOKO = 6;            


    public static $UBICACION_TIENDA = 0; //SHOPPING
    public static $UBICACION_CALLE = 1; //STREET
    public static $UBICACION_PLANTA = 2; //mezzanine
    public static $UBICACION_SOTANO = 3; //belowGround
    public static $UBICACION_OTRO = 4; //other
    public static $UBICACION_DESCONOCIDO = 5; //unknown

    use comun\TraitSubtipo;         
    use comun\TraitEstado;    
    use comun\TraitCes;
    use comun\TraitSuperficieConstruida;
    use comun\TraitSuperficieUtil;
    use comun\TraitSuperficieAjardinada;
    use comun\TraitPlantasConstruidas;
    use comun\TraitBanyo;
    use comun\TraitSeguridad;

    /**
     * Aire acondicionado
     * @var \inmotek\model\inmueble\caracteristica\AireAcondicionado
     */
    private ?AireAcondicionado $aireAcondicionado = null;

    /**
     * Calefaccion
     * @var \inmotek\model\inmueble\caracteristica\Calefaccion
     */
    private ?Calefaccion $calefaccion = null;


    /**
     * Cocina equipada
     * @var bool
     */
    private ?bool $cocinaEquipada = null;

    /**
     * Área de la fachada
     * @var int
     */
    private ?int $areaFachada = null;


    /**
     * Última actividad
     * @var string
     */
    private string $ultimaActividad = "";

    /**
     * Habitaciones - Comparticiones - Huecos
     * @var int
     */
    private ?int $huecos = null;

    /**
     * Almacen
     * @var bool
     */
    private ?bool $almacen = null;


    /**
     * Extractor de humos
     * @var bool
     */
    private ?bool $extractorHumos = null;

    /**
     * Ubicacion del inmueble
     * @var int
     */
    private ?int $ubicacion = null;

    /**
     * Número de ventanas
     * @var int
     */
    private ?int $numeroVentanas = null;

    /**
     * Disponibilidad de trastero
     * \inmotek\model\inmueble\caracteristica\DisponibilidadTrastero
     */
    private ?DisponibilidadTrastero $disponibilidadTrastero = null;     


    public function getTipologia(){
        return $this;
    }

    public function getTipo() : string{
        return "Local";
    }

    /**
     * Get aire acondicionado
     *
     * @return  \inmotek\model\inmueble\caracteristica\AireAcondicionado
     */ 
    public function getAireAcondicionado() : ?AireAcondicionado 
    {
        return $this->aireAcondicionado;
    }

    /**
     * Set aire acondicionado
     *
     * @param  \inmotek\model\inmueble\caracteristica\AireAcondicionado  $aireAcondicionado  Aire acondicionado
     *
     * @return  self
     */ 
    public function setAireAcondicionado(?AireAcondicionado $aireAcondicionado) : self
    {
        $this->aireAcondicionado = $aireAcondicionado;

        return $this;
    }

    /**
     * Get calefaccion
     *
     * @return  \inmotek\model\inmueble\caracteristica\Calefaccion
     */ 
    public function getCalefaccion() :?Calefaccion
    {
        return $this->calefaccion;
    }

    /**
     * Set calefaccion
     *
     * @param  \inmotek\model\inmueble\caracteristica\Calefaccion  $calefaccion  Calefaccion
     *
     * @return  self
     */ 
    public function setCalefaccion(?Calefaccion $calefaccion) : self
    {
        $this->calefaccion = $calefaccion;

        return $this;
    }

    /**
     * Get cocina equipada
     *
     * @return  bool
     */ 
    public function getCocinaEquipada() : ?bool
    {
        return $this->cocinaEquipada;
    }

    /**
     * Set cocina equipada
     *
     * @param  bool  $cocinaEquipada  Cocina equipada
     *
     * @return  self
     */ 
    public function setCocinaEquipada(?bool $cocinaEquipada) : self
    {
        $this->cocinaEquipada = $cocinaEquipada;

        return $this;
    }

    /**
     * Get área de la fachada
     *
     * @return  int
     */ 
    public function getAreaFachada() : ?int
    {
        return $this->areaFachada;
    }

    /**
     * Set área de la fachada
     *
     * @param  int  $areaFachada  Área de la fachada
     *
     * @return  self
     */ 
    public function setAreaFachada(?int $areaFachada) : self
    {
        $this->areaFachada = $areaFachada;

        return $this;
    }

    /**
     * Get última actividad
     *
     * @return  string
     */ 
    public function getUltimaActividad() : string
    {
        return $this->ultimaActividad;
    }

    /**
     * Set última actividad
     *
     * @param  string  $ultimaActividad  Última actividad
     *
     * @return  self
     */ 
    public function setUltimaActividad(string $ultimaActividad = "") : self
    {
        $this->ultimaActividad = $ultimaActividad;

        return $this;
    }

    /**
     * Get habitaciones - Comparticiones - Huecos
     *
     * @return  int
     */ 
    public function getHuecos() : ?int
    {
        return $this->huecos;
    }

    /**
     * Set habitaciones - Comparticiones - Huecos
     *
     * @param  int  $huecos  Habitaciones - Comparticiones - Huecos
     *
     * @return  self
     */ 
    public function setHuecos(?int $huecos) : self
    {
        $this->huecos = $huecos;

        return $this;
    }

    /**
     * Get almacen
     *
     * @return  bool
     */ 
    public function getAlmacen() : ?bool
    {
        return $this->almacen;
    }

    /**
     * Set almacen
     *
     * @param  bool  $almacen  Almacen
     *
     * @return  self
     */ 
    public function setAlmacen(?bool $almacen) : self
    {
        $this->almacen = $almacen;

        return $this;
    }

    /**
     * Get extractor de humos
     *
     * @return  bool
     */ 
    public function getExtractorHumos() : ?bool
    {
        return $this->extractorHumos;
    }

    /**
     * Set extractor de humos
     *
     * @param  bool  $extractorHumos  Extractor de humos
     *
     * @return  self
     */ 
    public function setExtractorHumos(?bool $extractorHumos) : self
    {
        $this->extractorHumos = $extractorHumos;

        return $this;
    }

    /**
     * Get ubicacion del inmueble
     *
     * @return  int
     */ 
    public function getUbicacion() : ?int
    {
        return $this->ubicacion;
    }

    /**
     * Set ubicacion del inmueble
     *
     * @param  int  $ubicacion  Ubicacion del inmueble
     *
     * @return  self
     */ 
    public function setUbicacion(?int $ubicacion) : self
    {
        $this->ubicacion = $ubicacion;

        return $this;
    }

    /**
     * Get número de ventanas
     *
     * @return  int
     */ 
    public function getNumeroVentanas() : ?int
    {
        return $this->numeroVentanas;
    }

    /**
     * Set número de ventanas
     *
     * @param  int  $numeroVentanas  Número de ventanas
     *
     * @return  self
     */ 
    public function setNumeroVentanas(?int $numeroVentanas) : self
    {
        $this->numeroVentanas = $numeroVentanas;

        return $this;
    }

    /**
     * Get disponibilidad de trastero
     */ 
    public function getDisponibilidadTrastero() : ?DisponibilidadTrastero
    {
        return $this->disponibilidadTrastero;
    }

    /**
     * Set disponibilidad de trastero
     *
     * @return  self
     */ 
    public function setDisponibilidadTrastero(?DisponibilidadTrastero $disponibilidadTrastero) : self
    {
        $this->disponibilidadTrastero = $disponibilidadTrastero;

        return $this;
    }

}