<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble\caracteristica;

class TipoTerreno extends CaracteristicaInt {
    static $ITEMS = ["LLANO" => "Llano",
                    "SEMILLANO" => "Semillano",
                    "PENDIENTE" => "Pendiente"];

    static $LLANO = 1;
    static $SEMILLANO = 2;
    static $PENDIENTE = 3;

    use TraitFactory;

    protected function isCorrecto(int $item)
    {
        return self::$LLANO <= $item AND self::$PENDIENTE >= $item;
    }
}

