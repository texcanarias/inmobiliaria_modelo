<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble\caracteristica;

abstract class CaracteristicaInt {
    /**
     * Valor definido
     * @var int
     */
    private ?int $item = null;

    protected function __construct(?int $item)
    {
        try{
            $this->setItem($item);
        }
        catch(\Exception $ex){
            throw $ex;
        }
    }

    public function __invoke() : ?int
    {
        return $this->getItem();
    }

    /**
     * Get int
     * @return ?int
     */ 
    protected function getItem() : ?int
    {
        return $this->item;
    }


    /**
     * Set int
     * @param int $item
     */ 
    protected function setItem(?int $item) : self
    {
        if(null != $item AND !$this->isCorrecto($item)){
            throw new \Exception("Valor incorrecto");
        }

        $this->item = $item;
        return $this;
    }

    /**
     * Debe sobrecargarse con la comprobacion de que el valor pasado es correcto
     */
    abstract protected function isCorrecto(int $item);

}


