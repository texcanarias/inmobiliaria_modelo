<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\tipologia\comun;

trait TraitFuego{
    /**
     * Presencia de alarma de incendio
     * @var bool
     */
    private ?bool $tieneAlarmaIncendio = null;

    /**
     * Presencia de extintores de incendio
     * @var bool
     */
    private ?bool $tieneExtintores = null;

    /**
     * Presencia de detectores de incendio
     * @var bool
     */
    private ?bool $tieneDetectores = null;

    /**
     * Get presencia de extintores de incendio
     *
     * @return  bool
     */ 
    public function getTieneExtintores() : ?bool
    {
        return $this->tieneExtintores;
    }

    /**
     * Set presencia de extintores de incendio
     *
     * @param  bool  $tieneExtintores  Presencia de extintores de incendio
     *
     * @return  self
     */ 
    public function setTieneExtintores(?bool $tieneExtintores) : self
    {
        $this->tieneExtintores = $tieneExtintores;

        return $this;
    }

    /**
     * Get presencia de detectores de incendio
     *
     * @return  bool
     */ 
    public function getTieneDetectores() : ?bool
    {
        return $this->tieneDetectores;
    }

    /**
     * Set presencia de detectores de incendio
     *
     * @param  bool  $tieneDetectores  Presencia de detectores de incendio
     *
     * @return  self
     */ 
    public function setTieneDetectores(?bool $tieneDetectores) : self
    {
        $this->tieneDetectores = $tieneDetectores;

        return $this;
    }

    /**
     * Get presencia de alarma de incendio
     *
     * @return  bool
     */ 
    public function getTieneAlarmaIncendio() : ?bool
    {
        return $this->tieneAlarmaIncendio;
    }

    /**
     * Set presencia de alarma de incendio
     *
     * @param  bool  $tieneAlarmaIncendio  Presencia de alarma de incendio
     *
     * @return  self
     */ 
    public function setTieneAlarmaIncendio(?bool $tieneAlarmaIncendio) : self
    {
        $this->tieneAlarmaIncendio = $tieneAlarmaIncendio;

        return $this;
    }
}