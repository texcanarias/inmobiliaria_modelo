<?php
declare(strict_types = 1);
namespace inmotek\model\base;

use \texcanarias\ZipValidator\ZipCodeValidator;
use \inmotek\model\base\DuplaIdNombre;
use \inmotek\model\base\Gps;

class Direccion
{
    static public int $errorRegistrarCPSinPais = 401;
    static public int $errorCPNoValido = 402;
    static public int $errorPaisDosCaracteres = 403;

    /**
     * @var String $pais Código de dos letras del país
     */
    private ?string $pais = "";

    /**
     * @var \inmotek\model\base\DuplaIdNombre $provincia Nombre de la provincia (Cadena de 25 caracteres)
     */
    private DuplaIdNombre $provincia;

    /**
     * @var \inmotek\model\base\DuplaIdNombre $localidad Nombre de la localidad (Cadena de 40 caracteres)
     */
    private DuplaIdNombre $localidad;

    /**
     * @var int Código postal (Cadena de 5 carácteres)
     */
    private ?string $cp = "";

    /**
     * @var \inmotek\model\base\Gps Localizacion GPS
     */
    private Gps $gps;

    protected function __construct()
    {
    }

    static public function factory(string $clave, string $pais, int $idProvincia, string $provincia, int $idLocalidad, string $localidad, string $cp, float $latitud, float $longitud) : self
    {
        $gps = \inmotek\model\base\Gps::factory($latitud, $longitud);
        $d = new self();
        $d->setGps($gps);
        $d->setProvincia(DuplaIdNombre::factory($idProvincia, $clave, $provincia));
        $d->setLocalidad(DuplaIdNombre::factory($idLocalidad, $clave, $localidad));
        try {            
            $d->setPais($pais);            
            $d->setCp($cp);
        } catch (\Exception $ex) {
            $d->pais = 'ES';
            $d->cp = '';
        }
        return $d;
    }

    public function getPais(): string
    {
        return $this->pais;
    }

    public function getProvincia(): \inmotek\model\base\DuplaIdNombre
    {
        return $this->provincia;
    }

    public function getLocalidad(): \inmotek\model\base\DuplaIdNombre
    {
        return $this->localidad;
    }

    public function getCp() : string
    {
        return $this->cp;
    }

    protected function setPais(?string $pais) : self
    {
        if(null != $pais){
            $isDosCaracteres = preg_match('/^[A-Z]{2}$/i', $pais);
            if (!$isDosCaracteres) {
                throw new \Exception("Debe ser una cadena de dos carácteres [" . $pais ."]", self::$errorPaisDosCaracteres);
            }
        }
        $this->pais = $pais;
        return $this;
    }

    protected function setProvincia(\inmotek\model\base\DuplaIdNombre $provincia) : self
    {
        $this->provincia = $provincia;
        return $this;
    }

    protected function setLocalidad(\inmotek\model\base\DuplaIdNombre $localidad) : self
    {
        $this->localidad = $localidad;
        return $this;
    }

    protected function setCp(?string $cp, ?string $pais = null) : self
    {

        if (null != $pais && "" != $pais) {
            $this->pais = $this->setPais($pais);
        }
        
        if(null == $this->pais)
        {
            throw new \Exception("No se puede registrar el CP sin registrar el país", self::$errorRegistrarCPSinPais);
        }

        $this->cp = $cp;

        if(null != $this->cp AND "" != $this->cp){
            try {                
                $zv = ZipCodeValidator::factory();
                $cpValidate = $zv->validate($this->cp, $this->pais);
            } catch (\Exception $ex) {
                throw $ex;                    
            }
            if(!$cpValidate){
                throw new \Exception("El CP no es válido", self::$errorCPNoValido);
            }
        }
        return $this;
    }

    protected function setGps(\inmotek\model\base\Gps $gps) : self
    {
        $this->gps = $gps;
        return $this;
    }

    /**
     * Recupera la posición GPS de la direccion
     * @return \inmotek\model\base\Gps $gps
     */
    public function getGps() : \inmotek\model\base\Gps
    {
        return $this->gps;
    }
}
