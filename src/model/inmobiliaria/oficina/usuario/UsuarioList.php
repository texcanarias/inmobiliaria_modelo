<?php 
declare(strict_types = 1);
namespace inmotek\model\inmobiliaria\oficina\usuario;

use \inmotek\model\base\ArrayStorage;

class UsuarioList extends ArrayStorage {

    public function __construct() {
        parent::__construct('\inmotek\model\inmobiliaria\oficina\usuario\Usuario');
    }
}
