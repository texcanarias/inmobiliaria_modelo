<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble\tipologia\comun;

trait TraitOrientacion{
   
    /**
     * Orientacion norte
     * @var bool
     */
    public ?bool $orientacionNorte = null;

    /**
     * Orientacion sur
     * @var bool
     */
    public ?bool $orientacionSur = null;

    /**
     * Orientacion oeste
     * @var bool
     */
    public ?bool $orientacionOeste = null;

    /**
     * Orientacion este
     * @var bool
     */
    public ?bool $orientacionEste = null;

    /**
     * Get orientacion norte
     *
     * @return  bool
     */ 
    public function getOrientacionNorte() : ?bool
    {
        return $this->orientacionNorte;
    }

    /**
     * Set orientacion norte
     *
     * @param  bool  $orientacionNorte  Orientacion norte
     *
     * @return  self
     */ 
    public function setOrientacionNorte(?bool $orientacionNorte) : self
    {
        $this->orientacionNorte = $orientacionNorte;
        $this->reorientar();
        return $this;
    }

    /**
     * Get orientacion sur
     *
     * @return  bool
     */ 
    public function getOrientacionSur() : ?bool
    {
        return $this->orientacionSur;
    }

    /**
     * Set orientacion sur
     *
     * @param  bool  $orientacionSur  Orientacion sur
     *
     * @return  self
     */ 
    public function setOrientacionSur(?bool $orientacionSur) : self
    {
        $this->orientacionSur = $orientacionSur;
        $this->reorientar();
        return $this;
    }

    /**
     * Get orientacion oeste
     *
     * @return  bool
     */ 
    public function getOrientacionOeste() :?bool
    {
        return $this->orientacionOeste;
    }

    /**
     * Set orientacion oeste
     *
     * @param  bool  $orientacionOeste  Orientacion oeste
     *
     * @return  self
     */ 
    public function setOrientacionOeste(?bool $orientacionOeste) : self
    {
        $this->orientacionOeste = $orientacionOeste;
        $this->reorientar();
        return $this;
    }

    /**
     * Get orientacion este
     *
     * @return  bool
     */ 
    public function getOrientacionEste() : ?bool
    {
        return $this->orientacionEste;
    }

    /**
     * Set orientacion este
     *
     * @param  bool  $orientacionEste  Orientacion este
     *
     * @return  self
     */ 
    public function setOrientacionEste(?bool $orientacionEste) : self
    {
        $this->orientacionEste = $orientacionEste;
        $this->reorientar();
        return $this;        
    }

    /**
     * Pasa los nulos a false;
     */
    private function reorientar(){
        if(null == $this->orientacionNorte){
            $this->orientacionNorte = false;
        }

        if(null == $this->orientacionSur){
            $this->orientacionSur = false;
        }

        if(null == $this->orientacionOeste){
            $this->orientacionOeste = false;
        }

        if(null == $this->orientacionEste){
            $this->orientacionEste = false;
        }
    }
}