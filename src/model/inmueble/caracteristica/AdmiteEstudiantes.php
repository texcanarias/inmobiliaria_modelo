<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble\caracteristica;

class AdmiteEstudiantes extends CaracteristicaInt{

    use TraitFactory;

    static $ITEMS = ["NO" => 0,
                    "SI" => 1,
                    "SOLO_ESTUDIANTES" => 2];

    static $NO = 0;
    static $SI = 1;
    static $SOLO_ESTUDIANTES = 2;

    protected function isCorrecto(int $item) 
    {
        return self::$NO <= $item AND self::$SOLO_ESTUDIANTES >= $item;
    }

}


