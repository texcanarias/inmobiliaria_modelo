<?php 
declare(strict_types = 1);
namespace inmotek\model\base;

/**
 * Modelo base para sostener la persistencia
 */
interface InterfaceEntidad extends InterfacePersistenciaDb{
    public function getId() : ?int;
    public function setId(?int $id = null) : self;
    public function isNew() : bool;
}