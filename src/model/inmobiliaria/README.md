# Descripción de los modelos inmotek\model\inmobiliaria

```plantuml
@startuml

package inmotek <<Frame>> {
    package model <<Frame>> {
        package base <<Frame>> {
            class Gps
            class ArrayStorage
            class DuplaIdNombre
        }
         package inmobiliaria <<Frame>>{
            class Inmobiliaria
            class InmobiliariaList
            class Direccion
            package oficina <<Frame>> {
                package usuario <<Frame>> {
                    class Usuario
                    class UsuarioList
                }
            class Oficina
            class OficinaList
            }
            package config <<Frame>> {
                package portales <<Frame>> {
                    class Portales
                    class Fotocasa
                    class Idealista
                }
            }
         }
    }
}


ArrayStorage <|-- InmobiliariaList
ArrayStorage <|-- OficinaList
InmobiliariaList o-- Inmobiliaria
OficinaList o-- Oficina

class Inmobiliaria{
    - clave;
    - letra;
    - demo
    - sistema
    - \inmotek\model\inmobiliaria\direccion  direccion
    - token
    - \inmotek\model\inmobiliaria\oficinas oficinas
    - \inmotek\model\inmobiliaria\portales portales
    + __construct($id = null)
    + getDireccion()
    + setDireccion(\inmotek\model\inmobiliaria\direccion $direccion)
    + getOficinas()
    + setOficinas(\inmotek\model\inmobiliaria\oficina\oficinaList $oficinas)
    + getClave()
    + getLetra()
    + getDemo()
    + getSistema()
    + getToken()
    + getPorales()
    + setClave($clave)
    + setLetra($letra)
    + setDemo($demo)
    + setSistema($sistema)
    + setToken(String $token)
    + setPortales($portales)
}

DuplaIdNombre <|-- Inmobiliaria
Inmobiliaria *-- Direccion
Inmobiliaria *-- OficinaList
Inmobiliaria *-- Portales

class Direccion{
    - pais
    - provincia
    - localidad
    - direccion
    - cp
    - gps
    + getPais()
    + getProvincia()
    + getLocalidad()
    + getDireccion()
    + getCp()
    + setPais(String $pais)
    + setProvincia(String $provincia)
    + setLocalidad(String $localidad)
    + setDireccion(String $direccion)
    + setCp($cp, $pais = null)
    + setGps(\inmotek\model\base\Gps $gps)
    + getGps($gps)
}

Direccion *-- ZipCodeValidator
Direccion *-- ZipCode
Direccion *-- Gps

class Oficina{
    - principal;
    - publicar;
    - orden;
    - direccion;
    - usuarios
    + __construct()
    + getDireccion(): \inmotek\model\inmobiliaria\direccion
    + setDireccion(\inmotek\model\inmobiliaria\direccion $direccion)
    + getPrincipal()
    + setPrincipal($principal)
    + getPublicar()
    + setPublicar($publicar)
    + getOrden()
    + setOrden($orden)
    + getUsuarios(): \inmotek\model\inmobiliaria\oficina\usuario\usuarioList
    + setUsuarios(\inmotek\model\inmobiliaria\oficina\usuario\usuarioList $usuarios)
}

DuplaIdNombre <|-- Oficina
Oficina *-- Direccion
Oficina *-- UsuarioList
UsuarioList *-- Usuario

class Usuario{
    - direccion
    + __construct()
    + getDireccion(): \inmotek\model\inmobiliaria\direccion
    + setDireccion(\inmotek\model\inmobiliaria\direccion $direccion)
}

DuplaIdNombre <|-- Usuario
Usuario *-- Direccion


class Portales{
    - publicarPortales;
    - idealista;
    - fotocasa;
    + __construct()
    + getPublicarPortales()
    + setPublicarPortales($item)
    + getIdealista()
    + setIdealista(\inmotek\model\inmobiliaria\config\portales\idealista $idealista)
    + getFotocasa()
    + setFotocasa(\inmotek\model\inmobiliaria\config\portales\fotocasa $fotocasa)
}

Portales *-- Idealista
Portales *-- Fotocasa

class Fotocasa{
    - activo;
    - tipoContacto
    - mailContacto
    - telefonoContacto
    - cartera 
    - terceros;
    + setActivo($item)
    + setCartera($item)
    + setTipoContacto($item)
    + setMailcontacto($item)
    + setTelefonoContacto($item)
    + setTerceros($item)
}

class Idealista{
    - activo
    - cartera
    - mostrarGestionador
    + setActivo($item)
    + setCartera($item)
    + setMostrarGestionador($item)
    + getActivo()
    + getCartera()
    + getMostrarGestionador()
}
@enduml
```
