<?php namespace inmotek_test\phpunit;

use PHPUnit\Framework\TestCase;
use inmotek\model\inmueble\caracteristica\AdmiteEstudiantes;
use inmotek\model\inmueble\caracteristica\AguaCaliente;
use inmotek\model\inmueble\caracteristica\AireAcondicionado;
use inmotek\model\inmueble\caracteristica\Aislamiento;
use inmotek\model\inmueble\caracteristica\Amueblado;
use inmotek\model\inmueble\caracteristica\Calefaccion;
use inmotek\model\inmueble\caracteristica\CalificacionUrbanistica;
use inmotek\model\inmueble\caracteristica\CaracteristicaPared;
use inmotek\model\inmueble\caracteristica\CaracteristicaPuerta;
use inmotek\model\inmueble\caracteristica\CaracteristicaSuelo;
use inmotek\model\inmueble\caracteristica\CaracteristicaVentana;
use inmotek\model\inmueble\caracteristica\DisponibilidadGarage;
use inmotek\model\inmueble\caracteristica\DisponibilidadTrastero;
use inmotek\model\inmueble\caracteristica\Exterior;
use inmotek\model\inmueble\caracteristica\SegundaMano;
use inmotek\model\inmueble\caracteristica\TipoEstructura;
use inmotek\model\inmueble\caracteristica\TipoFachada;
use inmotek\model\inmueble\caracteristica\TipoTerreno;
use inmotek\model\inmueble\caracteristica\Volumen;

final class caracteristicasTest extends TestCase {

    public function testAdmiteEstudiantes() {
        $item = AdmiteEstudiantes::factory(AdmiteEstudiantes::$SOLO_ESTUDIANTES);
        $this->assertTrue( AdmiteEstudiantes::$SOLO_ESTUDIANTES == $item() );
    }

    public function testAguaCaliente(){
        $item = AguaCaliente::factory(AguaCaliente::$INDIVIDUAL_GAS_NATURAL); 
        $this->assertTrue(AguaCaliente::$INDIVIDUAL_GAS_NATURAL == $item());
    }

    public function testAireAcondicionado(){
        $item = AireAcondicionado::factory(AireAcondicionado::$BOMBA_DE_CALOR);
        $this->assertTrue(AireAcondicionado::$BOMBA_DE_CALOR == $item());
    }

    public function testAislamiento(){
        $item = new Aislamiento();
        $item->setTermico(true);
        $item->setAcustico(true);
        $this->assertTrue($item->getTermico());
        $this->assertTrue($item->getAcustico());
    }

    public function testAmueblado(){
        $item = Amueblado::factory(Amueblado::$AMUEBLADO);
        $this->assertTrue(Amueblado::$AMUEBLADO == $item());
    }

    public function testCalefaccion(){
        $item = Calefaccion::factory(Calefaccion::$CHIMENEA);
        $this->assertTrue(Calefaccion::$CHIMENEA == $item());
    }

    public function testCalificacionUrbanistica(){
        $item = CalificacionUrbanistica::factory(CalificacionUrbanistica::$URBANIZABLE);
        $this->assertTrue(CalificacionUrbanistica::$URBANIZABLE == $item());
    }

    public function testCarecteristicaPared(){
        $item = new CaracteristicaPared();
        $item->setTipo(CaracteristicaPared::$TIPO_GOTELE);
        $item->setEstado(CaracteristicaPared::$ESTADO_BUEN_ESTADO);
        $this->assertTrue(CaracteristicaPared::$TIPO_GOTELE == $item->getTipo());
        $this->assertTrue(CaracteristicaPared::$ESTADO_BUEN_ESTADO == $item->getEstado());
    }

    public function testCarecteristicaPuerta(){
        $item = new CaracteristicaPuerta();
        $item->setTipo(CaracteristicaPuerta::$TIPO_ROBLE);
        $item->setEstado(CaracteristicaPuerta::$ESTADO_NUEVAS);
        $this->assertTrue(CaracteristicaPuerta::$TIPO_ROBLE == $item->getTipo());
        $this->assertTrue(CaracteristicaPuerta::$ESTADO_NUEVAS == $item->getEstado());
    }

    public function testCarecteristicaSuelo(){
        $item = new CaracteristicaSuelo();
        $item->setTipo(CaracteristicaSuelo::$TIPO_ROBLE);
        $item->setEstado(CaracteristicaSuelo::$ESTADO_BUEN_ESTADO);
        $this->assertTrue(CaracteristicaSuelo::$TIPO_ROBLE == $item->getTipo());
        $this->assertTrue(CaracteristicaSuelo::$ESTADO_BUEN_ESTADO == $item->getEstado());
    }

    public function testCarecteristicaVentana(){
        $item = new CaracteristicaVentana();
        $item->setTipo(CaracteristicaVentana::$TIPO_ALUMINIO);
        $item->setEstado(CaracteristicaVentana::$ESTADO_BUEN_ESTADO);
        $this->assertTrue(CaracteristicaVentana::$TIPO_ALUMINIO == $item->getTipo());
        $this->assertTrue(CaracteristicaVentana::$ESTADO_BUEN_ESTADO == $item->getEstado());
    }


    public function testDisponibilidadGarage(){
        $item = DisponibilidadGarage::factory(DisponibilidadGarage::$GARAJE_INCLUIDO);
        $this->assertTrue(DisponibilidadGarage::$GARAJE_INCLUIDO == $item());
    }

    public function testDisponibilidadTrastero(){
        $item = DisponibilidadTrastero::factory(DisponibilidadTrastero::$TRASTERO_OPCIONAL);
        $this->assertTrue(DisponibilidadTrastero::$TRASTERO_OPCIONAL == $item());
    }

    public function testExterior(){
        $item = Exterior::factory(Exterior::$SEMIEXTERIOR);
        $this->assertTrue(Exterior::$SEMIEXTERIOR == $item());
    }


    public function testSegundaMano(){
        $item = SegundaMano::factory(SegundaMano::$SEGUNDA_MANO);
        $this->assertTrue(SegundaMano::$SEGUNDA_MANO == $item());
    }

    public function testTipoEstructura(){
        $item = TipoEstructura::factory(TipoEstructura::$HORMIGON);
        $this->assertTrue(TipoEstructura::$HORMIGON == $item());
    }

    public function testTipoFachada(){
        $item = TipoFachada::factory(TipoFachada::$CARAVISTA);
        $this->assertTrue(TipoFachada::$CARAVISTA == $item());
    }

    public function testTipoTerreno(){
        $item = TipoTerreno::factory(TipoTerreno::$LLANO);
        $this->assertTrue(TipoTerreno::$LLANO == $item());
    }

    public function testVolumen(){
        $item = Volumen::factory(20,30,40);    
        $this->assertTrue(20 == $item->getProfundidad());
        $this->assertTrue(30 == $item->getAnchura());
        $this->assertTrue(40 == $item->getAltura());
        $this->assertTrue(600 == $item->getSuperficie() );
        $this->assertTrue(24000 == $item->getVolumen() );     
    }

    public function testNulo(){
        $item = TipoTerreno::factory(null);
        $this->assertTrue(null == $item());
    }


}
