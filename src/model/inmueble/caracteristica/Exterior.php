<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble\caracteristica;

use phpDocumentor\Reflection\DocBlock\Tags\Example;

class Exterior extends CaracteristicaInt {
    static $ITEMS = [   "INDIFINIDO" => 0,
                        "EXTERIOR" => 1,
                        "SEMIEXTERIOR" => 2,
                        "INTERIOR" => 3];  

    static $INDEFINIDO = 0;
    static $EXTERIOR = 1;
    static $SEMIEXTERIOR = 2;
    static $INTERIOR = 3;                          

    use TraitFactory;

    protected function isCorrecto(int $item)
    {
        return self::$INDEFINIDO <= $item AND self::$INTERIOR >= $item;
    }
}