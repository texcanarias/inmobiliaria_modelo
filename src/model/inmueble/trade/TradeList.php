<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble\trade;

/**
 * @todo Cambiar los indices de texto por indices numericos
 */
class TradeList implements \Iterator, \ArrayAccess, \Countable {

    /**
     * Array que almacenará los datos
     */
    private $holder = [];

    static public $VENTA = 'venta';    
    static public $ALQUILER = 'alquiler';
    static public $VACACIONAL = 'vacacional';
    static public $TRASPASO = 'traspaso';    

    static private $INDICES_ADMITIDOS = ['alquiler', 'traspaso', 'vacacional','venta'];

    static private $ERROR_INDICE_NO_ADMITIDO = 'Índice no admitido debe ser alquiler, traspaso, vacacional o venta';

    public function __construct() {
        $this->holder = [   'alquiler' => null, 
                            'traspaso' => null,
                            'vacacional' => null,
                            'venta' => null
                        ];    
    }

    /**
     * implementacion de la interfaz iterator
     */
    public function rewind() 
    {
        reset($this->holder);
    }

    /**
     * implementacion de la interfaz iterator
     */    
    public function current() 
    {
        return current($this->holder);
    }

    /**
     * implementacion de la interfaz iterator
     */    
    public function key() 
    {
        return key($this->holder);
    }

    /**
     * implementacion de la interfaz iterator
     */    
    public function next() 
    {
        next($this->holder);
    }

    /**
     * implementacion de la interfaz iterator
     */    
    public function valid() 
    {
        return false !== $this->current();
    }
    
    public function offsetSet($offset, $value){
        if (is_null($offset)) {
            switch(get_class($value)){
                case 'inmotek\model\inmueble\trade\Alquiler':
                    $this->setAlquiler($value);
                break;
                case 'inmotek\model\inmueble\trade\Traspaso':
                    $this->setTraspaso($value);
                break;
                case 'inmotek\model\inmueble\trade\Vacacional':
                    $this->setVacacional($value);
                break;
                case 'inmotek\model\inmueble\trade\Venta':
                    $this->setVenta($value);
                break;
                default:
                    throw new \Exception(self::$ERROR_INDICE_NO_ADMITIDO);
                break;
            }
        } else {
            switch($offset){
                case 'alquiler':
                    $this->setAlquiler($value);
                break;
                case 'traspaso':
                    $this->setTraspaso($value);
                break;
                case 'vacacional':
                    $this->setVacacional($value);
                break;
                case 'venta':
                    $this->setVenta($value);
                break;
                default:
                    throw new \Exception(self::$ERROR_INDICE_NO_ADMITIDO);
                break;
            }   
        } 
    }

    public function setAlquiler(\inmotek\model\inmueble\trade\Alquiler $alquiler){
        $this->holder['alquiler'] = $alquiler;
    }

    public function setTraspaso(\inmotek\model\inmueble\trade\Traspaso $traspaso){
        $this->holder['traspaso'] = $traspaso;
    }

    public function setVacacional(\inmotek\model\inmueble\trade\Vacacional $vacacional){
        $this->holder['vacacional'] = $vacacional;
    }

    public function setVenta(\inmotek\model\inmueble\trade\Venta $venta){
        $this->holder['venta'] = $venta;
    }

    /**
     * Recupera los datos del alquiler
     * @return \inmotek\model\inmueble\trade\Alquiler
     */
    public function getAlquiler() : \inmotek\model\inmueble\trade\Alquiler{
        if(null == $this->holder['alquiler']){
            $this->holder['alquiler'] = new \inmotek\model\inmueble\trade\Alquiler();
        }
        return $this->holder['alquiler'];
    }

    /**
     * Recupera los datos del traspaso
     * @return \inmotek\model\inmueble\trade\Traspaso
     */
    public function getTraspaso() : \inmotek\model\inmueble\trade\Traspaso{
        if(null == $this->holder['traspaso']){
            $this->holder['traspaso'] = new \inmotek\model\inmueble\trade\Traspaso();
        }
        return $this->holder['traspaso'];
    }

    /**
     * Recupera los datos del vacacional
     * @return \inmotek\model\inmueble\trade\Vacacional
     */
    public function getVacacional($vacacional) : \inmotek\model\inmueble\trade\Vacacional{
        if(null == $this->holder['vacacional']){
            $this->holder['vacacional'] = new \inmotek\model\inmueble\trade\Vacacional();
        }
        return $this->holder['vacacional'];
    }

    /**
     * Recupera los datos del venta
     * @return \inmotek\model\inmueble\trade\Venta
     */
    public function getVenta($venta) : \inmotek\model\inmueble\trade\Venta{
        if(null == $this->holder['venta']){
            $this->holder['venta'] = new \inmotek\model\inmueble\trade\Venta();
        }
        return $this->holder['venta'];
    }

    public function isAlquiler(){
        return $this->offsetExists('alquiler');
    }

    public function isTraspaso(){
        return $this->offsetExists('traspaso');
    }

    public function isVacacional(){
        return $this->offsetExists('vacacional');
    }

    public function isVenta(){
        return $this->offsetExists('venta');
    }

    /**
     * Verifica si el índice que se va a meter es válido
     */
    public function offsetAdmitido($offset){
        return in_array($offset, self::$INDICES_ADMITIDOS);
    }

    /**
     * implementacion para la interfaz ArrayAccess
     * Comprueba si existe o no un indice
     * @param int $offset
     * @return bool
     */
    public function offsetExists($offset) 
    {
        if(!$this->offsetAdmitido($offset)){
            throw new \Exception(self::$ERROR_INDICE_NO_ADMITIDO);
        }
        return (null != $this->holder[$offset])?true:false;
    }

    /**
     * implementacion para la interfaz ArrayAccess
     * Destruye una posicion del offset
     * @param type $offset
     */
    public function offsetUnset($offset) 
    {
        if(!$this->offsetAdmitido($offset)){
            throw new \Exception(self::$ERROR_INDICE_NO_ADMITIDO);
        }
        $this->holder[$offset] = null;
    }

    /**
     * implementacion para la interfaz ArrayAccess
     * Recupera una posicion del array
     * @param type $offset
     * @return type
     */
    public function offsetGet($offset) 
    {
        if(!$this->offsetAdmitido($offset)){
            throw new \Exception(self::$ERROR_INDICE_NO_ADMITIDO);
        }
        return $this->holder[$offset];
    }
    
    /**
     * Implementación para la interfaz countable
     * @return int
     */
    public function count(){
        return count($this->holder);
    }    
}
