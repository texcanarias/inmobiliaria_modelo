<?php 
declare(strict_types = 1);
namespace inmotek\model\inmobiliaria\factura;

/**
 * Factura 
 * Datos generales de la factura.
 * 
 */
class Factura {

    /**
     * Identificador único 
     * @var integer
     *
     */
    private $id;

    /**
     * Identificador del usario al que se le factura
     * @var integer
     *
     */
    private $id_usuario;

    /**
     * Identificador del contrato
     * @var integer
     *
     */
    private $id_contrato;

    /**
     * Fecha de facturación
     * @var date
     *
     */
    private $fecha;

    /**
     * Fecha de vencimiento
     * @var date
     *
     */
    private $vencimiento;

    /**
     * Indica si la factura ha sido pagada
     * @var boolean
     *
     */
    private $pagada;

    /**
     * Fecha de pago de la factura
     * @var date
     *
     */
    private $fecha_pago;

    /**
     * Modo de pago. Los modos de pago son t.- Tranferencia, c.- contado, w.- TPV virtual, v.- TPV físico, r.- recibo domiciliado
     * @var char 
     *
     */
    private $modo_pago;

    /**
     * Nota de pago. Descripción sobre el pago.
     * @var string
     *
     */
    private $notas_pago;

    /**
     * Identificador del inmueble
     * @var int
     *
     */
    private $id_inmueble;

    /**
     * Identificador de cliente
     * @var int
     *
     */
    private $id_cliente;

    /**
     * Identificador de comunidad
     * @var int
     *
     */
    private $id_comunidad;

    /**
     * Identificador de promocion
     * @var int
     *
     */
    private $id_promocion;

    /**
     * Labels
     * @var array
     *
     */
    private $etiquetas;

    /**
     * Notas adjuntas a la factura
     * @var string
     *
     */
    private $notas;

    /**
     * Tipo de factura
     * @var string
     *
     */
    private $tipo_factura;

    /**
     * Factura relacionada con p.- promoción, c.-comunidad, i.-inmueble
     * @var char
     *
     */
    private $relacionar_a;

    /**
     * Estado de la factura r.-remesada, c.-reclamada, d.-devuelta
     * @var char
     *
     */
    private $estado;

    /**
     * Fecha de devolución
     * @var date
     *
     */
    private $fecha_devolucion;

    /**
     * Motivo de la devolución
     * @var string
     *
     */
    private $motivo_devolucion;

    /**
     * Fecha y hora de la reclamación
     * @var datetime
     *
     */
    private $envio_reclamacion;

    
    /**
     * Detalle de cada una de las facturas
     * @var factura_detalle
     * 
     */
    private $detalle;
}
