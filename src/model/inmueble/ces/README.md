```plantuml
@startuml

package inmotek <<Frame>> {
    package model <<Frame>> {
        package inmueble <<Frame>> {
            package ces <<Frame>> {            
                class Ces
                class CesCore
            }
        }
    }
}

class CesCore{
    + {static} LETRAS
    - letra
    - valor
    - __construct()
    + {static} factory(valor, letra)
    + getValor()
    + getLetra()
    + setValor(valor)
    + setLetra(letra)
}

class Ces{
    - exentoCes
    - cos2 : cesCore
    - kw : cesCore
    - registryNumber
    - info
    - {static} __construct(exentoCes, co2, kw, registryNumber, info)
    + {static} factory(exentoCes, co2, kw, registryNumber, info)
    + getExentoCes()
    + getCo2() : cesCore
    + getKw() : cesCore
    + getRegistryNumber()
    + getInfo()
}

Ces o-- CesCore

@enduml
```