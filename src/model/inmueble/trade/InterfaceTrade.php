<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\trade;

interface InterfaceTrade{
    public function getPrecio() : float;
    public function setPrecio(float $precio);
}