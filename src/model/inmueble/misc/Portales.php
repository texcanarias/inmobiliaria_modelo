<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble;

/**
 * @todo Comprobar si está deprecated
 */
class Portales {

    private $id_inmueble;
    private $spainhouses = '0';
    private $inmogeo = '0';
    private $pisoscom = '0';
    private $pisoscom_direccion = 'f';
    private $puntospisoscom = 0;
    private $fotocasa = '0';
    private $fotocasa_direccion = 'f';
    private $fotocasa_fecha_creacion = '';
    private $fotocasa_fecha_modificacion = '';
    private $facilisimo = '0';
    private $globaliza = '0';
    private $anaconda = '0';
    private $anaconda_fecha_creacion = '';
    private $anaconda_fecha_modificacion = '';
    private $tucasa = '0';
    private $tucasa_direccion = 'f';
    private $yaencontre = '0';
    private $yaencontre_direccion = 'f';
    private $idealista = '0';
    private $idealista_direccion = 'f';
    private $portales = '0';
    private $portales_destacado = '0';
    private $interempresas = '0';
    private $ibuscando = '0';
    private $hogaria = '0';
    private $urbaniza = '0';
    private $urbaniza_direccion = 'f';
    private $globinmo = '0';
    private $inmotools = '0';
    private $inmotools_direccion = 'f';
    private $kyero = '0';
    private $coapi = '0';
    private $coapi_direccion = 'f';
    private $g15 = '0';
    private $g15_direccion = 'f';
    private $habitaclia = '0';
    private $vivastreet = '0';
    private $immoweb = '0';
    private $domaza = '0';
    private $misoficinas = '0';

}
