<?php 
declare(strict_types = 1);
namespace inmotek\model\inmobiliaria\cliente;

/**
 * Modelos para clientes, que podrían ser tanto clientes finales como propietarios
 * @SWG\Definition()
 */
class ClienteRelacion extends ClienteBase{

    /**
     * Identifica si este cliente es familiar del que lo tiene referido
     * @var boolean
     *
     */     
    private $is_relation;
    
    /**
     * Descipcion de la relación 
     * @var string
     *
     */
    private $relacion;
    
    public function __construct($id, $nombre) {
        parent::__construct($id, $nombre);
    }

}
