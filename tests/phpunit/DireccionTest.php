<?php namespace inmotek_test\phpunit;

use PHPUnit\Framework\TestCase;

final class direccionTest extends TestCase {

    public function testDireccion() {

        $dir = \inmotek\model\base\Direccion::factory('clave','ES', 35, 'Las Palmas', 35012, 'LPGC', '35012', 20.00, 20.01);

        $this->assertTrue( 'ES' == $dir->getPais() );
        $this->assertTrue( 35 == $dir->getProvincia()->getId() );
        $this->assertTrue( 'Las Palmas' == $dir->getProvincia()->getName() );
        $this->assertTrue( 35012 == $dir->getLocalidad()->getId() );
        $this->assertTrue( 'LPGC' == $dir->getLocalidad()->getName() );
        $this->assertTrue( '35012' == $dir->getCp() );
        $this->assertTrue( 20.00 == $dir->getGps()->getLatitude() );
        $this->assertTrue( 20.01 == $dir->getGps()->getLongitude() );

    }

}
