<?php namespace inmotek_test\phpunit;

use PHPUnit\Framework\TestCase;
use inmotek\model\inmobiliaria\Inmobiliaria;
use inmotek\model\inmueble\media\Video;
use inmotek\model\inmueble\media\VideoList;
use inmotek\model\inmueble\media\Plano;
use inmotek\model\inmueble\media\PlanoList;
use inmotek\model\inmueble\media\Documento;
use inmotek\model\inmueble\media\DocumentoList;
use inmotek\model\inmueble\media\Imagen;
use inmotek\model\inmueble\media\ImagenList;

final class inmuebleMediaTest extends TestCase {

    private function getInmu(){
        $inmo = new Inmobiliaria(1, 'PRO', "Pruebas y text,S.A.");
        $inmo->setLetra('PR');
        $inmo->setToken('zztop123');
        $inmo->setDemo(false);
        $inmo->setSistema(true);        

        $direccion = \inmotek\model\inmueble\Localizacion::factoryLocalizacion('clave','ES', 35, 'Las Palmas', 35012, 'LPGC', '35012', 20.00, 20.01, 'Don Pío Coronado', '172', 'Unica', '2º', 'A');
        $inmo->setDireccion($direccion);

        $usuarios = new \inmotek\model\inmobiliaria\oficina\usuario\UsuarioList();
        $usuarios[] = new \inmotek\model\inmobiliaria\oficina\usuario\Usuario(1, "Roberto Fernandez");
        $usuarios[0]->setDireccion($direccion);

        $oficinas = new \inmotek\model\inmobiliaria\oficina\OficinaList();
        $oficinas[] = new \inmotek\model\inmobiliaria\oficina\Oficina(1,"Central");
        $oficinas[0]->setDireccion($direccion);
        $oficinas[0]->setPrincipal(true);
        $oficinas[0]->setOrden(0);
        $oficinas[0]->setUsuarios($usuarios);
        $inmo->setOficinas($oficinas);


        $inmueble = \inmotek\model\inmueble\Inmueble::factoryInmueble(1, "Casa palaciega",$inmo);
        return $inmueble;
    }

    public function testVideo(){
        $coleccion = new VideoList();

        $video = Video::factoryVideo(1, 'test', 'video_666.mov');
        $video->setDescripcion("");
        $video->setTexto("");
        $video->setOrden(1);
        $video->setPublicar(true);
        $video->setTxt(''); 
        $video->setMime('');

        $video2 = clone $video;
        $video2->setId(2);
        $video2->setName('video_667.mov');

        $ordenAnterior = $video->getOrden();
        $video2->setOrden(++$ordenAnterior);

        $coleccion[] = $video;
        $coleccion[] = $video2;

        $this->assertTrue( 1 == $coleccion[0]->getId() );
        $this->assertTrue( 2 == $coleccion[1]->getId() );
    }

    public function testPlano(){
        $coleccion = new PlanoList();

        $plano = Plano::factoryPlano(1, 'test', 'plano666.jpeg');
        $plano->setDescripcion("");
        $plano->setTexto("");
        $plano->setOrden(1);
        $plano->setPublicar(true);
        $plano->setTxt(''); 
        $plano->setMime('');

        $plano2 = clone $plano;
        $plano2->setId(2);
        $plano2->setName('plano_667.jpeg');

        $ordenAnterior = $plano->getOrden();
        $plano2->setOrden(++$ordenAnterior);

        $coleccion[] = $plano;
        $coleccion[] = $plano2;

        $this->assertTrue( 1 == $coleccion[0]->getId() );
        $this->assertTrue( 2 == $coleccion[1]->getId() );
    }

    public function testDocumento(){
        $coleccion = new DocumentoList();

        $item = Documento::factoryDocumento(1, 'test', 'contrato.doc');
        $item->setDescripcion("");
        $item->setTexto("");
        $item->setOrden(1);
        $item->setPublicar(true);
        $item->setTxt(''); 
        $item->setMime('');

        $item2 = clone $item;
        $item2->setId(2);
        $item2->setName('recibo.doc');

        $ordenAnterior = $item->getOrden();
        $item2->setOrden(++$ordenAnterior);

        $coleccion[] = $item;
        $coleccion[] = $item2;

        $this->assertTrue( 1 == $coleccion[0]->getId() );
        $this->assertTrue( 2 == $coleccion[1]->getId() );
    }

    public function testImagen(){
        $coleccion = new ImagenList();

        $item = Imagen::factoryImagen(1, 'test', 'img20.jpeg');
        $item->setDescripcion("");
        $item->setTexto("");
        $item->setOrden(1);
        $item->setPublicar(true);
        $item->setTxt(''); 
        $item->setMime('');

        $item2 = clone $item;
        $item2->setId(2);
        $item2->setName('img22.jpeg');

        $ordenAnterior = $item->getOrden();
        $item2->setOrden(++$ordenAnterior);

        $coleccion[] = $item;
        $coleccion[] = $item2;

        $this->assertTrue( 1 == $coleccion[0]->getId() );
        $this->assertTrue( 2 == $coleccion[1]->getId() );
    }

    public function testIntegracionInmuebleMedia(){

        $inmu = $this->getInmu();

        $coleccionI = new imagenList();
        $coleccionI[] = Imagen::factoryImagen(1, 'test', 'contrato_666.jpeg');
        $inmu->setImagen($coleccionI);

        $coleccionV = new VideoList();
        $coleccionV[] = Video::factoryVideo(1, 'test', 'video_666.mov');
        $inmu->setVideo($coleccionV);

        $coleccionD = new DocumentoList();
        $coleccionD[] = Documento::factoryDocumento(1, 'test', 'contrato_666.pdf');
        $inmu->setDocumento($coleccionD);

        $coleccionP = new PlanoList();
        $coleccionP[] = Plano::factoryPlano(1, 'test', 'plano_666.pdf');
        $inmu->setplano($coleccionP);
    

        $this->assertTrue( 'contrato_666.jpeg' == $inmu->getImagen()[0]->getName() );
        $this->assertTrue( 'video_666.mov' == $inmu->getVideo()[0]->getName() );
        $this->assertTrue( 'contrato_666.pdf' == $inmu->getDocumento()[0]->getName() );
        $this->assertTrue( 'plano_666.pdf' == $inmu->getPlano()[0]->getName() );
    }

}
