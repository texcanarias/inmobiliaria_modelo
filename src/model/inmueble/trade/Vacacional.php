<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\trade;

class Vacacional extends Alquiler {
    static $PENSION_SOLO_DESAYUNO = 3;
    static $PENSION_MEDIA = 1;
    static $PENSION_COMPLETA = 2;

    /**
     * Código turístico
     * @var string
     */
    private $codigoTuristico;

    /**
     * Limpieza incluida
     * @var bool
     */
    private $limpiezaIncluida;

    /**
     * Ropa de cama incluida
     * @var bool
     */
    private $ropaCamaIncluida;

    /**
     * Tipo de pension
     * @var int
     */
    private $tipoPension;

    /**
     * Get código turístico
     *
     * @return  string
     */ 
    public function getCodigoTuristico()
    {
        return $this->codigoTuristico;
    }

    /**
     * Set código turístico
     *
     * @param  string  $codigoTuristico  Código turístico
     *
     * @return  self
     */ 
    public function setCodigoTuristico(string $codigoTuristico)
    {
        $this->codigoTuristico = $codigoTuristico;

        return $this;
    }

    /**
     * Get limpieza incluida
     *
     * @return  bool
     */ 
    public function getLimpiezaIncluida() : ?bool
    {
        return $this->limpiezaIncluida;
    }

    /**
     * Set limpieza incluida
     *
     * @param  bool  $limpiezaIncluida  Limpieza incluida
     *
     * @return  self
     */ 
    public function setLimpiezaIncluida(bool $limpiezaIncluida)
    {
        $this->limpiezaIncluida = $limpiezaIncluida;

        return $this;
    }

    /**
     * Get ropa de cama incluida
     *
     * @return  bool
     */ 
    public function getRopaCamaIncluida() : ?bool
    {
        return $this->ropaCamaIncluida;
    }

    /**
     * Set ropa de cama incluida
     *
     * @param  bool  $ropaCamaIncluida  Ropa de cama incluida
     *
     * @return  self
     */ 
    public function setRopaCamaIncluida(bool $ropaCamaIncluida)
    {
        $this->ropaCamaIncluida = $ropaCamaIncluida;

        return $this;
    }

    /**
     * Get tipo de pension
     *
     * @return  int
     */ 
    public function getTipoPension() :int
    {
        return $this->tipoPension;
    }

    /**
     * Set tipo de pension
     *
     * @param  int  $tipoPension  Tipo de pension
     *
     * @return  self
     */ 
    public function setTipoPension(int $tipoPension)
    {
        $this->tipoPension = $tipoPension;

        return $this;
    }
}