<?php namespace inmotek_test\phpunit;

use PHPUnit\Framework\TestCase;
use inmotek\model\inmobiliaria\Inmobiliaria;

final class inmueblePortalTest extends TestCase {

    private function getInmu(){
        $inmo = new Inmobiliaria(1, "PRO", "Pruebas y text,S.A.");
        $inmo->setLetra('PR');
        $inmo->setToken('zztop123');
        $inmo->setDemo(false);
        $inmo->setSistema(true);        

        $direccion = \inmotek\model\inmueble\Localizacion::factoryLocalizacion('test','ES', 35, 'Las Palmas', 35012, 'LPGC', '35012', 20.00, 20.01, 'Don Pío Coronado', '172', 'Unica', '2º', 'A');
        $inmo->setDireccion($direccion);

        $usuarios = new \inmotek\model\inmobiliaria\oficina\usuario\UsuarioList();
        $usuarios[] = new \inmotek\model\inmobiliaria\oficina\usuario\Usuario(1, "Roberto Fernandez");
        $usuarios[0]->setDireccion($direccion);

        $oficinas = new \inmotek\model\inmobiliaria\oficina\OficinaList();
        $oficinas[] = new \inmotek\model\inmobiliaria\oficina\Oficina(1,"Central");
        $oficinas[0]->setDireccion($direccion);
        $oficinas[0]->setPrincipal(true);
        $oficinas[0]->setOrden(0);
        $oficinas[0]->setUsuarios($usuarios);
        $inmo->setOficinas($oficinas);


        $inmueble = \inmotek\model\inmueble\Inmueble::factoryInmueble(1,  "Casa palaciega",$inmo);

        $portal = \inmotek\model\portal\PortalVisibilidad::factory(\inmotek\model\portal\PortalVisibilidad::$MOSTRAR_DIRECCION_COMPLETA);
        $inmueble->setPortalVisibilidad($portal);


        return $inmueble;
    }

    private function getInmuAlt(){
        $inmueble = clone $this->getInmu();

        $portal = \inmotek\model\portal\PortalVisibilidadGratuito::factory(null);
        $inmueble->setPortalVisibilidad($portal);

        return $inmueble;
    }


    public function testInmueble() {

        $inmueble = $this->getInmu();
        $inmuebleAlt = $this->getInmuAlt();

        $this->assertTrue(\inmotek\model\portal\PortalVisibilidad::$MOSTRAR_DIRECCION_COMPLETA == $inmueble->getPortalVisibilidad()());
        $this->assertTrue(\inmotek\model\portal\PortalVisibilidad::$MOSTRAR_DIRECCION_SOLO_CALLE == $inmuebleAlt->getPortalVisibilidad()());
    }
}
