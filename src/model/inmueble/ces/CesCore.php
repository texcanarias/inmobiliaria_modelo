<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\ces;

class CesCore{
    public static $LETRAS = ['A', 'B', 'C', 'D', 'E', 'F', 'G'];

    /**
     * Valor numético
     * @float
     */
    private ?float $valor = null;

    /**
     * Letra
     * @string
     */
    private ?string $letra =  null;

    private function __construct(float $valor = 0, string $letra = ''){
        try{
            $this->setLetra($letra);
            $this->setValor($valor);
        }
        catch(\Exception $e){
            throw $e;
        }
    }

    public static function factory(float $valor = 0, string $letra = ''){
        try{        
            $c = new self($valor, $letra);
            return $c;
        }
        catch(\Exception $e){
            throw $e;
        }
    }

    /**
     * Get the value of valor
     */ 
    public function getValor() : float
    {
        return $this->valor;
    }
  

    /**
     * Get the value of letra
     */ 
    public function getLetra() : string
    {
        return $this->letra;
    }

    /**
     * Set the value of valor
     *
     * @return  self
     */ 
    public function setValor(float $valor)
    {
        $isValorNegativo = 0 > $valor;
        if($isValorNegativo){
            throw new \Exception('El valor numérico para valor no puede ser negativo.');
        }
        $this->valor = $valor;

        return $this;
    }

    /**
     * Set the value of letra
     *
     * @return  self
     */ 
    public function setLetra(string $letra)
    {
        $isValorPermitido = '' != $letra && in_array($letra, self::$LETRAS);
        if(!$isValorPermitido){
            throw new \Exception('El valor para letra no es correcto.');
        }
        $this->letra = $letra;

        return $this;
    }
}