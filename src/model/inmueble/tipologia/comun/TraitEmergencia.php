<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\tipologia\comun;

trait TraitEmergencia
{
    /**
     * Salidas de emergencia
     * @var bool
     */
    private ?bool $emergenciaSalida = null;

    /**
     * Luces de emergencia
     * @var bool
     */
    private ?bool $emergenciaLuces = null;

    

    /**
     * Get salidas de emergencia
     *
     * @return  bool
     */ 
    public function getEmergenciaSalida() : ?bool
    {
        return $this->emergenciaSalida;
    }

    /**
     * Set salidas de emergencia
     *
     * @param  bool  $emergenciaSalida  Salidas de emergencia
     *
     * @return  self
     */ 
    public function setEmergenciaSalida(?bool $emergenciaSalida) : self
    {
        $this->emergenciaSalida = $emergenciaSalida;

        return $this;
    }

    /**
     * Get luces de emergencia
     *
     * @return  bool
     */ 
    public function getEmergenciaLuces() : ?bool
    {
        return $this->emergenciaLuces;
    }

    /**
     * Set luces de emergencia
     *
     * @param  bool  $emergenciaLuces  Luces de emergencia
     *
     * @return  self
     */ 
    public function setEmergenciaLuces(?bool $emergenciaLuces) : self
    {
        $this->emergenciaLuces = $emergenciaLuces;

        return $this;
    }
}