<?php 
declare(strict_types = 1);
namespace inmotek\model\base\comunicacion;

use \inmotek\model\base\ArrayStorage;

class ComunicacionList extends ArrayStorage {

    public function __construct() {
        parent::__construct('inmotek\model\base\comunicacion\Comunicacion');
    }
}
