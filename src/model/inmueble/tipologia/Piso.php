<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\tipologia;

use \inmotek\model\inmueble\caracteristica\Calefaccion;
class Piso implements InterfaceTipologia{

  public static $SUBTIPO_APARTAMENTO = 0;
  public static $SUBTIPO_ATICO = 1;
  public static $SUBTIPO_BAJO = 2;
  public static $SUBTIPO_BUHARDILLA = 3;
  public static $SUBTIPO_DUPLEX = 4;
  public static $SUBTIPO_ESTUDIO = 5;
  public static $SUBTIPO_LOFT = 6;
  public static $SUBTIPO_PISO = 7;
  public static $SUBTIPO_QUADPLEX = 8;
  public static $SUBTIPO_TRIPLEX = 9;

  use comun\TraitSubtipo;      
  use comun\TraitCes;
  use comun\TraitEstado;  
  use comun\TraitAnyoConstruccion;
  use comun\TraitSuperficieConstruida;
  use comun\TraitSuperficieUtil;
  use comun\TraitSuperficieAjardinada;
  use comun\TraitEdificio;
  use comun\TraitVivienda;
  use comun\TraitOrientacion;
  use comun\TraitBanyo;
  use comun\TraitSeguridad;
  use comun\TraitEmergencia;
  use comun\TraitAccesibilidad;
  use comun\TraitDomotica;
  use comun\TraitDeporteOcio;


    public function getTipologia() : self{
      return $this;
    }

    public function getTipo() : string{
      return "Piso";
  }
  
}