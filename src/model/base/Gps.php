<?php 
declare(strict_types = 1);
namespace inmotek\model\base;

/**
 * Localizacion GPS
 */
class Gps {      
    /**
     * Latitud GPS
     * @var float
     */
    private float $latitude; 

    /**
     * Longitud GPS
     * @var float
     */
    private float $longitude; 

    private function __construct(){
    }

    static public function factory(float $latitude = 0, float $longitude = 0) : self{
        try{
            $gps = new self();
            $gps->setLatitude($latitude + (rand (-999 , 999) / 10000) );
            $gps->setLongitude($longitude + (rand (-999 , 999) / 10000) );
        return $gps;
        }
        catch (\Exception $ex){
            throw $ex;
        }
    }
    

    public function getLatitude() : float{
        return $this->latitude;
    }

    private function setLatitude(float $latitude):self{
        if(90<$latitude OR -90>$latitude){
            throw new \Exception("El valor latitud no puede ser mayor que 90 o menor que -90");
        }
        $this->latitude = $latitude;
        return $this;
    }

    public function getLongitude():float{
        return $this->longitude;
    }

    private function setLongitude(float $longitude) : self{
        if(180<$longitude OR -180>$longitude){
            throw new \Exception("El valor longitud no puede ser mayor que 180 o menor que -190");
        }
        $this->longitude = $longitude;
        return $this;
    }
}