<?php 
declare(strict_types = 1);
namespace inmotek\model\base;

class DuplaIdNombre 
    extends \inmotek\model\base\ModelEntidad
{    
    
    /**
     * Nombre
     * @var string
     *
     */
    private string $name = "";   
    
    protected function __construct(?int $id, string $clave = "", string $name = "") {
        parent::__construct($id, $clave);
        $this->name = $name;
    }

    public function factory(?int $id, string $clave = "", string $name) : self{
        return new self($id, $clave, $name);
    }

    function getName() : string{
        return $this->name;
    }

    public function setName(string $name) : self{
        $this->name = $name;
        return $this;
    }


    
}