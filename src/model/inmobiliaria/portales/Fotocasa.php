<?php 
declare(strict_types = 1);
namespace inmotek\model\inmobiliaria\portales;

/**
 * Datos de idealista relacionados con la inmobiliaria
 */
class Fotocasa{

    /**
     * @var bool Si está declarado para volvar en idealista (en la base de datos fotocasa)
     */
    private $activo;

    /**
     * @var int Tipo de contacto 1 .- , 2 .- , 3 .- , 4 .-  (en la base de datos fotocasa_tipo_contacto)
     */
    private $tipoContacto;

    /**
     * @var string email de contacto (en la base de datos fotocasa_mailcontacto)
     */
    private $mailContacto;

    /**
     * @var string email de contacto (en la base de datos fotocasa_telefonocontacto)
     */
    private $telefonoContacto;

    /**
     * @var bool Si está en cartera (en la base de datos fotocasa_cartera)
     */
    private $cartera;

    /**
     * @var bool Si es para terceros (en la base de datos fotocasa_terceros) Foto casa exporta a otros portales. Si está a true permite la exporación.
     */
    private $terceros;


    public function setActivo($item){
        $this->activo = $item;
        return $this;
    }

    public function setCartera($item){
        $this->cartera = $item;
        return $this;
    }

    public function setTipoContacto($item){
        $this->tipoContacto = $item;
        return $this;
    }

    public function setMailcontacto($item){
        $this->mailContacto = $item;
        return $this;
    }

    public function setTelefonoContacto($item){
        $this->telefonoContacto = $item;
        return $this;
    }

    public function setTerceros($item){
        $this->terceros = $item;
        return $this;
    }
}