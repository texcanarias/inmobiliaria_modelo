<?php 
declare(strict_types = 1);
namespace inmotek\model\base;


trait TraitToString{
    
    /**
     * Método destinado a mostrar todas las propiedades del objeto
     * @return string
     */    
    public  function __toString(){
        $properties = get_object_vars ( $this );
        $cadena = "";
        foreach ($properties as $property => $value) {
            $cadena .= $property." = ".$value."\n";
        }
        return $cadena;
    }    
}