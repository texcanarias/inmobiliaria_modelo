```plantuml
@startuml

package inmotek <<Frame>> {
    package model <<Frame>> {
        package inmueble <<Frame>> {
            package media <<Frame>> {
                class InterfaceMedia
                class Media
                class Documento
                class DocumentoList
                class Imagen
                class ImagenList
                class Plano
                class PlanoList
                class Video
                class VideoList
            }
        }
        package base <<Frame>> {
            class ArrayStorage
            class DuplaIdNombre{
                - nombre
                + getName()
                + setName()
            }
        }
    }
}

interface InterfaceMedia {
    + getDescripcion()
    + getTexto()
    + getOrden()
    + getPublicar()
    + getTxt()
    + getMime()
}

Media --- InterfaceMedia
DuplaIdNombre <|-- Media

class Media {
    - descripcion
    - texto
    - orden
    - publicar
    - txt
    - mime
    + getDescripcion()
    + getTexto()
    + getOrden()
    + getPublicar()
    + getTxt()
    + getMime()
}

class DocumentoList {
    + __construct()
}
class Documento {
}

Media <|-- Documento
ArrayStorage <|-- DocumentoList
DocumentoList *-- Documento

class ImagenList {
    + __construct()
}
class Imagen {
}

Media <|-- Imagen
ArrayStorage <|-- ImagenList
ImagenList *-- Imagen

class PlanoList {
    + __construct()
}
class Plano {
}

Media <|-- Plano
ArrayStorage <|-- PlanoList
PlanoList *-- Plano

class VideoList {
    + __construct()
}
class Video {
}

Media <|-- Video
ArrayStorage <|-- VideoList
VideoList *-- Video

@enduml
```