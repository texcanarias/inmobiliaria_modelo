<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble\media;

class Media extends \inmotek\model\base\DuplaIdNombre implements InterfaceMedia{


    /**
     * Identificador del inmueble al que pertenece el recurso
     */
    private ?int $idInmueble = null;

    /**
     * Identificador de la promocion a la que pertenece el recurso
     */
    private ?int $idPromocion = null;

    /**
     * Descripción del documento
     * @var string
     *
     */
    private string $descripcion;

    /**
     * Texto del documento
     * @var string
     *
     */
    private string $texto;

    /**
     * Orden de presentacion
     * @var int 
     *
     */
    private int $orden = 0;
    
    /**
     * Documento privado
     * @var bool
     *
     */
    private bool $publicar = false;
    
    /**
     * Texto largo
     * @var string 
     */
    private string $txt; 
       
    /**
     *
     * @var string Tipo mime del fichero
     */
    private string $mime;

    /**
     * Get descripción del documento
     *
     * @return  string
     */ 
    public function getDescripcion() : string
    {
        return $this->descripcion;
    }

    /**
     * Get texto del documento
     *
     * @return  string
     */ 
    public function getTexto() : string
    {
        return $this->texto;
    }

    /**
     * Get orden de presentacion
     *
     * @return  int
     */ 
    public function getOrden() : int
    {
        return $this->orden;
    }

    /**
     * Get documento privado
     *
     * @return  bool
     */ 
    public function getPublicar() : bool
    {
        return $this->publicar;
    }

    /**
     * Get texto largo
     *
     * @return  string
     */ 
    public function getTxt() : string
    {
        return $this->txt;
    }

    /**
     * Get tipo mime del fichero
     *
     * @return  string
     */ 
    public function getMime() : string
    {
        return $this->mime;
    }

    /**
     * Set descripción del documento
     *
     * @param  string  $descripcion  Descripción del documento
     *
     * @return  self
     */ 
    public function setDescripcion(string $descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Set texto del documento
     *
     * @param  string  $texto  Texto del documento
     *
     * @return  self
     */ 
    public function setTexto(string $texto)
    {
        $this->texto = $texto;

        return $this;
    }

    /**
     * Set orden de presentacion
     *
     * @param  int  $orden  Orden de presentacion
     *
     * @return  self
     */ 
    public function setOrden(int $orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Set documento privado
     *
     * @param  boolean  $publicar  Documento privado
     *
     * @return  self
     */ 
    public function setPublicar(bool $publicar)
    {
        $this->publicar = $publicar;

        return $this;
    }

    /**
     * Set texto largo
     *
     * @param  string  $txt  Texto largo
     *
     * @return  self
     */ 
    public function setTxt(string $txt)
    {
        $this->txt = $txt;

        return $this;
    }

    /**
     * Set tipo mime del fichero
     *
     * @param  string  $mime  Tipo mime del fichero
     *
     * @return  self
     */ 
    public function setMime(string $mime)
    {
        $this->mime = $mime;

        return $this;
    }

    /**
     * Recupera el id que está relacionado con el recurso
     *
     * @return integer
     */
    public function getIdRelacion() : ?int{
        if(null != $this->idInmueble){
            return $this->idInmueble;
        }
        if(null != $this->idPromocion){
            return $this->idPromocion;
        }
        return null;
    }

    /**
     * Get identificador del inmueble al que pertenece el recurso
     */ 
    public function getIdInmueble() : ?int
    {
        return $this->idInmueble;
    }

    /**
     * Set identificador del inmueble al que pertenece el recurso
     *
     * @return  self
     */ 
    public function setIdInmueble(?int $id_inmueble) : self
    {
        $this->idInmueble = $id_inmueble;

        return $this;
    }

    /**
     * Get identificador de la promocion a la que pertenece el recurso
     */ 
    public function getIdPromocion() : ?int
    {
        return $this->idPromocion;
    }

    /**
     * Set identificador de la promocion a la que pertenece el recurso
     *
     * @return  self
     */ 
    public function setIdPromocion(?int $id_promocion) : self
    {
        $this->idPromocion = $id_promocion;

        return $this;
    }
}
