<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble\misc;

class Acciones {

    private $id;
    private $id_accion;
    private $id_usuario;
    private $id_inmueble;
    private $id_cliente;
    private $id_proveedor;
    private $id_promocion;
    private $id_comunidad;
    private $id_mandato;
    private $id_comunicacion;
    private $fecha;
    private $fechafin;

    /**
     * Día completo
     * @var boolean
     *
     */
    private $diacompleto;
    private $hora;
    private $horafin;

    /**
     * Día completo
     * @var string
     *
     */
    private $agencia;

    /**
     * Tipo de acción. V.- Visita ó Visita múltiple,A.- Actividad,O.- Oferta,E.- Evento,L.- Llamada,P.- Actividad con proveedor / competencia,M.- Reservado,I.- Apunte
     * @var char
     *
     */
    private $tipo;
    private $tipoevento;

    /**
     * Importacion. 0.-normal, 1.-baja, 2.-alta
     * @var integer
     *
     */
    private $importancia;

    /**
     * Modificado
     * @var datetime
     *
     */
    private $modificado;

    /**
     * Privado
     * @var interger 0.- Ámbito público, 1.- Ámbito privado, 2.- Ámbito oficina
     *
     */
    private $privado;
    private $asunto;
    private $notas;
    private $notasprivadas;
    private $telefono;
    private $cantidad;
    private $respuesta;
    private $notavisita;
    private $mostraraviso;
    private $cuandoaviso;
    private $momentoaviso;

    /**
     * Aceptado por la inmobiliaria en el caso de un inmueble de la bolsa global
     * @var boolean
     *
     */
    private $aceptada_inmo;

    /**
     * Pendiente
     * @var boolean
     *
     */
    private $pendiente;

    /**
     * Cerrado
     * @var boolean
     *
     */
    private $cerrado;

    /**
     * No atendido
     * @var boolean
     *
     */
    private $no_atendido;

    /**
     * No replicar
     * @var boolean
     *
     */
    private $noreplicar;

    /**
     * SMS enviar
     * @var boolean
     *
     */
    private $smsenviar;

    /**
     * SMS número
     * @var string
     *
     */
    private $smsnumero;

    /**
     * SMS Cuando. 30M.- ,1.-,2.-,6.-,D.-
     * @var string
     *
     */
    private $smscuando;

    /**
     * SMS Momento.
     * @var datetime
     *
     */
    private $smsmomento;

    /**
     * SMS texto
     * @var string
     *
     */
    private $smstxt;

    /**
     * SMS id Identificador del SMS
     * @var int
     *
     */
    private $smsid;

    /**
     * SMS id enviar propiedad
     * @var boolean
     *
     */
    private $smsenviarprop;

    /**
     * SMS numero propiedad
     * @var string
     *
     */
    private $smsnumeroprop;

    /**
     * SMS cuando propiedad 30M.- ,1.- ,2.- ,6.- ,D.-
     * @var string
     *
     */
    private $smscuandoprop;

    /**
     * SMS momento propiead
     * @var datetime
     *
     */
    private $smsmomentoprop;

    /**
     * SMS texto propiedad
     * @var string
     *
     */
    private $smstxtprop;

    /**
     * SMS id propiedad
     * @var integer
     *
     */
    private $smsidprop;

    /**
     * SMS id calendar
     * @var string
     *
     */
    private $id_calendar;

    /**
     * Cadena para búsqueda fonética
     * @var string
     *
     */
    private $ft_field;

    /**
     * etiquetas.
     * @var array
     *
     */
    private $etiquetas;

    /**
     * Firma digital.
     * @var string
     *
     */
    private $firma;


}