<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble;

/**
 * @todo Esta clase es para soportar trabajo con el sistema de persistencia
 */
class Data{
    private $id = null;
    private $id_campo = 0;
    private $entidad = '';
    private $id_entidad = 0;
    private $lang = '';
    private $text_val = '';
    private $int_val = 0;
    private $date_val = '';
    private $varchar_val = '';
    
    /**
     * Contructor de un modelo de Data para inmuebles
     * @return \inmotek\base\model\Data
     */
    static public function factoryDataInmueble(){
        $a = new Data();
        $a->entidad = 1;
        $a->lang = null;
        return $a;
    }
}