<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble\caracteristica;

class AguaCaliente extends CaracteristicaInt{    
    static $ITEMS = [   "NO" => 1,
                        "CENTRAL" => 2,
                        "INDIVIDUAL_GAS_NATURAL" => 3,
                        "INDIVIDUAL_ELECTRICA" => 4,
                        "INDIVIDUAL_GAS_BUTANO" => 5,
                        "INDIVIDUAL_CALOR_AZUL_BAJO_CONSUMO" => 6,
                        "INDIVIDUAL_SOLAR" => 7,
                        "INDIVIDUAL_GASOLEO" => 8,
                        "TOMA_GAS" => 9,
                        "CENTRAL_CON_CONTADOR_INDIVIDUAL" => 10,
                        "RESERVADO" => 11,
                        "SI_SIN_DETALLAR" => 12,
                        "INDIVIDUAL_GAS_PROPANO" => 13];

    static $NO = 1;
    static $CENTRAL = 2;
    static $INDIVIDUAL_GAS_NATURAL = 3;
    static $INDIVIDUAL_ELECTRICA = 4;
    static $INDIVIDUAL_GAS_BUTANO = 5;
    static $INDIVIDUAL_CALOR_AZUL_BAJO_CONSUMO = 6;
    static $INDIVIDUAL_SOLAR = 7;
    static $INDIVIDUAL_GASOLEO = 8;
    static $TOMA_GAS = 9;
    static $CENTRAL_CON_CONTADOR_INDIVIDUAL = 10;
    static $RESERVADO = 11;
    static $SI_SIN_DETALLAR = 12;
    static $INDIVIDUAL_GAS_PROPANO = 13;
    static $AEROTERMIA = 14;

    use TraitFactory;

    protected function isCorrecto(int $item) 
    {
        return self::$NO <= $item AND self::$AEROTERMIA >= $item;
    }

}