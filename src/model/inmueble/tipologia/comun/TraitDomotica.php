<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\tipologia\comun;

trait TraitDomotica
{
    /**
     * Tienes recursos domóticos
     * @var bool
     */
    private ?bool $domotica = null;

    /**
     * Persioanas eléctricas
     * @var bool
     */
    private ?bool $persianasElectricas = null;

    /**
     * Get tienes recursos domóticos
     *
     * @return  bool
     */ 
    public function getDomotica() : ?bool
    {
        return $this->domotica;
    }

    /**
     * Set tienes recursos domóticos
     *
     * @param  bool  $domotica  Tienes recursos domóticos
     *
     * @return  self
     */ 
    public function setDomotica(?bool $domotica) : self
    {
        $this->domotica = $domotica;

        return $this;
    }

    /**
     * Get persioanas eléctricas
     *
     * @return  bool
     */ 
    public function getPersianasElectricas() : ?bool
    {
        return $this->persianasElectricas;
    }

    /**
     * Set persioanas eléctricas
     *
     * @param  bool  $persianasElectricas  Persioanas eléctricas
     *
     * @return  self
     */ 
    public function setPersianasElectricas(?bool $persianasElectricas) : self
    {
        $this->persianasElectricas = $persianasElectricas;

        return $this;
    }
}