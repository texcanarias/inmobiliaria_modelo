<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble\media;

interface InterfaceMedia {

    public function getDescripcion();
    public function getTexto();
    public function getOrden();
    public function getPublicar();
    public function getTxt();
    public function getMime();
}
