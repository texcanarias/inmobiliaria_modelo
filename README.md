# Fichero de modelos del negocio de inmotek

Clases destinadas a ser la base para los modelos de inmotek

## @Todo

. La variable muebles (si la casa está amuablada debería estar dentro de trade/venta o traspaso)
. La variable mueblesalquiler (si la casa está amueblada para el alquiler debe estar dentro de trade/alquioer o turistico)

## Apuntes docker

### Ejecutar el bash en una imagen

docker ps Lista los contenedores
docker exec -t -i container_name /bin/bash

### Eliminar ficheros temporales

docker-compose rm -v

### Arrancar las imagenes

docker-composer up

### Recompila las imagenes

docker-composer build

## Arranque de los tests

* composer install
* composer phpunit

## composer

Ver si el composer está bien estruturado

* composer validate

## Dependecias

<https://github.com/barbieswimcrew/zip-code-validator> Validador de codigos postales
