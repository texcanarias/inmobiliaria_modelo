<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\tipologia\comun;

trait TraitAscensor
{
    /**
     * Número de ascensores
     * @var int
     */
    private ?int $ascensorNumero = null;

    /**
     * Al menos un ascensor en cota 0
     * @var bool 
     */
    private ?bool $ascensorCotaCero = null;

    /**
     * Get número de ascensores
     *
     * @return  int
     */ 
    public function getAscensorNumero() : ?int
    {
        return $this->ascensorNumero;
    }

    /**
     * Set número de ascensores
     *
     * @param  int  $ascensorNumero  Número de ascensores
     *
     * @return  self
     */ 
    public function setAscensorNumero(?int $ascensorNumero) : self
    {
        if(null == $ascensorNumero){
            return $this;
        }

        if(0 > $ascensorNumero){
            throw new \Exception("El número de ascensores debe ser mayor o igual a 0");
        }
        $this->ascensorNumero = $ascensorNumero;

        return $this;
    }

    /**
     * Get al menos un ascensor en cota 0
     *
     * @return  bool
     */ 
    public function getAscensorCotaCero() : ?bool
    {
        return $this->ascensorCotaCero;
    }

    /**
     * Set al menos un ascensor en cota 0
     *
     * @param  bool  $ascensorCotaCero  Al menos un ascensor en cota 0
     *
     * @return  self
     */ 
    public function setAscensorCotaCero(?bool $ascensorCotaCero) : self
    {
        if(null == $ascensorCotaCero){
            return $this;
        }

        $this->ascensorCotaCero = $ascensorCotaCero;
        if($this->ascensorCotaCero AND 0 == $this->ascensorNumero){
            $this->ascensorNumero = 1;
        }

        return $this;
    }

    /**
     * Recupera si hay ascensor disponible
     */
    public function isAscensorDisponible() : bool{
        return null != $this->ascensorNumero AND 0 < $this->ascensorNumero;
    }
}