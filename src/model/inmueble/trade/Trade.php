<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\trade;

class Trade implements InterfaceTrade{
    /**
     * @var float $precio
     */
   private $precio;  

    /**
     * Get the value of precio
     */ 
    public function setPrecio(float $precio)
    {
        $this->precio = $precio;
        return $this;
    }


    /**
     * Get the value of precio
     */ 
    public function getPrecio() : float
    {
        return $this->precio;
    }
}