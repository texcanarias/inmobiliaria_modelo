<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\tipologia\comun;

trait TraitSuperficieUtil{
    /**
     * Superficie útil
     * @var int
     */
    private ?int $superficieUtil = null;

    public function getSuperficieUtil() : ?int{
        return $this->superficieUtil;
    }

    public function setSuperficieUtil(?int $superficieUtil) : self{
        if(null == $superficieUtil){
            return $this;
        }

        if(0 <= $superficieUtil ){
            $this->superficieUtil = $superficieUtil;
        }else{
            throw new \Exception("Error en la superficie util debe estar ser mayor que 0");
        }
        return $this;
    }
}