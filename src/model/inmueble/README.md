# Clases principales para los inmuebles

```plantuml
@startuml

package inmotek <<Frame>> {
    package model <<Frame>> {
        package base <<Frame>> {
            class Gps
            class ArrayStorage
            class DuplaIdNombre
        }
        package inmobiliaria <<Frame>>{
            class Inmobiliaria
            class InmobiliariaList
            class Direccion
            package oficina <<Frame>> {
                package usuario <<Frame>> {
                    class Usuario
                    class UsuarioList
                }
            class Oficina
            class OficinaList
            }
        }
        package inmueble <<Frame>>{
            class Inmueble
            class InmuebleAlt
            class InmuebleBase
            class Localizacion
            class Nota
        }
    }
}

DuplaIdNombre <|-- Nota
DuplaIdNombre <|-- InmuebleBase
Direccion <|-- Localizacion
InmuebleBase <|-- Inmueble

class Nota  {
    + {static} TRANSFER  = 1
    + {static} PUBLIC = 2
    + {static} DELIVERY = 3
    + {static} PRIVATE = 4
    + {static} GROUP = 5
    + {static} ENTIDAD_INMUEBLE = 1
    + {static} ENTIDAD_INTERES_CLIENTE = 2
    - id_inmueble
    - id_idioma
    - tipo_nota
    - function __construct($id = null, $texto, $id_inmueble, $id_idioma, $tipo_nota) 
    + {static} function factory($id, $texto, $id_inmueble, $id_idioma, $tipo_nota)
}


class Localizacion {
    - calle
    - portal
    - escalera
    - piso
    - mano
    + function getCalle()
    + function setCalle( $calle)
    + function getPortal()
    + function setPortal($portal)
    + function getEscalera()
    + function setEscalera($escalera)
    + function getPiso()
    + function setPiso($piso)
    + function getMano()
    + function setMano($mano)
}

@enduml
```