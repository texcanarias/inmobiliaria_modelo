<?php 
declare(strict_types = 1);
namespace inmotek\model\inmobiliaria;

use \inmotek\model\base\ArrayStorage;

class InmobiliariaList extends ArrayStorage {

    public function __construct() {
        parent::__construct('\inmotek\model\inmobiliaria\Inmobiliaria');
    }
}
