<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\tipologia;

use \inmotek\model\inmueble\caracteristica\TipoEstructura;
use \inmotek\model\inmueble\caracteristica\TipoFachada;
use \inmotek\model\inmueble\caracteristica\Exterior;

class Edificio 
  implements InterfaceTipologia{

  public static $SUBTIPO_CASTILLO = 0;
  public static $SUBTIPO_EDIFICIO = 1;
  public static $SUBTIPO_HOTEL = 2;
  public static $SUBTIPO_INDUSTRIAL = 3;
  public static $SUBTIPO_MIXTO = 4;
  public static $SUBTIPO_OFICINA = 5;
  public static $SUBTIPO_RESIDENCIAL = 6;

  use comun\TraitCes;
  use comun\TraitSubtipo;
  use comun\TraitEstado;  
  use comun\TraitAnyoConstruccion;
  use comun\TraitSuperficieConstruida;
  use comun\TraitSuperficieUtil;
  use comun\TraitSuperficieAjardinada;
  use comun\TraitPlantasConstruidas;
  use comun\TraitAscensor;
  use comun\TraitSeguridad;  
  use comun\TraitEmergencia;
  use comun\TraitOrientacion;
  use comun\TraitDomotica;

  /**
   * Edificion de propietarios
   * @var bool
   */
  private ?bool $eficicioPropietarios = null;

  /**
   * Edificion con pisos destinados al alquiler
   * @var bool
   */
  private ?bool $edificioAlquiler = null;

  /**
   * Plazas de garaje en el edificion
   * @var int
   */
  private ?int $plazasGarage = null;

  /**
   * Tipo de estructrura
   * @var \inmotek\model\inmueble\caracteristica\TipoEstructura
   */
  private ?TipoEstructura $tipoEstructura = null;

  /**
   * Tipo de fachada
   * @var \inmotek\model\inmueble\caracteristica\TipoFachada
   */
  private ?TipoFachada $tipoFachada = null;

  
    /**
     * Exterior o interior
     * @var \inmotek\model\inmueble\caracteristica\Exterior
     */
    private ?Exterior $exterior = null;


    /**
     * Número de pisos en la planta
     * @var int
     */
    private ?int $pisosEnPlanta = null;


  public function getTipologia() : self{
    return $this;
  }

  public function getTipo() : string{
    return "Edificio";
  }


  /**
   * Get edificion de propietarios
   *
   * @return  bool
   */ 
  public function getEficicioPropietarios() : ?bool
  {
    return $this->eficicioPropietarios;
  }

  /**
   * Set edificion de propietarios
   *
   * @param  bool  $eficicioPropietarios  Edificion de propietarios
   *
   * @return  self
   */ 
  public function setEficicioPropietarios(?bool $eficicioPropietarios)
  {
    $this->eficicioPropietarios = $eficicioPropietarios;

    return $this;
  }

  

  /**
   * Get edificion con pisos destinados al alquiler
   *
   * @return  bool
   */ 
  public function getEdificioAlquiler() : ?bool
  {
    return $this->edificioAlquiler;
  }

  /**
   * Set edificion con pisos destinados al alquiler
   *
   * @param  bool  $edificioAlquiler  Edificion con pisos destinados al alquiler
   *
   * @return  self
   */ 
  public function setEdificioAlquiler(?bool $edificioAlquiler) : self
  {
    $this->edificioAlquiler = $edificioAlquiler;

    return $this;
  }

  

  /**
   * Get plazas de garaje en el edificion
   *
   * @return  int
   */ 
  public function getPlazasGarage() : ?int
  {
    return $this->plazasGarage;
  }

  /**
   * Set plazas de garaje en el edificion
   *
   * @param  int  $plazasGarage  Plazas de garaje en el edificion
   *
   * @return  self
   */ 
  public function setPlazasGarage(?int $plazasGarage) : self
  {
    $this->plazasGarage = $plazasGarage;

    return $this;
  }



  /**
   * Get tipo de estructrura
   *
   * @return  \inmotek\model\inmueble\caracteristica\TipoEstructura
   */ 
  public function getTipoEstructura() : ?TipoEstructura
  {
    return $this->tipoEstructura;
  }

  /**
   * Set tipo de estructrura
   *
   * @param  \inmotek\model\inmueble\caracteristica\TipoEstructura  $tipoEstructura  Tipo de estructrura
   *
   * @return  self
   */ 
  public function setTipoEstructura(?TipoEstructura $tipoEstructura) : self
  {
    $this->tipoEstructura = $tipoEstructura;

    return $this;
  }

  /**
   * Get tipo de fachada
   *
   * @return  \inmotek\model\inmueble\caracteristica\TipoFachada
   */ 
  public function getTipoFachada() : ?TipoFachada
  {
    return $this->tipoFachada;
  }

  /**
   * Set tipo de fachada
   *
   * @param  \inmotek\model\inmueble\caracteristica\TipoFachada  $tipoFachada  Tipo de fachada
   *
   * @return  self
   */ 
  public function setTipoFachada(TipoFachada $tipoFachada) : self
  {
    $this->tipoFachada = $tipoFachada;

    return $this;
  }

    /**
     * Get exterior o interior
     *
     * @return  \inmotek\model\inmueble\caracteristica\Exterior
     */ 
    public function getExterior() : ?Exterior
    {
        return $this->exterior;
    }

    /**
     * Set exterior o interior
     *
     * @param  \inmotek\model\inmueble\caracteristica\Exterior  $exterior  Exterior o interior
     *
     * @return  self
     */ 
    public function setExterior(?Exterior $exterior) : self
    {
        $this->exterior = $exterior;

        return $this;
    }

    /**
     * Get número de pisos en la planta
     *
     * @return  int
     */ 
    public function getPisosEnPlanta() : ?int
    {
        return $this->pisosEnPlanta;
    }

    /**
     * Set número de pisos en la planta
     *
     * @param  int  $pisosEnPlanta  Número de pisos en la planta
     *
     * @return  self
     */ 
    public function setPisosEnPlanta(?int $pisosEnPlanta) : self
    {
        $this->pisosEnPlanta = $pisosEnPlanta;

        return $this;
    }

}