<?php 
declare(strict_types = 1);
namespace inmotek\model\cms;

use \inmotek\model\inmobiliaria\Inmobiliaria;
use \inmotek\model\base\LogRegistro;


interface InterfaceCms{
      /**
     * Get identificador de la inmobiliaria
     *
     * @return  int
     */ 
    public function getIdInmobiliaria() : int;

    /**
     * Set identificador de la inmobiliaria
     *
     * @param  int  $idInmobiliaria  Identificador de la inmobiliaria
     *
     * @return  self
     */ 
    public function setIdInmobiliaria(int $idInmobiliaria) : self;

    /**
     * Get the value of inmobiliaria
     */ 
    public function getInmobiliaria() : Inmobiliaria;

    /**
     * Set the value of inmobiliaria
     *
     * @return  self
     */ 
    public function setInmobiliaria(Inmobiliaria $inmobiliaria) : self;

    /**
     * Get dominio
     *
     * @return  string
     */ 
    public function getDomain() : string;

    /**
     * Set dominio
     *
     * @param  string  $domain  Dominio
     *
     * @return  self
     */ 
    public function setDomain(string $domain) : self;

    /**
     * Get subdominio
     *
     * @return  string
     */ 
    public function getSubdomain() : string;

    /**
     * Set subdominio
     *
     * @param  string  $subdomain  Subdominio
     *
     * @return  self
     */ 
    public function setSubdomain(string $subdomain) : self;

    /**
     * Get arreglo con los idiomas en los que se puede escribir en la web
     *
     * @return  array
     */ 
    public function getIdiomas();

    /**
     * Set arreglo con los idiomas en los que se puede escribir en la web
     *
     * @param  array  $idiomas  Arreglo con los idiomas en los que se puede escribir en la web
     *
     * @return  self
     */ 
    public function setIdiomas(array $idiomas);

    /**
     * Get fechas de registro del CMS Creation Update Delete
     *
     * @return  LogRegistro
     */ 
    public function getLogRegistro() : LogRegistro;

    /**
     * Set fechas de registro del CMS Creation Update Delete
     *
     * @param  \inmotek\model\base\LogRegistro  $logRegistro  Fechas de registro del CMS Creation Update Delete
     *
     * @return  self
     */ 
    public function setLogRegistro(LogRegistro $logRegistro);
}