<?php
declare(strict_types=1);
namespace inmotek\model\inmueble\caracteristica;

class Amueblado  extends CaracteristicaInt
{
    static $ITEMS = [
        "SIN_DATOS" => 0,
        "AMUEBLADO" => 1,
        "SEMIAMUEBLADO" => 2,
        "NO_AMUEBLADO" => 3
    ];

    static $SIN_DATOS = 0;
    static $AMUEBLADO = 1;
    static $SEMIAMUEBLADO = 2;
    static $NO_AMUEBLADO = 3;

    use TraitFactory;
    

    protected function isCorrecto(int $item)
    {
        return self::$SIN_DATOS <= $item AND self::$NO_AMUEBLADO >= $item;
    }
}
