<?php namespace inmotek_test\phpunit;

use PHPUnit\Framework\TestCase;
use inmotek\model\inmobiliaria\Inmobiliaria;

final class inmobiliariaTest extends TestCase {

    public function testInmobiliario() {
        $inmo = new Inmobiliaria(1, 'PRO', "Pruebas y text,S.A.");
        $inmo->setLetra('PR');
        $inmo->setToken('zztop123');
        $inmo->setDemo(false);
        $inmo->setSistema(true);        

        $direccion = \inmotek\model\inmueble\Localizacion::factoryLocalizacionInmo('test', 'ES', 35, 'Las Palmas', 35012, 'LPGC', '35012', 'Don Pío Coronado');
        $inmo->setDireccion($direccion);

        $usuarios = new \inmotek\model\inmobiliaria\oficina\usuario\UsuarioList();
        $usuarios[] = new \inmotek\model\inmobiliaria\oficina\usuario\Usuario(1, "Roberto Fernandez");
        $usuarios[0]->setDireccion($direccion);

        $oficinas = new \inmotek\model\inmobiliaria\oficina\OficinaList();
        $oficinas[] = new \inmotek\model\inmobiliaria\oficina\Oficina(1,"Central");
        $oficinas[] = new \inmotek\model\inmobiliaria\oficina\Oficina(2,"Veneguera");
        $oficinas[0]->setDireccion($direccion);
        $oficinas[0]->setPrincipal(true);
        $oficinas[0]->setOrden(0);
        $oficinas[0]->setUsuarios($usuarios);
        $oficinas[1]->setDireccion($direccion);
        $oficinas[1]->setPrincipal(false);
        $oficinas[1]->setOrden(1);
        $oficinas[1]->setUsuarios(clone $usuarios);

        $inmo->setOficinas($oficinas);


        $this->assertTrue( 1 == $inmo->getId() );
        $this->assertTrue(2 == count($inmo->getOficinas()));
    }

}
