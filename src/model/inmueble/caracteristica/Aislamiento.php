<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble\caracteristica;

class Aislamiento{
    /**
     * Tiene aislamiento térmico (T en el sistema de persistencia)
     * @var bool
     */
    private ?bool $termico = null;

    /**
     * Tiene aislamiento acustivo (A en el sistema de persistencia)
     */
    private ?bool $acustico = null;


    /**
     * Get tiene aislamiento térmico (T en el sistema de persistencia)
     *
     * @return  bool
     */ 
    public function getTermico(): ?bool
    {
        return $this->termico;
    }

    /**
     * Set tiene aislamiento térmico (T en el sistema de persistencia)
     *
     * @param  bool  $termico  Tiene aislamiento térmico (T en el sistema de persistencia)
     *
     * @return  self
     */ 
    public function setTermico(bool $termico)
    {
        $this->termico = $termico;

        return $this;
    }

    /**
     * Get tiene aislamiento acustivo (A en el sistema de persistencia)
     */ 
    public function getAcustico() : ?bool
    {
        return $this->acustico;
    }

    /**
     * Set tiene aislamiento acustivo (A en el sistema de persistencia)
     *
     * @return  self
     */ 
    public function setAcustico(bool $acustico)
    {
        $this->acustico = $acustico;

        return $this;
    }
}



