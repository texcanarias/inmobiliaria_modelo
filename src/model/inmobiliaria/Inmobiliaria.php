<?php 
declare(strict_types = 1);
namespace inmotek\model\inmobiliaria;

use \inmotek\model\base\DuplaIdNombre;
use \inmotek\model\cms\InterfaceCms;
use \inmotek\model\inmueble\Localizacion;
use \inmotek\model\inmobiliaria\oficina\Oficina;
use \inmotek\model\inmobiliaria\oficina\OficinaList;
use \inmotek\model\inmobiliaria\oficina\usuario\Usuario;
//use inmotek\model\inmobiliaria\portales\Portales as PortalesPortales;

class Inmobiliaria extends DuplaIdNombre {

    /**
     * @var string Prefijo para los nombres de los inmuebles
     */
    private ?string $letra;

    /**
     * @var boolean Si está en modo demo
     */
    private bool $demo = false;

    /**
     * @var bool Si es una inmobiliara de sistema
     */
    private bool $sistema = false;

    /**
     *
     * @var Localizacion Direccion de la inmobiliaria
     */
    private $direccion;
    
    /**
     * @var ?string Token usado por la inmobiliaria para procesos de ideintificacion en el APIV2
     */
    private ?string $token;

    /**
     * @var OficinaList Lista de las oficinas
     */
    private OficinaList $oficinas;

    /**
     * @var Oficina Oficina pricipal de la inmobiliaria
     */
    private ?Oficina $oficinaPrincipal = null;

    /**
     * @var Portales Portales relacionados con la inmobiliaria
     */
    private $portales;

    /**
     * Usuario responsable de la inmobiliaria
     *
     * @var Usuario
     */
    private ?Usuario $usuarioPrincipal = null;

    /**
     * Cms vinculado a la inmobiliaria
     * @var ?InterfaceCms
     */
    private ?InterfaceCms $Cms = null;


    public function __construct(?int $id , string $clave, string $name) {
        parent::__construct($id, $clave, $name);
    }

    public function getDireccion(): Localizacion
    {
        return $this->direccion;
    }

    public function setDireccion(Localizacion $direccion) : self{
        $this->direccion = $direccion;
        return $this;
    }

    public function getOficinas(): OficinaList{
        return $this->oficinas;
    }

    public function setOficinas(OficinaList $oficinas) : self{
        $this->oficinas = $oficinas;
        return $this;
    }

    public function getLetra() : ?string{
        return $this->letra;
    }

    public function getDemo() : bool {
        return $this->demo;
    }

    public function getSistema() : bool {
        return $this->sistema;
    }

    public function getToken(): String {
        return $this->token;
    }


    public function getPorales() {
        return $this->portales;
    }

    public function setLetra(?string $letra) : self {
        $this->letra = $letra;
        return $this;
    }

    public function setDemo(bool $demo) : self {
        $this->demo = $demo;
        return $this;
    }

    public function setSistema(bool $sistema) : self {
        $this->sistema = $sistema;
        return $this;
    }

    public function setToken(String $token) : self {
        $this->token = $token;
        return $this;
    }

    public function setPortales($portales) : self {
        $this->portales = $portales;
        return $this;
    }


    /**
     * Get usuario responsable de la inmobiliaria
     *
     * @return  Usuario
     */ 
    public function getUsuarioPrincipal() : ?Usuario
    {
        return $this->usuarioPrincipal;
    }

    /**
     * Set usuario responsable de la inmobiliaria
     *
     * @param  Usuario  $usuarioPrincipal  Usuario responsable de la inmobiliaria
     *
     * @return  self
     */ 
    public function setUsuarioPrincipal(?Usuario $usuarioPrincipal) : self
    {
        $this->usuarioPrincipal = $usuarioPrincipal;

        return $this;
    }

    /**
     * Get cms vinculado a la inmobiliaria
     *
     * @return  ?InterfaceCms
     */ 
    public function getCms() : ?InterfaceCms
    {
        return $this->Cms;
    }

    /**
     * Set cms vinculado a la inmobiliaria
     *
     * @param  ?InterfaceCms  $Cms  Cms vinculado a la inmobiliaria
     *
     * @return  self
     */ 
    public function setCms(?InterfaceCms $Cms) : self
    {
        $this->Cms = $Cms;

        return $this;
    }

    /**
     * Get oficina pricipal de la inmobiliaria
     *
     * @return  Oficina
     */ 
    public function getOficinaPrincipal() : ?Oficina
    {
        return $this->oficinaPrincipal;
    }

    /**
     * Set oficina pricipal de la inmobiliaria
     *
     * @param  Oficina  $oficinaPrincipal  Oficina pricipal de la inmobiliaria
     *
     * @return  self
     */ 
    public function setOficinaPrincipal(Oficina $oficinaPrincipal) : self
    {
        $this->oficinaPrincipal = $oficinaPrincipal;

        return $this;
    }
}
