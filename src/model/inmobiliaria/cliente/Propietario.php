<?php 
declare(strict_types = 1);
namespace inmotek\model\inmobiliaria\cliente;

/**
 * Modelos para propietario
 */
class Propietario extends ClienteBase{
    
    public function __construct($id, $nombre) {
        parent::__construct($id, $nombre);
    }

}
