<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble\media;

class Documento extends Media{
    public function factoryDocumento(?int $id, string $clave , string $name){
        $p = new self($id, $clave, $name);
        return $p;
    }    
}