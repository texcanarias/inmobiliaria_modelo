<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble\caracteristica;

class TipoEstructura  extends CaracteristicaInt  {
    static $ITEMS = [   "INDIFINIDO" => 0,
                        "HORMIGON" => 1,
                        "MADERA" => 2,
                        "MIXTA" => 3];  


    static $INDEFINIDO = 0;
    static $HORMIGON = 1;
    static $MADERA = 2;
    static $MIXTA = 3;  

    use TraitFactory;

    protected function isCorrecto(int $item)
    {
        return self::$INDEFINIDO <= $item AND self::$MIXTA >= $item;
    }

}