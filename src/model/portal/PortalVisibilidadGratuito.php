<?php 
declare(strict_types = 1);
namespace inmotek\model\portal;

class PortalVisibilidadGratuito extends PortalVisibilidad implements PortalVisibilidadInterface {

    static public function factory(?int $item) : self{
        try{        
            return new self(self::$MOSTRAR_DIRECCION_SOLO_CALLE);
        }
        catch(\Exception $ex){
            throw $ex;
        }
    }

}