<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble\media;

class Plano extends Media{
    public function factoryPlano(?int $id, string $clave , string $name){
        $p = new self($id, $clave, $name);
        return $p;
    }    
}

