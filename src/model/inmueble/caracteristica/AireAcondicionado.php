<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble\caracteristica;

class AireAcondicionado  extends CaracteristicaInt {
    
    static $ITEMS = ["NO" => 0,
                    "SI_SIN_DETALLAR" => 12,
                    "BOMBA_DE_CALOR" => 2,
                    "CLIMATIZACION" => 3,
                    "INSTALACION_COMPLETA" => 4,
                    "PREINSTALACION" => 5,
                    "SPLIT" => 6
    ];  

    static $NO = 0;
    static $SI_SIN_DETALLAR = 1;
    static $BOMBA_DE_CALOR = 2;
    static $CLIMATIZACION = 3;
    static $INSTALACION_COMPLETA = 4;
    static $PREINSTALACION = 5;
    static $SPLIT = 6;

    use TraitFactory;

    protected function isCorrecto(int $item)
    {
        return self::$NO <= $item AND self::$SPLIT >= $item;
    }
}