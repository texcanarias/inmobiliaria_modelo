<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble\caracteristica;

class Calefaccion   extends CaracteristicaInt {
    static $ITEMS = [   "NO" => 1,
                        "CENTRAL" => 2,
                        "INDIVIDUAL_GAS_NATURAL" => 3,
                        "INDIVIDUAL_ELECTRICA" => 4,
                        "INDIVIDUAL_GAS_BUTANO" => 5,
                        "INDIVIDUAL_CALOR_AZUL" => 6,
                        "INDIVIDUAL_SOLAR" => 7,
                        "INDIVIDUAL_GASOLEO" => 8,
                        "INDIVIDUAL_ACUMULA" => 9,
                        "INDIVIDUAL_SUELO" => 10,
                        "TUBOS" => 11,
                        "SI" => 12,
                        "BOMBA" => 13,
                        "CENTRAL_CONTADOR" => 14,
                        "TOMA" => 15,
                        "CHIMENEA" => 16,
                        "INDIVIDUAL_GAS_PROPANO" => 17,
                        "RESERVADO" => 18,
                        "PALLET" => 19];

    static $NO = 1;
    static $CENTRAL = 2;
    static $INDIVIDUAL_GAS_NATURAL = 3;
    static $INDIVIDUAL_ELECTRICA = 4;
    static $INDIVIDUAL_GAS_BUTANO = 5;
    static $INDIVIDUAL_CALOR_AZUL = 6;
    static $INDIVIDUAL_SOLAR = 7;
    static $INDIVIDUAL_GASOLEO = 8;
    static $INDIVIDUAL_ACUMULA = 9;
    static $INDIVIDUAL_SUELO = 10;
    static $TUBOS = 11;
    static $SI = 12;
    static $BOMBA = 13;
    static $CENTRAL_CONTADOR = 14;
    static $TOMA = 15;
    static $CHIMENEA = 16;
    static $INDIVIDUAL_GAS_PROPANO = 17;
    static $RESERVADO = 18;
    static $PALLET = 19;

    use TraitFactory;
    

    protected function isCorrecto(int $item)
    {
        return self::$NO <= $item AND self::$PALLET >= $item;
    }
    
}