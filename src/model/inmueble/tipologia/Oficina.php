<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\tipologia;

use \inmotek\model\inmueble\caracteristica\AireAcondicionado;
use \inmotek\model\inmueble\caracteristica\Calefaccion;
use \inmotek\model\inmueble\caracteristica\AguaCaliente;

class Oficina 
    implements InterfaceTipologia{

    public static $SUBTIPO_DEPARTAMENTO = 0;
    public static $SUBTIPO_DESPACHO = 1;
    public static $SUBTIPO_OFICINA = 2;

    use comun\TraitSubtipo;     
    use comun\TraitEstado;    
    use comun\TraitCes;
    use comun\TraitAnyoConstruccion;
    use comun\TraitSuperficieConstruida;
    use comun\TraitSuperficieUtil;
    use comun\TraitBanyo;
    use comun\TraitSeguridad;
    use comun\TraitEdificio;
    use comun\TraitFuego;
    use comun\TraitOrientacion;
    use comun\TraitDomotica;


    public function getTipologia() : self{
        return $this;
    }

    public function getTipo() : string{
        return "Oficina";
    }

    /**
     * Aire acondicionado
     * @var \inmotek\model\inmueble\caracteristica\AireAcondicionado
     */
    private ?AireAcondicionado $aireAcondicionado = null;

    /**
     * Calefaccion
     * @var \inmotek\model\inmueble\caracteristica\Calefaccion
     */
    private ?Calefaccion $calefaccion = null;

    /**
     * Agua caliente
     * inmotek\model\inmueble\caracteristica\AguaCaliente
     */
    private ?AguaCaliente $aguaCaliente = null;


        /**
     * Get aire acondicionado
     *
     * @return  \inmotek\model\inmueble\caracteristica\AireAcondicionado
     */ 
    public function getAireAcondicionado() : ?AireAcondicionado 
    {
        return $this->aireAcondicionado;
    }

    /**
     * Set aire acondicionado
     *
     * @param  \inmotek\model\inmueble\caracteristica\AireAcondicionado  $aireAcondicionado  Aire acondicionado
     *
     * @return  self
     */ 
    public function setAireAcondicionado(?AireAcondicionado $aireAcondicionado) : self
    {
        $this->aireAcondicionado = $aireAcondicionado;

        return $this;
    }

    /**
     * Get calefaccion
     *
     * @return  \inmotek\model\inmueble\caracteristica\Calefaccion
     */ 
    public function getCalefaccion() :?Calefaccion
    {
        return $this->calefaccion;
    }

    /**
     * Set calefaccion
     *
     * @param  \inmotek\model\inmueble\caracteristica\Calefaccion  $calefaccion  Calefaccion
     *
     * @return  self
     */ 
    public function setCalefaccion(?Calefaccion $calefaccion) : self
    {
        $this->calefaccion = $calefaccion;

        return $this;
    }

    /**
     * Get agua caliente
     */ 
    public function getAguaCaliente() : ?AguaCaliente
    {
        return $this->aguaCaliente;
    }

    /**
     * Set agua caliente
     *
     * @return  self
     */ 
    public function setAguaCaliente(?AguaCaliente $aguaCaliente) : self
    {
        $this->aguaCaliente = $aguaCaliente;

        return $this;
    }

}