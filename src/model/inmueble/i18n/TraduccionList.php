<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble\i18n;

class TraduccionList implements \Iterator, \ArrayAccess, \Countable {

    static public $ES = 'es';
    static public $EN = 'en';
    static public $EU = 'eu';
    static public $CA = 'ca';
    static public $FR = 'fr';
    static public $DE = 'de';
    static public $NL = 'nl';
    static public $RU = 'ru';
    static public $NO = 'no';
    static public $CH = 'ch';
    static public $GA = 'ga';

    static private $INDICES_ADMITIDOS = ['es', 'en', 'eu', 'ca', 'fr', 'de', 'nl', 'ru', 'no', 'ch','ga'];
    static private $ERROR_INDICE_NO_ADMITIDO = 'Índice no admitido. Idioma incorrecto';

    private $holder;

    public function __construct() {
        $this->holder = [];    
    }

    /**
     * implementacion de la interfaz iterator
     */
    public function rewind() 
    {
        reset($this->holder);
    }

    /**
     * implementacion de la interfaz iterator
     */    
    public function current() 
    {
        return current($this->holder);
    }

    /**
     * implementacion de la interfaz iterator
     */    
    public function key() 
    {
        return key($this->holder);
    }

    /**
     * implementacion de la interfaz iterator
     */    
    public function next() 
    {
        next($this->holder);
    }

    /**
     * implementacion de la interfaz iterator
     */    
    public function valid() 
    {
        return false !== $this->current();
    }
    
    public function offsetSet($offset, $value){
        if('inmotek\model\inmueble\i18n\Texto' != get_class($value)){
            throw new \Exception('Debe pasarse un objeto de tipo Texto');
        }

        if($this->offsetAdmitido($offset)){
            $this->holder[$offset] = $value;
        }
    }

    /**
     * Verifica si el índice que se va a meter es válido
     */
    public function offsetAdmitido($offset){
        return in_array($offset, self::$INDICES_ADMITIDOS);
    }

    /**
     * implementacion para la interfaz ArrayAccess
     * Comprueba si existe o no un indice
     * @param int $offset
     * @return bool
     */
    public function offsetExists($offset) 
    {
        if(!$this->offsetAdmitido($offset)){
            throw new \Exception(self::$ERROR_INDICE_NO_ADMITIDO. '[' . $offset . ']');
        }
        return (isset($this->holder[$offset]))?true:false;
    }

    /**
     * implementacion para la interfaz ArrayAccess
     * Destruye una posicion del offset
     * @param type $offset
     */
    public function offsetUnset($offset) 
    {
        if(!$this->offsetAdmitido($offset)){
            throw new \Exception(self::$ERROR_INDICE_NO_ADMITIDO. '[' . $offset . ']');
        }
        $this->holder[$offset] = null;
    }

    /**
     * implementacion para la interfaz ArrayAccess
     * Recupera una posicion del array
     * @param type $offset
     * @return type
     */
    public function offsetGet($offset) 
    {
        if(!$this->offsetAdmitido($offset)){
            throw new \Exception(self::$ERROR_INDICE_NO_ADMITIDO. '[' . $offset . ']');
        }
        return $this->holder[$offset];
    }
    
    /**
     * Implementación para la interfaz countable
     * @return int
     */
    public function count(){
        return count($this->holder);
    }    
}
