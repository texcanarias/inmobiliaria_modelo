```plantuml
@startuml

package inmotek <<Frame>> {
    package model <<Frame>> {
        package inmueble <<Frame>> {
            package trade <<Frame>> {
                class Alquiler
                class Trade
                class Traspaso
                class Venta
                interface InterfaceTrade
            }
        }
    }
}

interface InterfaceTrade{
    + getPrecio();
}

class Alquiler {
    - opcionCompra
}

class Trade {
    - precio  
    + getPrecio()
}

Trade ... InterfaceTrade
Trade <|-- Alquiler  

class Venta{
    - opcionCompra
}

Trade <|-- Venta

class Traspaso{
}

Venta <|--- Traspaso

@enduml
```