# Descripción de los modelos inmotek\model\base

```plantuml
@startuml
interface InterfazEntidad{
    + getId()
    + setId()
    + isNew()
}

class ModelEntidad{
    - id
    + construct()
    + getId()
    + setId()
    + isNew()
}

ModelEntidad --- InterfazEntidad

class DuplaIdNombre{
    - nombre
    + getName()
    + setName()
}

ModelEntidad <|-- DuplaIdNombre

class Gps{
    - latitud
    - longitud
    + getLatitude()
    + setLatitude(latitude)
    + getLongitude()
    + setLongitude(longitude)
}

class ArrayStorage{
    - holder
    - instanceName
    + rewind()
    + current()
    + key()
    + next()
    + valid()
    + offsetSet(offset, value)
    + offsetExists(offset)
    + offsetUnset(offset)
    + offsetGet(offset)
    + count()
}

class Label{
}

DuplaIdNombre <|-- Label

@enduml
```
