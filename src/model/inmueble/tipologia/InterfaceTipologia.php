<?php
declare(strict_types = 1);
namespace inmotek\model\inmueble\tipologia;

interface InterfaceTipologia{
    public function getTipologia();
    
    /**
     * Recupera el tipo del inmueble
     */
    public function getTipo() : string;
}