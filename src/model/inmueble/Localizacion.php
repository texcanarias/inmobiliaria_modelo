<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble;
use inmotek\model\base\DuplaIdNombre;

class Localizacion extends \inmotek\model\base\Direccion{   

    /**
     * Dirección
     * @var string
     */
    private string $calle;

    /**
     * Dirección - Portal
     * @var string
     */
    private string $portal;

    /**
     * Dirección - escalera
     * @var string
     */
    private string $escalera;

    /**
     * Dirección - piso
     * @var string
     */
    private string $piso;

    /**
     * Dirección - mano
     * @var string
     */
    private string $mano;


    protected function __construct()
    {
    }

    static public function factoryLocalizacion( string $clave, 
                                                string $pais, 
                                                int $idProvincia, 
                                                ?string $provincia, 
                                                int $idLocalidad, 
                                                ?string $localidad, 
                                                string $cp, 
                                                ?float $latitud, 
                                                ?float $longitud, 
                                                string $calle, 
                                                string $portal, 
                                                string $escalera, 
                                                string $piso, 
                                                string $mano)
    {
            if(null == $provincia){
                $provincia = "";
            }
            if(null == $localidad){
                $localidad = "";
            }

            $d = new Localizacion();
            if(null != $latitud && null != $longitud){
                $gps = \inmotek\model\base\Gps::factory($latitud, $longitud);
            }
            else{
                $gps = \inmotek\model\base\Gps::factory();
            }
            $d->setGps($gps);                
            $d->setProvincia(DuplaIdNombre::factory($idProvincia, $clave, $provincia));
            $d->setLocalidad(DuplaIdNombre::factory($idLocalidad, $clave, $localidad));
            //Para evitar datos precisos ocultamos esta informacion @todo
            /*
            $d->setCalle($calle);            
            $d->setPortal($portal);
            $d->setEscalera($escalera);
            $d->setPiso($piso);
            $d->setMano($mano);*/
            $d->setCalle("");            
            $d->setPortal("");
            $d->setEscalera("");
            $d->setPiso("");
            $d->setMano("");


            //Como el cp se verifica con el país hay que asegurarse que se instancia correctamente
            try {            
                $d->setPais($pais);            
                $d->setCp($cp);
            } catch (\Exception $ex) {
                $d->setPais('ES');
                $d->setCp('');    
            }
    
            return $d;
    }

    static public function factoryLocalizacionInmo(string $clave, ?string $pais, int $idProvincia, string $provincia, int $idLocalidad, string $localidad, string $cp, string $calle)
    {
        $d = new Localizacion();
        $d->setProvincia(DuplaIdNombre::factory($idProvincia, $clave, $provincia));
        $d->setLocalidad(DuplaIdNombre::factory($idLocalidad, $clave, $localidad));
        $d->setCalle($calle);

        //Como el cp se verifica con el país hay que asegurarse que se instancia correctamente
        if(null == $pais){
            $d->setPais('es');
        }
        try {            
            $d->setPais($pais);            
            $d->setCp($cp);
        } catch (\Exception $ex) {
            $d->setPais('es');
            $d->setCp('');
        }
        
        return $d;        
    }

    static public function factoryLocalizacionPromocion(string $clave, string $pais, int $idProvincia, string $provincia, int $idLocalidad, string $localidad,  float $latitud, float $longitud)
    {
        $d = new Localizacion();

        $gps = \inmotek\model\base\Gps::factory($latitud, $longitud);
        $d->setGps($gps);

        $d->setProvincia(DuplaIdNombre::factory($idProvincia, $clave, $provincia));
        $d->setLocalidad(DuplaIdNombre::factory($idLocalidad, $clave, $localidad));
        $d->setPais($pais);            
        
        return $d;        
    }


    /**
     * Get dirección
     *
     * @return  string
     */ 
    public function getCalle() : string
    {
        return $this->calle;
    }

    /**
     * Set dirección
     *
     * @param  string  $calle  Dirección
     *
     * @return  self
     */ 
    public function setCalle( $calle) : self
    {
        $this->calle = $calle;

        return $this;
    }

    /**
     * Get dirección - Portal
     *
     * @return  string
     */ 
    public function getPortal() : string
    {
        return $this->portal;
    }

    /**
     * Set dirección - Portal
     *
     * @param  string  $portal  Dirección - Portal
     *
     * @return  self
     */ 
    public function setPortal($portal) : self
    {
        $this->portal = $portal;

        return $this;
    }

    /**
     * Get dirección - escalera
     *
     * @return  string
     */ 
    public function getEscalera() : string
    {
        return $this->escalera;
    }

    /**
     * Set dirección - escalera
     *
     * @param  string  $escalera  Dirección - escalera
     *
     * @return  self
     */ 
    public function setEscalera($escalera) : self
    {
        $this->escalera = $escalera;

        return $this;
    }

    /**
     * Get dirección - piso
     *
     * @return  string
     */ 
    public function getPiso() : string
    {
        return $this->piso;
    }

    /**
     * Set dirección - piso
     *
     * @param  string  $piso  Dirección - piso
     *
     * @return  self
     */ 
    public function setPiso($piso) : self
    {
        $this->piso = $piso;

        return $this;
    }

    /**
     * Get dirección - mano
     *
     * @return  string
     */ 
    public function getMano() : string
    {
        return $this->mano;
    }

    /**
     * Set dirección - mano
     *
     * @param  string  $mano  Dirección - mano
     *
     * @return  self
     */ 
    public function setMano($mano) : self
    {
        $this->mano = $mano;

        return $this;
    }
}
