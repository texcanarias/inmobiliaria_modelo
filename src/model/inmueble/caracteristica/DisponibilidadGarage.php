<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble\caracteristica;

class DisponibilidadGarage  extends CaracteristicaInt{
    static $ITEMS = ["NO_TIENE" => 0,
                    "GARAJE_OPCIONAL" => 1,
                    "GARAJE_INCLUIDO" => 2
    ];

    static $NO_TIENE = 0;
    static $GARAJE_OPCIONAL = 1;
    static $GARAJE_INCLUIDO = 2;

    use TraitFactory;

    protected function isCorrecto(int $item)
    {
        return self::$NO_TIENE <= $item AND self::$GARAJE_INCLUIDO >= $item;
    }

}