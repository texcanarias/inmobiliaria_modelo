<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble\media;

class Imagen extends Media{
    static $array_txt_descfoto = array(
        "salon" => "Salón",
        "cocina" => "Cocina",
        "vestibulo" => "Vestibulo",
        "dormitorio1" => "Dormitorio 1",
        "dormitorio2" => "Dormitorio 2",
        "dormitorio3" => "Dormitorio 3",
        "dormitorio4" => "Dormitorio 4",
        "bano" => "Baño",
        "aseo" => "Aseo",
        "terraza" => "Terraza",
        "jardin" => "Jardín",
        "trastero" => "Trastero",
        "garaje" => "Garaje",
        "parcela" => "Parcela",
        "exterior" => "Exterior (fachada)",
        "calle" => "Exterior (calle)",
        "patio" => "Patio",
        "detalle" => "Detalle",
        "vistas" => "Vistas",
        "despacho" => "Despacho",
        "escaparate" => "Escaparate",
        "playa" => "Playa",
        "grua" => "Grúa",
        "oficinas" => "Oficinas",
        "cenital" => "Cenital",
        "iexterior" => "Exterior",
        "interior" => "Interior",
        "plano" => "Plano"
    );

    static $DES_SALON = 1;
    static $DES_COCINA = 2;
    static $DES_VESTIBULO = 3;
    static $DES_DORMITORIO_1 = 4;
    static $DES_DORMITORIO_2 = 5;
    static $DES_DORMITORIO_3 = 6;
    static $DES_DORMITORIO_4 = 7;
    static $DES_BANYO = 8;
    static $DES_ASEO = 9;
    static $DES_TERRAZA = 10;
    static $DES_JARDIN = 11;
    static $DES_TRASTERO = 12;
    static $DES_GARAJE = 13;
    static $DES_PARCELA = 14;
    static $DES_EXTERIOR_FACHADA = 15;
    static $DES_EXTERIOR_CALLE = 16;
    static $DES_PATIO = 17;
    static $DES_DETALLE = 18;
    static $DES_VISTAS = 19;
    static $DES_DESPACHO = 20;
    static $DES_ESCAPARATE = 21;
    static $DES_PLAYA = 22;
    static $DES_GRUA = 23;
    static $DES_OFICINAS = 24;
    static $DES_CENITAL = 25;
    static $DES_EXTERIOR = 26;
    static $DES_INTERIOR = 27;
    static $DES_PLANO = 28;
    static $DES_ALMACEN = 29;
    static $DES_ANEXO = 30;
    static $DES_ATICO = 31;
    static $DES_BARBACOA = 32;
    static $DES_BODEGA = 33;
    static $DES_CATASTRO = 34;
    static $DES_CES = 35;
    static $DES_CESLABEL = 36;
    static $DES_COMEDOR = 37;
    static $DES_COTAS = 38;
    static $DES_DESPENSA = 39;
    static $DES_DNI = 40;
    static $DES_ENCARGO = 41;
    static $DES_ESCRITURAS = 42;
    static $DES_ESPACIO = 43;
    static $DES_FORMAS_PAGO = 44;
    static $DES_FOTOS = 45;
    static $DES_GALERIA = 46;
    static $DES_GYM = 47;
    static $DES_HABITACION_SERVICIO = 48;
    static $DES_HALL = 49;
    static $DES_JACUZZI = 50;
    static $DES_JUNTAS= 51;
    static $DES_MIRADOR = 52;
    static $DES_OTRO = 53;
    static $DES_OTRO_PUBLICO = 54;
    static $DES_PASILLO = 55;
    static $DES_PISCINA = 56;
    static $DES_PLANO_PRIVADO = 57;
    static $DES_PORCHE = 58;
    static $DES_RECEPCION = 59;
    static $DES_SALA_CINE = 60;
    static $DES_SAUNA = 61;
    static $DES_SPA = 62;
    static $DES_VALORACION = 63;
    static $DES_VESTIDOR = 64;
    static $DES_ZONAS_COMUNES = 65;




    /**
     * Ancho de la imagen
     * @var int
     */
    private int $ancho = 0;

    /**
     * Alto de la imagen
     * @var int
     */
    private int $alto = 0;
 
    /**
     * Clasificacion de la fotografía
     * @var int
     */
    private ?int $clasificacion = null;

    public function factoryImagen(?int $id, string $clave , string $name){
        $p = new self($id, $clave, $name);
        return $p;
    }    

    /**
     * Get ancho de la imagen
     *
     * @return  int
     */ 
    public function getAncho() : int
    {
        return $this->ancho;
    }

    /**
     * Set ancho de la imagen
     *
     * @param  int  $ancho  Ancho de la imagen
     *
     * @return  self
     */ 
    public function setAncho(int $ancho)
    {
        if(0 > $ancho){
            throw new \Exception("El ancho debe ser mayor que 0");
        }
        $this->ancho = $ancho;

        return $this;
    }

    /**
     * Get alto de la imagen
     *
     * @return  int
     */ 
    public function getAlto(): int
    {
        return $this->alto;
    }

    /**
     * Set alto de la imagen
     *
     * @param  int  $alto  Alto de la imagen
     *
     * @return  self
     */ 
    public function setAlto(int $alto) 
    {
        if(0 > $alto){
            throw new \Exception("El alto debe ser mayor que 0");
        }
        $this->alto = $alto;

        return $this;
    }

    /**
     * Get clasificacion de la fotografía
     *
     * @return  int
     */ 
    public function getClasificacion() : ?int
    {
        return $this->clasificacion;
    }

    /**
     * Set clasificacion de la fotografía
     *
     * @param  int  $clasificacion  Clasificacion de la fotografía
     *
     * @return  self
     */ 
    public function setClasificacion(?int $clasificacion)
    {
        if (null != $clasificacion){
            $dentroRango = 1 <= $clasificacion && 28 >= $clasificacion;
            if(!$dentroRango){
                throw new \Exception("El valor pasado no es correcto");
            }
        }
        $this->clasificacion = $clasificacion;

        return $this;
    }
}
