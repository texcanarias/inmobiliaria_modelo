<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble\tipologia\comun;

use \inmotek\model\inmueble\caracteristica\TipoEstructura;
use \inmotek\model\inmueble\caracteristica\TipoFachada;
use \inmotek\model\inmueble\caracteristica\Exterior;

trait TraitEdificio
{
    use TraitAscensor;

    /**
     * Tipo de estructrura
     * @var \inmotek\model\inmueble\caracteristica\TipoEstructura
     */
    private ?TipoEstructura $tipoEstructura = null;

    /**
     * Tipo de fachada
     * @var \inmotek\model\inmueble\caracteristica\TipoFachada
     */
    private ?TipoFachada $tipoFachada = null;

    /**
     * Altura de la planta donde está la vivienda y oficina
     * @var int
     */
    private ?int $alturaReal = null;

    /**
     * Altura total de la vivienda
     * @var int
     */
    private ?int $alturaTotal = null;

    /**
     * Exterior o interior
     * @var \inmotek\model\inmueble\caracteristica\Exterior
     */
    private ?Exterior $exterior = null;


    /**
     * Número de pisos en la planta
     * @var int
     */
    private ?int $pisosEnPlanta = null;

    /**
     * Get tipo de estructrura
     *
     * @return  \inmotek\model\inmueble\caracteristica\TipoEstructura
     */ 
    public function getTipoEstructura() : ?TipoEstructura
    {
        return $this->tipoEstructura;
    }

    /**
     * Set tipo de estructrura
     *
     * @param  \inmotek\model\inmueble\caracteristica\TipoEstructura  $tipoEstructura  Tipo de estructrura
     *
     * @return  self
     */ 
    public function setTipoEstructura(?TipoEstructura $tipoEstructura) : self
    {
        $this->tipoEstructura = $tipoEstructura;

        return $this;
    }

    /**
     * Get tipo de fachada
     *
     * @return  \inmotek\model\inmueble\caracteristica\TipoFachada
     */ 
    public function getTipoFachada() : ?TipoFachada
    {
        return $this->tipoFachada;
    }

    /**
     * Set tipo de fachada
     *
     * @param  \inmotek\model\inmueble\caracteristica\TipoFachada  $tipoFachada  Tipo de fachada
     *
     * @return  self
     */ 
    public function setTipoFachada(?TipoFachada $tipoFachada) : self
    {
        $this->tipoFachada = $tipoFachada;

        return $this;
    }

    /**
     * Get altura de la planta donde está la vivienda y oficina
     *
     * @return  int
     */ 
    public function getAlturaReal() : ?int
    {
        return $this->alturaReal;
    }

    /**
     * Set altura de la planta donde está la vivienda y oficina
     *
     * @param  int  $alturaReal  Altura de la planta donde está la vivienda y oficina
     *
     * @return  self
     */ 
    public function setAlturaReal(?int $alturaReal) : self
    {
        $this->alturaReal = $alturaReal;

        return $this;
    }

    /**
     * Get altura total de la vivienda
     *
     * @return  int
     */ 
    public function getAlturaTotal() : ?int
    {
        return $this->alturaTotal;
    }

    /**
     * Set altura total de la vivienda
     *
     * @param  int  $alturaTotal  Altura total de la vivienda
     *
     * @return  self
     */ 
    public function setAlturaTotal(?int $alturaTotal)
    {
        $this->alturaTotal = $alturaTotal;

        return $this;
    }

    /**
     * Get exterior o interior
     *
     * @return  \inmotek\model\inmueble\caracteristica\Exterior
     */ 
    public function getExterior() : ?Exterior
    {
        return $this->exterior;
    }

    /**
     * Set exterior o interior
     *
     * @param  \inmotek\model\inmueble\caracteristica\Exterior  $exterior  Exterior o interior
     *
     * @return  self
     */ 
    public function setExterior(?Exterior $exterior) : self
    {
        $this->exterior = $exterior;

        return $this;
    }

    /**
     * Get número de pisos en la planta
     *
     * @return  int
     */ 
    public function getPisosEnPlanta() : ?int
    {
        return $this->pisosEnPlanta;
    }

    /**
     * Set número de pisos en la planta
     *
     * @param  int  $pisosEnPlanta  Número de pisos en la planta
     *
     * @return  self
     */ 
    public function setPisosEnPlanta(?int $pisosEnPlanta) : self
    {
        $this->pisosEnPlanta = $pisosEnPlanta;

        return $this;
    }
}
