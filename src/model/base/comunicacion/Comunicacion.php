<?php 
declare(strict_types = 1);
namespace inmotek\model\base\comunicacion;

class Comunicacion {

    static public $TELEFONO = 0; 
    static public $MOVIL = 1; 
    static public $FAX = 2;
    static public $EMAIL = 3;
    static public $WEB = 4;
    static public $WHATSAPP = 5;
    static public $TELEGRAM = 6;
    static public $TWITTER = 7;
    static public $FACEBOOK = 8;

    /**
     * Dato de comunicacion que se registra
     */
    private string $dato;

    /**
     * Tipo de dato según los statics
     */
    private int $tipo;

    /**
     * Orden de prioridad
     */
    private ?int $orden = null;

    private function __construct(string $dato, int $tipo, ?int $orden = null) {
        $this->dato = $dato;
        $this->tipo = $tipo;
        $this->orden = $orden;
    }

    public static function factory(string $dato, int $tipo, ?int $orden = null) : self{
        $c = new self($dato, $tipo, $orden);
        return $c;
    }


    /**
     * Get dato de comunicacion que se registra
     */ 
    public function getDato() : string
    {
        return $this->dato;
    }

    /**
     * Get tipo de dato según los statics
     */ 
    public function getTipo() : int
    {
        return $this->tipo;
    }

    /**
     * Get Orden de prioridad
     */ 
    public function getOrden() : int
    {
        return $this->orden;
    }
}
