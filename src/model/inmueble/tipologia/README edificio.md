```plantuml
@startuml

package inmotek <<Frame>> {
    package model <<Frame>> {
        package inmueble <<Frame>> {
            package tipologia <<Frame>> {
                interface InterfaceTipologia
                class Edificio
                package comun <<Frame>>{
                    class TraitAccesibilidad <<trait>>
                    class TraitAnyoConstruccion <<trait>>
                    class TraitAscensor <<trait>>
                    class TraitEmergencia <<trait>>
                    class TraitEstado <<trait>>
                    class TraitCes <<trait>>
                    class TraitSubtipo <<trait>>
                    class TraitPlantasConstruidas <<trait>>
                    class TraitSeguridad <<trait>>
                    class TraitSuperficieAjardinada <<trait>>
                    class TraitSuperficieUtil <<trait>>
                    class TraitSuperficieConstruida  <<trait>>
                }
            }
            package ces <<Frame>>{
                class Ces
            }
        }
    }
}

InterfaceTipologia <|-- Edificio


Edificio -- TraitCes
Edificio -- TraitSubtipo
Edificio -- TraitEstado  
Edificio -- TraitAnyoConstruccion
Edificio -- TraitSuperficieUtil
Edificio -- TraitSuperficieAjardinada
Edificio -- TraitPlantasConstruidas
Edificio -- TraitAscensor
Edificio -- TraitSeguridad  
Edificio -- TraitEmergencia

class Edificio{
    - anyoConstruccion
    - estado
    - superficie
}


class TraitEstado
{
    + {static} REFORMA_INTEGRAL : 1
    + {static} REFORMA_NECESARIA : 2
    + {static} BUEN_ESTADO : 3
    + {static} MUY_BUEN_ESTADO : 4
    + {static} RECIEN_REFORMADO : 5
    + {static} NUEVO_O_SEMINUEVO : 6

    - estado
}

class TraitAnyoConstruccion
{
    - anyoConstruccion : int
}

class TraitAscensor {
    - ascensorNumero : int
    - ascensorCotaCero : int
}

class TraitEmergencia{
    - emergenciaSalida
    - emergenciaLuces
}

TraitCes -- Ces

class TraitCes{
    - ces : Ces
}

class TraitSubtipo{
    - subtipo: int
}

class TraitPlantasConstruidas {
    - plantasConstruidas
}

class TraitSeguridad {
    - seguridadSistema
    - seguridadAlarma
    - seguridadPuerta
    - seguridad24h
    - seguridadPersonal
}

class TraitSuperficieAjardinada {
    - superficieAjardinada
}

class TraitSuperficieConstruida
{
    - superficieConstruida
}

class TraitSuperficieUtil {
    - superficieUtil
}




@enduml
```