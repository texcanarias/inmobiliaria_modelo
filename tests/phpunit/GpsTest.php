<?php namespace inmotek_test\phpunit;

use PHPUnit\Framework\TestCase;

final class gpsTest extends TestCase {

    public function testGps() {
        $gps = \inmotek\model\base\Gps::factory(20.00, 20.01);

        $this->assertTrue( 20.00 == $gps->getLatitude() );
        $this->assertTrue( 20.01 == $gps->getLongitude() );

    }

}
