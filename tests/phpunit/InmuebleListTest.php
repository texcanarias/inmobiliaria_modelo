<?php namespace inmotek_test\phpunit;

use PHPUnit\Framework\TestCase;
use inmotek\model\inmobiliaria\Inmobiliaria;

final class inmuebleListTest extends TestCase {

    private function getInmu(){
        $inmo = new Inmobiliaria(1, "PRO", "Pruebas y text,S.A.");
        $inmo->setLetra('PR');
        $inmo->setToken('zztop123');
        $inmo->setDemo(false);
        $inmo->setSistema(true);        
        $inmueble = \inmotek\model\inmueble\Inmueble::factoryInmueble(1,  "Casa palaciega",$inmo);
        return $inmueble;
    }

    public function testInmueble() {
        $inmo = new Inmobiliaria(1, "PRO", "Pruebas y text,S.A.");
        $inmo->setLetra('PR');
        $inmo->setToken('zztop123');
        $inmo->setDemo(false);
        $inmo->setSistema(true);        

        $inmueblesColeccion = new \inmotek\model\inmueble\InmuebleList();

        $inmueblesColeccion[] = \inmotek\model\inmueble\Inmueble::factoryInmueble(1,  "Perro",$inmo);
        $inmueblesColeccion[] = \inmotek\model\inmueble\Inmueble::factoryInmueble(2,  "Gato",$inmo);
        $inmueblesColeccion[] = \inmotek\model\inmueble\Inmueble::factoryInmueble(3,  "Gallina",$inmo);
        $inmueblesColeccion[] = \inmotek\model\inmueble\Inmueble::factoryInmueble(4,  "Pez",$inmo);
        $inmueblesColeccion[] = \inmotek\model\inmueble\Inmueble::factoryInmueble(5,  "Lombriz",$inmo);

        $this->assertTrue( 1 == $inmueblesColeccion[0]->getId() );
        $this->assertTrue( 2 == $inmueblesColeccion[1]->getId() );
        $this->assertTrue( 3 == $inmueblesColeccion[2]->getId() );
        $this->assertTrue( 4 == $inmueblesColeccion[3]->getId() );
        $this->assertTrue( 5 == $inmueblesColeccion[4]->getId() );
        $this->assertTrue( 5 == count($inmueblesColeccion));
    }
}
