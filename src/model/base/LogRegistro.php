<?php 
declare(strict_types = 1);
namespace inmotek\model\base;

/**
 * Modelo base para sostener la persistencia
 */
class LogRegistro{
    /**
     * Fecha de creación del registro del inmueble. 
     * @var \DateTime
     */
    private ?\DateTime $creationDate;

    /**
     * Última fecha en la que se ha modificado el registro del inmueble.
     * @var \DateTime
     */
    private ?\DateTime $modifiedDate; 


    /**
     * Fecha de borrado
     * @var \DateTime
     */
    private ?\DateTime $deleteDate;

    private function __construct(\DateTime $creationDate = null, \DateTime $modifiedDate = null, \DateTime $deleteDate = null){
        $this->creationDate = $creationDate;
        $this->modifiedDate = $modifiedDate;
        $this->deleteDate = $deleteDate;
    }

    public static function factory(\DateTime $creationDate = null, \DateTime $modifiedDate = null, \DateTime $deleteDate = null) : self{
        $n = new self($creationDate , $modifiedDate , $deleteDate);
        return $n;
    }

    /**
     * Get fecha de creación del registro del inmueble.
     *
     * @return  \DateTime
     */ 
    public function getCreationDate() :  \DateTime
    {
        return $this->creationDate;
    }

    

    /**
     * Get última fecha en la que se ha modificado el registro del inmueble.
     *
     * @return  \DateTime
     */ 
    public function getModifiedDate() :  \DateTime
    {
        return $this->modifiedDate;
    }

    

    /**
     * Get fecha de borrado
     *
     * @return  \DateTime
     */ 
    public function getDeleteDate() :  \DateTime
    {
        return $this->deleteDate;
    }
}