<?php 
declare(strict_types = 1);
namespace inmotek\model\portal;

class PortalVisibilidad implements PortalVisibilidadInterface{
    static $MOSTRAR_DIRECCION_NO = 0; //f en inmotek
    static $MOSTRAR_DIRECCION_SOLO_CALLE = 1;  //s en inmotek  
    static $MOSTRAR_DIRECCION_COMPLETA = 2; //t en inmotek

    /**
     * Valor definido
     * @var int
     */
    private ?int $item = null;

    protected function __construct(?int $item)
    {
        try{
            $this->setItem($item);
        }
        catch(\Exception $ex){
            throw $ex;
        }
    }

    static public function factory(?int $item) : self{
        try{        
            return new self($item);
        }
        catch(\Exception $ex){
            throw $ex;
        }
    }

    public function __invoke() : ?int
    {
        return $this->getItem();
    }

    /**
     * Get int
     * @return ?int
     */ 
    protected function getItem() : ?int
    {
        return $this->item;
    }


    /**
     * Set int
     * @param int $item
     */ 
    protected function setItem(?int $item) : self
    {
        if(null != $item AND !$this->isCorrecto($item)){
            throw new \Exception("Valor incorrecto");
        }

        $this->item = $item;
        return $this;
    }    

    protected function isCorrecto(int $item)
    {
        return self::$MOSTRAR_DIRECCION_NO <= $item AND self::$MOSTRAR_DIRECCION_COMPLETA >= $item;
    }

}
