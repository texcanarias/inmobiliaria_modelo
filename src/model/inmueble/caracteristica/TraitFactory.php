<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble\caracteristica;

/**
 * Definicion genaral de un método de factoria
 */
trait TraitFactory {
    static public function factory(?int $item) : self{
        try{        
            return new self($item);
        }
        catch(\Exception $ex){
            throw $ex;
        }
    }
}