<?php namespace inmotek_test\phpunit;

use inmotek\model\base\LogRegistro;
use PHPUnit\Framework\TestCase;
use inmotek\model\inmobiliaria\Inmobiliaria;
use inmotek\model\promocion\Promocion;
use inmotek\model\inmueble\i18n\Texto;
use inmotek\model\promocion\i18n\TextoList;
use inmotek\model\inmueble\i18n\TraduccionList;


final class promocionTest extends TestCase {

    private function getLoca(){
        return \inmotek\model\inmueble\Localizacion::factoryLocalizacion('test','ES', 35, 'Las Palmas', 35012, 'LPGC', '35012', 20.00, 20.01, 'Don Pío Coronado', '172', 'Unica', '2º', 'A');
    }

    private function getUsuario(){
        return new \inmotek\model\inmobiliaria\oficina\usuario\Usuario(1, "Roberto Fernandez");   
    }

    private function getUsuarios(){
        $usuarios = new \inmotek\model\inmobiliaria\oficina\usuario\UsuarioList();
        $usuarios[] = $this->getUsuario();
        return $usuarios;
    }

    private function getInmo(){
        $inmo = new Inmobiliaria(1, "PRO", "Pruebas y text,S.A.");
        $inmo->setLetra('PR');
        $inmo->setToken('zztop123');
        $inmo->setDemo(false);
        $inmo->setSistema(true);        

        $direccion = $this->getLoca();
        $inmo->setDireccion($direccion);

        $usuarios = $this->getUsuarios();
        $usuarios[0]->setDireccion($direccion);

        $oficinas = new \inmotek\model\inmobiliaria\oficina\OficinaList();
        $oficinas[] = new \inmotek\model\inmobiliaria\oficina\Oficina(1,"Central");
        $oficinas[0]->setDireccion($direccion);
        $oficinas[0]->setPrincipal(true);
        $oficinas[0]->setOrden(0);
        $oficinas[0]->setUsuarios($usuarios);
        $inmo->setOficinas($oficinas);

        return $inmo;
    }

    private function getInmu(){
        $inmueble = \inmotek\model\inmueble\Inmueble::factoryInmueble(1,  "Casa palaciega", $this->getInmo());
        return $inmueble;
    }

    public function testPromocion() {
        $promo = Promocion::factoryPromocion(null, "Promocion rio grande", $this->getInmo());

        $promo->setLocalizacion($this->getLoca());

        $fecha = new \DateTime();
        $promo->setLogRegistro(LogRegistro::factory( $fecha, $fecha, $fecha));

        $promo->setCaptador($this->getUsuario());
        $promo->setGestionador($this->getUsuarios());

        $inmuebles = new \inmotek\model\inmueble\InmuebleList();
        $inmuebles[] = \inmotek\model\inmueble\Inmueble::factoryInmueble(1,  "Casa palaciega 0", $this->getInmo());
        $inmuebles[] = \inmotek\model\inmueble\Inmueble::factoryInmueble(2,  "Casa palaciega 1", $this->getInmo());
        $inmuebles[] = \inmotek\model\inmueble\Inmueble::factoryInmueble(3,  "Casa palaciega 2", $this->getInmo());
        $promo->setInmuebles($inmuebles);

        $oficina = new \inmotek\model\inmobiliaria\oficina\Oficina(1,"Central");
        $promo->setOficina($oficina);

        $es = "Esto es un texto";
        $en = "This's a text";
        $ca = "Això és un text";
        $eu = "Hau testu bat da";
        $fr = "Ceci est un texte";

        $traduccion = new TraduccionList();
        $traduccion[TraduccionList::$ES] = Texto::factoryTexto(1, 'test', $es);
        $traduccion[TraduccionList::$EN] = Texto::factoryTexto(2, 'test', $en);
        $traduccion[TraduccionList::$CA] = Texto::factoryTexto(3, 'test', $ca);
        $traduccion[TraduccionList::$EU] = Texto::factoryTexto(4, 'test', $eu);
        $traduccion[TraduccionList::$FR] = Texto::factoryTexto(5, 'test', $fr);

        $coleccion = new TextoList();
        $coleccion[TextoList::$COMO_LLEGAR_TEXTO_A] = $traduccion;

        $promo->setTextos($coleccion);

        $this->assertTrue( null == $promo->getId() );
        $this->assertTrue(3 == count($promo->getInmuebles()));
        $this->assertTrue('Esto es un texto' == $promo->getTextos()[TextoList::$COMO_LLEGAR_TEXTO_A][TraduccionList::$ES]->getName());
    }
}
