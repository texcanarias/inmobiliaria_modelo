<?php 
declare(strict_types = 1);
namespace inmotek\model\inmobiliaria\cliente;

/**
 * Modelos para clientes, que podrían ser tanto clientes finales como propietarios
 * @todo Arreglar todo el tema de la direccion
 */
class ClienteBase extends \inmotek\model\base\DuplaIdNombre{
    
    /**
     * Idioma de preferencia del cliente con el código ISO 639-1
     * @var string
     *
     */    
    private $language;
       
    
    /**
     * Fecha de nacimiento Formato ISO 8601
     * @var string
     *
     */        
    private $date;
    
    /**
     * Apellido
     * @var string
     *
     */            
    private $surnames;
    
    /**
     * Compañia a la que pertenece el cliente
     * @var string
     *
     */            
    private $company;   
    
    /**
     * Estado
     * @var boolean
     *
     */
    private $state;
    
    /**
     * Si se trata de un profesional
     * @var boolean
     *
     */                
    private $professional;
    
    /**
     * Si tiene un calendario
     * @var boolean
     *
     */                    
    private $schedule;
    
    /**
     * Correo electrónico 
     * @var string
     *
     */                        
    private $email;
    
    /**
     * NIF de cliente
     * @var string
     *
     */                            
    private $nif;
       
    
    private $telefono_trabajo;
    private $telefono_movil;
    private $telefono_casa;
    private $fax;

    /**
     * Comentarios / observaciones
     * @var string
     *
     */                                                        
    private $coment;
    
    /**
     * Direccion
     * @var string
     *
     */
    private $address;
    
    /**
     * Dirección web
     * @var string
     */
    private $web;
    
    /**
     * Identificador del usuario
     * @var id
     */
    private $agente_captador;
    
    public function __construct($id, $nombre) {
        parent::__construct($id, $nombre);
    }

}
