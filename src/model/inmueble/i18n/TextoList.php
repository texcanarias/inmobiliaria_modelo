<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble\i18n;

class TextoList implements \Iterator, \ArrayAccess, \Countable {

    static public $SITUACION = 0; //situacion
    static public $SALON = 1;
    static public $HABITACIONES = 2;
    static public $HALL = 3;
    static public $COCINA = 4;
    static public $BANYOS = 5;
    static public $ASCENSORES = 6;
    static public $ANTIGUEDAD = 7;
    static public $GARAJE = 8;
    static public $GASTOS = 9;
    static public $TENDEDERO = 10;
    static public $ORIENTACION = 11;
    static public $AMUEBLADO = 12;
    static public $TERRAZA = 13;
    static public $TRASTERO = 14;
    static public $LUZ = 15;
    static public $CONSERVACION = 16;
    static public $TITULO = 17; //titulo
    static public $NOTAS_PUBLICAS = 18;
    static public $NOTAS_ENVIO = 19;
    static public $META_DESCRIPCION = 20;
    static public $PLANTAS = 21;
    static public $DISTANCIAS = 22;
    static public $COLEGIOS = 23;
    static public $PLAZAS = 24;
    static public $ESCAPARATE = 25;
    static public $NEGOCIO = 26;
    static public $CATEGORIA = 27;

    static private $ERROR_INDICE_NO_ADMITIDO = 'Índice no admitido';

    private $holder;

    public function __construct() {
        $this->holder = [];    
    }

    /**
     * implementacion de la interfaz iterator
     */
    public function rewind() 
    {
        reset($this->holder);
    }

    /**
     * implementacion de la interfaz iterator
     */    
    public function current() 
    {
        return current($this->holder);
    }

    /**
     * implementacion de la interfaz iterator
     */    
    public function key() 
    {
        return key($this->holder);
    }

    /**
     * implementacion de la interfaz iterator
     */    
    public function next() 
    {
        next($this->holder);
    }

    /**
     * implementacion de la interfaz iterator
     */    
    public function valid() 
    {
        return false !== $this->current();
    }
    
    public function offsetSet($offset, $value){
        if('inmotek\model\inmueble\i18n\TraduccionList' != get_class($value)){
            throw new \Exception('Debe pasarse un objeto de tipo TraduccionList');
        }

        if(!$this->offsetAdmitido($offset)){
            throw new \Exception(self::$ERROR_INDICE_NO_ADMITIDO);
        }
        $this->holder[$offset] = $value;
    }

    /**
     * Verifica si el índice que se va a meter es válido
     */
    public function offsetAdmitido($offset){
        return self::$SITUACION <= $offset OR self::$CATEGORIA >= $offset;
    }

    /**
     * implementacion para la interfaz ArrayAccess
     * Comprueba si existe o no un indice
     * @param int $offset
     * @return bool
     */
    public function offsetExists($offset) 
    {
        if(!$this->offsetAdmitido($offset)){
            throw new \Exception(self::$ERROR_INDICE_NO_ADMITIDO);
        }
        return (isset($this->holder[$offset]))?true:false;
    }

    /**
     * implementacion para la interfaz ArrayAccess
     * Destruye una posicion del offset
     * @param type $offset
     */
    public function offsetUnset($offset) 
    {
        if(!$this->offsetAdmitido($offset)){
            throw new \Exception(self::$ERROR_INDICE_NO_ADMITIDO);
        }
        $this->holder[$offset] = null;
    }

    /**
     * implementacion para la interfaz ArrayAccess
     * Recupera una posicion del array
     * @param type $offset
     * @return type
     */
    public function offsetGet($offset) 
    {
        if(!$this->offsetAdmitido($offset)){
            throw new \Exception(self::$ERROR_INDICE_NO_ADMITIDO);
        }
        return $this->holder[$offset];
    }
    
    /**
     * Implementación para la interfaz countable
     * @return int
     */
    public function count(){
        return count($this->holder);
    }    
}
