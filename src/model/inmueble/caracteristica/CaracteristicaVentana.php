<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble\caracteristica;

class CaracteristicaVentana  extends Caracteristica {
    static $TIPO_ALUMINIO = 1;
    static $TIPO_ALUMINIO_CLIMALIT = 2;
    static $TIPO_PVC_CLIMALIT = 3;
    static $TIPO_MADERA = 4;
    static $TIPO_MADERA_CLIMATIC = 5;
    static $TIPO_HIERRO = 6;

    static $ESTADO_NUEVAS = 1;
    static $ESTADO_BUEN_ESTADO = 2;
    static $ESTADO_REPARAR = 3;
    static $ESTADO_MUY_DETERIORADAS = 4;
}