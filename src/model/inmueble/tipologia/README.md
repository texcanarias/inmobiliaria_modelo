```plantuml
@startuml

package inmotek <<Frame>> {
    package model <<Frame>> {
        package inmueble <<Frame>> {
            package tipologia <<Frame>> {
                interface InterfaceTipologia
                class Casa
                class Edificio
                class Garaje
                class Habitacion
                class Local
                class Nave
                class Oficina
                class Piso
                class Terreno
                class Trastero
                package comun <<Frame>>{
                    class TraitAccesibilidad <<trait>>
                    class TraitAnyoConstruccion <<trait>>
                    class TraitAscensor <<trait>>
                    class TraitBanyo <<trait>>
                    class TraitEmergencia <<trait>>
                    class TraitEstado <<trait>>
                    class TraitCes <<trait>>
                    class TraitSubtipo <<trait>>
                    class TraitOrientacion <<trait>>
                    class TraitPlantasConstruidas <<trait>>
                    class TraitSeguridad <<trait>>
                    class TraitSuperficieNegociable <<trait>>
                    class TraitSuperficieAjardinada <<trait>>
                    class TraitSuperficieConstruida <<trait>>
                    class TraitSuperficieUtil <<trait>>
                    class TraitVivienda <<trait>>
                }
            }
            package caracteristica <<Frame>>{
                class AdmiteEstudiantes
                class AguaCaliente
                class AireAcondicionado
                class Aislamiento
                class Amueblado
                class Calefaccion
                class CalificacionUrbanistica
                class Caracteristica
                class CaracteristicaPared
                class CaracteristicaPuerta
                class CaracteristicaSuelo
                class CaracteristicaVentana
                class DisponibilidadTratero
                class Exterior
                class SegundaMano
                class TipoEstructura
                class TipoFachada
                class TipoTerreno
            }
            package ces <<Frame>>{
                class Ces
            }
        }
    }
}

InterfaceTipologia <|-- Edificio
InterfaceTipologia <|-- Casa
InterfaceTipologia <|-- Garaje
InterfaceTipologia <|-- Habitacion
InterfaceTipologia <|-- Local
InterfaceTipologia <|-- Nave
InterfaceTipologia <|-- Oficina
InterfaceTipologia <|-- Piso
InterfaceTipologia <|-- Terreno
InterfaceTipologia <|-- Trastero



TraitVivienda *-- Amueblado
TraitVivienda *-- AireAcondicionado
TraitVivienda *-- Calefaccion
TraitVivienda *-- AguaCaliente
TraitVivienda *-- Aislamiento
TraitVivienda *-- DisponibilidadGarage
TraitVivienda *-- DisponibilidadTrastero

class TraitVivienda{
    - balcon : bool
    - terraza: bool
    - armarioEmpotrado: bool
    - numeroHabitaciones: bool
    - segundaMano: bool
    - amueblado : Amueblado
    - aireAcondicionado: AireAcondicionado
    - calefaccion: Calefaccion
    - aguaCaliente: AguaCaliente
    - aislamiento: Aislamiento
    - chimenea: bool
    - cocinaEquipada: bool
    - jardin: bool
    - piscina: bool
    - disponibilidadGarage: DisponibilidadGarage
    - animalesPermitidos: bool
    - diponibilidadTrastero: DisponibilidadTrastero
}


Casa <|- Edificio
Casa -- TraitOrientacion
Casa -- TraitBanyo
Casa -- TraitAccesibilidad
Casa -- TraitVivienda

class Casa{

}

Piso -- TraitSubtipo
Piso -- TraitCes
Piso -- TraitEstado
Piso -- TraitAnyoConstruccion
Piso -- TraitSuperficieConstruida
Piso -- TraitSuperficieUtil
Piso -- TraitVivienda

class Piso{
    - exterior
}

  Edificio -- TraitCes
  Edificio -- TraitSubtipo
  Edificio -- TraitEstado  
  Edificio -- TraitAnyoConstruccion
  Edificio -- TraitSuperficieUtil
  Edificio -- TraitSuperficieAjardinada
  Edificio -- TraitPlantasConstruidas
  Edificio -- TraitAscensor
  Edificio -- TraitSeguridad  
  Edificio -- TraitEmergencia

class Edificio{
    - anyoConstruccion
    - estado
    - superficie
}

Garaje -- TraitSubtipo
Garaje -- TraitEstado
Garaje -- TraitSuperficieConstruida
Garaje -- TraitAscensor
Garaje -- TraitSeguridad

class Garaje{
    - puertaAutomatica
    - plazaCubierta
}

Habitacion -- TraitSubtipo
Habitacion -- TraitSuperficieConstruida
Habitacion -- TraitSuperficieUtil
Habitacion *-- AdmiteEstudiantes
class Habitacion{
    admiteEstudiantes : AdmiteEstudiantes
}


Local -- TraitSubtipo
Local -- TraitEstado
Local -- TraitCes
Local -- TraitSuperficieConstruida
Local -- TraitSuperficieUtil
Local -- TraitSuperficieAjardinada
Local -- TraitPlantasConstruidas
Local -- TraitBanyo
Local -- TraitSeguridad
Local *-- AireAcondicionado
Local *-- Calefaccion

class Local{
    - aireAcondicionado : AireAcondicionado
    - calefaccion : Calefaccion
    - cocinaEquipada : bool
    - areaFachada : int
    - ultimaActividad : string
    - huecos : int
    - almacen : bool
    - extractorHumos : bool
    - ubicacion : bool
    - numeroVentanas: int
}


Nave -- TraitSubtipo
Nave -- TraitEstado
Nave -- TraitSuperficieConstruida
Nave -- TraitSuperficieUtil
Nave -- TraitSuperficieAjardinada
Nave -- TraitBanyo
Nave -- TraitPlantasConstruidas
Nave *-- AireAcondicionado
Nave *-- Calefaccion
class Nave{
    - aireAcondicionado : AireAcondicionado
    - calefaccion: Calefaccion
    - cocinaEquipada
}

Oficina -- TraitSubtipo
Oficina -- TraitEstado
Oficina -- TraitCes
Oficina -- TraitAnyoConstruccion
Oficina -- TraitSuperficieConstruida
Oficina -- TraitSuperficieUtil
Oficina -- TraitBanyo
Oficina -- TraitSeguridad

Oficina *-- TipoTerreno
class Oficina{
}


Terreno -- TraitSubtipo
Terreno *-- TipoTerreno
Terreno *-- CalificacionUrbanistica

class Terreno{
    - tipoTerreno : TipoTerreno
    - calificacionUrbanistica : CalificacionUrbanistica
    - superficie
    - superficieEdificable
    - facilityElectricidad
    - facilityGasNatural
    - facilityAccesoRodado
    - facilityAlcantarillado
    - facilityAcera
    - facilityIluminacionPublica
    - facilityAgua
}

Trastero -- TraitSubtipo
Trastero -- TraitEstado
Trastero -- TraitSuperficieConstruida
Trastero -- TraitSeguridad

class Trastero{
    - acceso24h
    - altura
    - muelleCarga
}

class TraitEstado
{
    + {static} REFORMA_INTEGRAL : 1
    + {static} REFORMA_NECESARIA : 2
    + {static} BUEN_ESTADO : 3
    + {static} MUY_BUEN_ESTADO : 4
    + {static} RECIEN_REFORMADO : 5
    + {static} NUEVO_O_SEMINUEVO : 6

    - estado
}

class TraitAccesibilidad
{
    - accesiblidadAcceso : bool
    - accesibilidadUso : bool
}

class TraitAnyoConstruccion
{
    - anyoConstruccion : int
}

class TraitAscensor {
    - ascensorNumero : int
    - ascensorCotaCero : int
}

class TraitBanyo {
    - numero: int
    - adaptado : bool
}

class TraitEmergencia{
    - emergenciaSalida
    - emergenciaLuces
}

TraitCes -- Ces

class TraitCes{
    - ces : Ces
}

class TraitSubtipo{
    - subtipo: int
}

class TraitOrientacion {
    - orientacionNorte
    - orientacionSur
    - orientacionOeste
    - orientacionEste
}

class TraitPlantasConstruidas {
    - plantasConstruidas
}

class TraitSeguridad {
    - seguridadSistema
    - seguridadAlarma
    - seguridadPuerta
    - seguridad24h
    - seguridadPersonal
}

class TraitSuperficieNegociable{
    - superficieNegociable
}

class TraitSuperficieAjardinada {
    - superficieAjardinada
}

class TraitSuperficieConstruida
{
    - superficieConstruida;
}

class TraitSuperficieUtil {
    - superficieutil;
}

class TraitVivienda {
    - balcon : bool
    - terraza : bool
    - armarioEmpotrado : bool
    - numeroHabitaciones : int
    - segundaMano : SegundaMano
    - amueblado : Amueblado 
    - aireAcondicionado : AireAcondicionado
    - calefaccion : Calefaccion
    - aguaCaliente : AguaCaliente
    - aislamiento : Aislamiento
    - chimenea : bool
    - cocinaEquipada : bool
    - jardin : bool
    - piscina : bool
    - disponibilidadGarage : DisponibilidadGarage
    - animalesPermitidos : bool
    - disponibilidadTrastero : DisponibilidadTrastero
}



class AdmiteEstudiantes{
    + {static} NO = 0
    + {static} SI = 1;
    + {static} SOLO_ESTUDIANTES

     - admiteEstudiantes : bool
}

class AguaCaliente{
    + {static} NO = 1
    + {static} CENTRAL = 2
    + {static} INDIVIDUAL_GAS_NATURAL = 3
    + {static} INDIVIDUAL_ELECTRICA = 4
    + {static} INDIVIDUAL_GAS_BUTANO = 5
    + {static} INDIVIDUAL_CALOR_AZUL_BAJO_CONSUMO = 6
    + {static} INDIVIDUAL_SOLAR = 7
    + {static} INDIVIDUAL_GASOLEO = 8
    + {static} TOMA_GAS = 9
    + {static} CENTRAL_CON_CONTADOR_INDIVIDUAL = 10
    + {static} RESERVADO = 11
    + {static} SI_SIN_DETALLAR = 12
    + {static} INDIVIDUAL_GAS_PROPANO = 13

    - aguaCaliente : int
}

class AireAcondicionado{
    + {static} NO = 0
    + {static} SI_SIN_DETALLAR = 12
    + {static} BOMBA_DE_CALOR = 2
    + {static} CLIMATIZACION = 3
    + {static} INSTALACION_COMPLETA = 4
    + {static} PREINSTALACION = 5
    + {static} SPLIT = 6

    - aireAcondicionado : int
}

class Aislamiento{
    - termico : bool
    - acustico : bool
}

class Amueblado{
    + {static} SIN_DATOS = 0
    + {static} AMUEBLADO = 1
    + {static} SEMIAMUEBLADO = 2
    + {static} NO_AMUEBLADO = 3

    - amueblado : int;
}

class Calefaccion{
    + {static} NO = 1
    + {static} CENTRAL = 2
    + {static} INDIVIDUAL_GAS_NATURAL = 3
    + {static} INDIVIDUAL_ELECTRICA = 4
    + {static} INDIVIDUAL_GAS_BUTANO = 5
    + {static} INDIVIDUAL_CALOR_AZUL = 6
    + {static} INDIVIDUAL_SOLAR = 7
    + {static} INDIVIDUAL_GASOLEO = 8
    + {static} INDIVIDUAL_ACUMULA = 9
    + {static} INDIVIDUAL_SUELO = 10
    + {static} TUBOS = 11
    + {static} SI = 12
    + {static} BOMBA = 13
    + {static} CENTRAL_CONTADOR = 14
    + {static} TOMA = 15
    + {static} CHIMENEA = 16
    + {static} INDIVIDUAL_GAS_PROPANO = 17
    + {static} RESERVADO = 18
    + {static} PALLET = 19

    - calefaccion : int

}

class CalificacionUrbanistica{
    + {static} RUSTICO = 0
    + {static} URBANIZABLE = 1
    + {static} URBANO = 2
    + {static} INDUSTRIAL = 3
    + {static} EN_DESARROLLO = 4

    - calificacionUrbanistica : int
}

class Caracteristica{
    - tipo
    - estado
}

class CaracteristicaPared{
    + {static} TIPO_GOTELE = 1
    + {static} TIPO_LISA = 2
    + {static} TIPO_PAPEL = 3
    + {static} TIPO_ESTUCO = 4
    + {static} ESTADO_RECIEN_PINTADA = 1
    + {static} ESTADO_BUEN_ESTADO = 2
    + {static} ESTADO_NECESARIO_PINTAR = 3
    + {static} ESTADO_NECESARIO_RASEAR_Y_PINTAR = 4
}

Caracteristica <|- CaracteristicaPared

class CaracteristicaPuerta{
    + {static} TIPO_ROBLE = 1
    + {static} TIPO_LACADO = 2
    + {static} TIPO_HAYA = 3
    + {static} TIPO_SAPELI = 4
    + {static} TIPO_PINO = 5
    + {static} TIPO_CRISTAL = 6
    + {static} TIPO_CEREZO = 7
    + {static} TIPO_MADERA = 8
    + {static} ESTADO_NUEVAS = 1
    + {static} ESTADO_BUEN_ESTADO = 2
    + {static} ESTADO_REGULAR = 3
    + {static} ESTADO_REPARAR = 4
    + {static} ESTADO_MUY_DETERIORADAS = 5
}

Caracteristica <|- CaracteristicaPuerta

class CaracteristicaSuelo{
    + {static} TIPO_TARIMA = 1
    + {static} TIPO_PARQUET = 2
    + {static} TIPO_TERRAZO = 3
    + {static} TIPO_RESERVADO = 4
    + {static} TIPO_MOQUETA = 5
    + {static} TIPO_SINTASOL = 6
    + {static} TIPO_GRES = 7
    + {static} TIPO_MARMOL = 8
    + {static} TIPO_ROBLE = 9
    + {static} TIPO_PINO = 10
    + {static} TIPO_ELONDO = 11
    + {static} TIPO_PARQUET_X = 12
    + {static} TIPO_CORCHO = 13
    + {static} TIPO_HIDRA = 14
    + {static} TIPO_NOGAL = 15
    + {static} TIPO_LAMINADO = 16
    + {static} TIPO_RESINA = 17
    + {static} TIPO_EUCALIPTO = 18
    + {static} TIPO_VINILO = 19
    + {static} ESTADO_NUEVO = 1
    + {static} ESTADO_BUEN_ESTADO = 2
    + {static} ESTADO_NECESITA_BARNIZAR = 3
    + {static} ESTADO_NECESITA_BARNIZAR_Y_LIJAR = 4
    + {static} ESTADO_MUY_DETERIORADO = 5
    + {static} ESTADO_REGULAR = 7
}

Caracteristica <|- CaracteristicaSuelo

class CaracteristicaVentana{
    + {static} TIPO_ALUMINIO = 1
    + {static} TIPO_ALUMINIO_CLIMALIT = 2
    + {static} TIPO_PVC_CLIMALIT = 3
    + {static} TIPO_MADERA = 4
    + {static} TIPO_MADERA_CLIMATIC = 5
    + {static} TIPO_HIERRO = 6
    + {static} ESTADO_NUEVAS = 1
    + {static} ESTADO_BUEN_ESTADO = 2
    + {static} ESTADO_REPARAR = 3
    + {static} ESTADO_MUY_DETERIORADAS = 4
}

Caracteristica <|- CaracteristicaVentana

class DisponibilidadTratero{
    + {static} NO_TIENE = 0
    + {static} TRASTERO_OPCIONAL = 1
    + {static} TRASTERO_INCLUIDO = 2
    - disponibilidadTrastero : int
}

class Exterior{
    + {static} INDIFINIDO = 0
    + {static} EXTERIOR = 1
    + {static} SEMIEXTERIOR = 2
    + {static} INTERIOR = 3

    - exterior : int
}

class SegundaMano{
    + {static} NUEVA = 0
    + {static} SEGUNDA_MANO = 1
    + {static} SIN_DEFINIR = null

    - segundaMano : int
}

class TipoEstructura{
   + {static} INDIFINIDO = 0
   + {static} HORMIGON = 1
   + {static} MADERA = 2
   + {static} MIXTA = 3  

    - tipoEstructura : int
}

class TipoFachada{
    + {static} Indefinido = 0
    + {static} Caravista = 1
    + {static} Raseada = 2
    + {static} Plaqueta = 3
    + {static} Piedra = 4
    + {static} Monocapa = 5
    + {static} Otros = 6
    + {static} Ventilada = 7
    + {static} Gresite = 8
    + {static} SATE = 9

    - $tipoFachada : int

}
class TipoTerreno{
    + {static} LLANO = 1;
    + {static} SEMILLANO = 2;
    + {static} PENDIENTE = 3;

    - tipoTerreno : int
}


@enduml
```