<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble\caracteristica;

class CaracteristicaPuerta extends Caracteristica{
    static $TIPO_ROBLE = 1;
    static $TIPO_LACADO = 2;
    static $TIPO_HAYA = 3;
    static $TIPO_SAPELI = 4;
    static $TIPO_PINO = 5;
    static $TIPO_CRISTAL = 6;
    static $TIPO_CEREZO = 7;
    static $TIPO_MADERA = 8; 


    static $ESTADO_NUEVAS = 1;
    static $ESTADO_BUEN_ESTADO = 2;
    static $ESTADO_REGULAR = 3;
    static $ESTADO_REPARAR = 4;
    static $ESTADO_MUY_DETERIORADAS = 5;

}