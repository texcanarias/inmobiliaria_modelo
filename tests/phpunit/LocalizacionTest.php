<?php namespace inmotek_test\phpunit;

use PHPUnit\Framework\TestCase;

final class localizacionTest extends TestCase {

    public function testLocalizacion() {
        $dir = \inmotek\model\inmueble\Localizacion::factoryLocalizacion('clave','ES', 35, 'Las Palmas', 35012, 'LPGC', '35012', 20.00, 20.01, 'Don Pío Coronado', '172', 'Unica', '2º', 'A');

        $this->assertTrue( 'ES' == $dir->getPais() );
        $this->assertTrue( 35 == $dir->getProvincia()->getId() );
        $this->assertTrue( 'Las Palmas' == $dir->getProvincia()->getName() );
        $this->assertTrue( 35012 == $dir->getLocalidad()->getId() );
        $this->assertTrue( 'LPGC' == $dir->getLocalidad()->getName() );
        $this->assertTrue( '35012' == $dir->getCp() );
        $this->assertTrue( 20.00 == $dir->getGps()->getLatitude() );
        $this->assertTrue( 20.01 == $dir->getGps()->getLongitude() );
        $this->assertTrue( 'Don Pío Coronado' == $dir->getCalle() );
        $this->assertTrue( '172' == $dir->getPortal() );
        $this->assertTrue( 'Unica' == $dir->getEscalera() );
        $this->assertTrue( '2º' == $dir->getPiso() );
        $this->assertTrue( 'A' == $dir->getMano() );
    }

}
