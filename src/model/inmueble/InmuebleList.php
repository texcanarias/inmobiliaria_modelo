<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble;
use \inmotek\model\base\ArrayStorage;

class InmuebleList extends ArrayStorage {

    public function __construct() {
        parent::__construct('\inmotek\model\inmueble\Inmueble');
    }
}
