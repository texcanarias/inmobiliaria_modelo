<?php 
declare(strict_types = 1);
namespace inmotek\model\inmueble\media;

use \inmotek\model\base\ArrayStorage;

class VideoList extends ArrayStorage {

    public function __construct() {
        parent::__construct('\inmotek\model\inmueble\media\Video');
    }
}
