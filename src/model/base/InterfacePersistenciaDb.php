<?php 
declare(strict_types = 1);
namespace inmotek\model\base;

/**
 * Modelo base para sostener la persistencia
 * Gestión del nombre de la base de datos
 */
interface InterfacePersistenciaDb{
    public function getDb() : string;
    public function setDb(string $Db) : self;
}